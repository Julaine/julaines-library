<?php
/**
 * Database connection handler
 *
 * Needs to be expanded
 * @package julaine
 */
/**
 * Class Database
* Cleaned out junk
 * @version 1.0.5 added insert and update
 * @package julaines-library
*
* Will this replace dependency on Spoon for database protocols?
* Or will this be removed eventually?
* 
*/
class Database {

	/**
	 *  New!!
	 * 
	 * @param  [type] $table_name [description]
	 * @param  [type] $form_data  [description]
	 * @return results of query
	 */
	function dbRowInsert($table_name, $form_data)
	{
	    // retrieve the keys of the array (column titles)
	    $fields = array_keys($form_data);

	    // build the query
	    $sql = "INSERT INTO ".$table_name."
	    (`".implode('`,`', $fields)."`)
	    VALUES('".implode("','", $form_data)."')";

	    // run and return the query result resource
	    return self::db($sql);
	}


	/**
	 * My version of a db connection
	 *
	 * @param string $string db query
	 * @param int    $debug  0,1,2
	 * @param string $other  Use for another database
	 * @return array|bool|int|null|string - varies
	 */
	public static function db($string, $debug=0) {
		$debugger = new Debugger;
		$dblink = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        
		$debugger->Debug($debug,$string);
		if (mysqli_connect_errno()) {
			printf("Due to unforeseen circumstances, our database is down: %s\n", mysqli_connect_error());
			exit();
		}
	    $dbConnect = mysqli_query($dblink,$string);
	    if ($dbConnect == false) {
			$self = filter_var($_SERVER['PHP_SELF'], FILTER_SANITIZE_STRING);
	        Bugs::setError("debug","WARNING the $string returned an error ".mysqli_connect_errno().".\npage: ".$self);
		   return FALSE;
	    } else {
            $row = '';
			if (substr($string,0,6) == "SELECT") {
				$numColumns = mysqli_field_count($dblink) ; //returns the number of columns
				$numFields = mysqli_num_rows($dbConnect) ; //returns the number of rows 
				if ($numColumns == 1) {
					if ($numFields > 1) {
						while($row2 = mysqli_fetch_array($dbConnect, MYSQLI_ASSOC)) {    
							 $row[] = $row2; 
						} 
					} else {
						//returns enumerated array with each column stored in array starting from 0
						$row = mysqli_fetch_row($dbConnect); 
					}
				} elseif ($numColumns > 1) {	
					//QUESTION is there a better way?
	               while ($row2 = mysqli_fetch_array($dbConnect, MYSQLI_ASSOC)) {
						 $row[] = $row2; 
	                }        
				} else {
					//there are no fields
	                Bugs::setError("debug","WARNING, no fields found on $string");
				}
			} elseif (substr($string,0,6) == "INSERT") {
				$row = mysqli_insert_id($dblink);
			} else {
				$row = mysqli_affected_rows($dblink);

			}
			mysqli_close($dblink);
			return $row;
		}
	}


    /**
     * Quote the name of a table or column.
     * Note: for now this will only put backticks around the name (mysql).
     *
     * @return	string			The quoted name.
     * @param	string $name	The name of a column or table to quote.
     */
    public function quoteName($name)
    {
        return '`' . $name . '`';
    }

    public function getEnumValues($table, $field) {
        // redefine vars
        $table = $this->quoteName((string) $table);
        $field = (string) $field;

        // build query
        $row = db("SHOW COLUMNS FROM $table LIKE '$field'");


		if (!$row) return 'no db';

        // has no type, so NOT an enum field
        if(!isset($row['Type'])) return "type is ".$row['Type'];

        // has a type but it's not an enum
        if (strtolower(substr($row['Type'], 0, 4) != 'enum'))
            return 'not enum';

        // process values
        $aSearch = array('enum', '(', ')', '\'');
        $types = str_replace($aSearch, '', $row['Type']);

        // return
        return (array) explode(',', $types);

    }
	/**
	 * Really lingering usage
	 * Used in old files
	 * @since 1.0.4
	 * @param $pairs
	 * @param $exclude
	 * @param $table
	 * @param $tableid
	 * @param $tableidvalue
	 * @return string
	 */
	function update($pairs, $exclude, $table, $tableid, $tableidvalue) {
		$SQL = "UPDATE $table SET ";
		$first=true;
		foreach ($pairs as $key=>$value) {
			if ($key!=$tableid) {
				if ($key!="Submit") {
					if (isset($exclude)) {
						if ($key!=$exclude) {
							if ($first)  {
								$SQL .= "$key='$value'";
								$first = false;
							} else {
								$SQL .= ",$key='$value'";
							}
						}
					} else {
						if ($first)  {
							$SQL .= "$key='$value'";
							$first = false;
						} else {
							$SQL .= ",$key='$value'";
						}
					}
				}
			}
		}
		$SQL .= " WHERE $tableid='$tableidvalue'";
		return $SQL;
	}

	/**
	 * Lingerging usage
	 * @since 1.0.4
	 * @param $pairs
	 * @param $exclude
	 * @param $table
	 * @return string
	 */
	function insert($pairs, $exclude, $table) {
		$first=true;
		$SQL2 = "INSERT INTO $table ";
		foreach ($pairs as $key=>$value) {
			if ($key != "Submit") {
				if (isset($exclude)) {
					if ($key != $exclude) {
						if ($first)  {
							$value="'".$value;
							$first=false;
						} else {
							$key=",".$key;
							$value=",'".$value;
						}
						$fields.=$key;
						$values.=$value."'";
					}
				} else {
					if ($first)  {
						$value="'".$value;
						$first=false;
					} else {
						$key=",".$key;
						$value=",'".$value;
					}
					$fields.=$key;
					$values.=$value."'";
				}
			}
		}
		$SQL2 .= "($fields) VALUES($values)";
		return $SQL2;
	}
}
?>