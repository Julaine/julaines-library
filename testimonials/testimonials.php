<?php
/**
 * @version  1.0.1
 * @uses  Spoon Library
 *
 * @todo  Add DB handler on __construct
 */
class Testimonials {
	function addTestimonial($postedValues) {
		if (is_array($postedValues)) extract($postedValues);
		$saveValues = array();
		$saveValues['productId'] = $productId;
		$saveValues['review'] = $review;
		$saveValues['reviewerName'] = $reviewerName;
		$saveValues['reviewerAddress'] = $reviewerAddress;
		$saveValues['reviewStars'] = $dateAdded;
		$saveValues['approved'] = date("Y-m-d h:i:a", time());

		$reviewId = $this->db->insert("reviews",$saveValues);	
		if ($reviewId > 0) {
			$_SESSION['success'] = 'Thank you.  The message was saved.';
			return TRUE;
		} else {
			$_SESSION['errors'] = 'The database failed to save the review.';
			return FALSE;
		}
	}
	function getTestimonial($reviewId) { 
		if ((int)$reviewId > 0) {
			$query = 'SELECT * FROM reviews 
					WHERE reviewId=?';
			$rows = $this->db->getRecord($query,$reviewId);
			return $rows;
		} else {
			return FALSE;
		}
	}
	 function getTestimonials($num=0) {
		$display = array();
		if ((int)$num > 0) {
			$findReview = Database::db("SELECT review,reviewerName,reviewerAddress
            FROM reviews WHERE approved='Yes' AND productId='0' ORDER BY RAND() LIMIT $num");
		} else {
			$findReview = Database::db("SELECT review,reviewerName,reviewerAddress FROM reviews WHERE approved='Yes' AND
			productId='0'");
		}
		if ($findReview) {
			
			foreach ($findReview as $row) {
				extract($row);
				$display[] = '<div class="alert alert-success">';
				$display[] = '<p><em>'.stripslashes($review).'</em>';
				$display[] = '<p class="reviewer_name">--'.$reviewerName;
				$display[] = '<br>'.$reviewerAddress.'</p>';
					$display[] = '</div>';
			}
		
		}
		return join("\n",$display);
	}
	function showAllTestimonials() {
		$query = 'SELECT * FROM reviews 
				WHERE productId=? 
				ORDER BY dateAdded DESC';
		$row = $this->db->getRecords($query, 0);
		if (!$row) return FALSE;
		$display = array();
		foreach ($row as $values) {
			$date = date("M d,y", strtotime($dateAdded));
			$display[] = <<<HEREDOC
<tr><td><a href="testimonials.php?id={$reviewId}">edit</a></td>
	<td>{$date}</td>
	<td><div class="rating starts $numStars">&nbsp;</div></td>
	<td>{$reviewerName}</td>
	<td>{$review}</td>
	<td>{$approved}</td>
	</tr>
HEREDOC;
		}
		return join("\n",$display);
	}	
	function updateTestimonial($postedValues) {
		if (is_array($postedValues)) extract($postedValues);
		if ($reviewStars > 0) {
			$updateStars = $reviewStars;	
		} else {
			$updateStars = $originalStars;
		}
		$affected = $this->db->update("reviews",$reviewsArray,'reviewId=?',$reviewId);
		if ($affected > 0) {
			$_SESSION['success'] = 'Thank you.  The review was updated.';
			return TRUE;
		} else {
			$_SESSION['errors'] = 'The database failed to save the review.';
			return FALSE;
		}
	}
} 
?>