<?php
/**
 * Page-level DocBlock
 *
 * @package julaines-library
 *
 */

/**
 * Class SendEmails
 * setup to simplify sending emails
 * Use everywhere
 * @package julaines-library
 * @version 0.0.1
 */
class SendEmails
{
    function sendTxtEmailFromDb($id, $to, $toEmail, $htmlVars = null, $orderId = 0, $subject = null)
    {
        if ($id > 0) {
            $getTpl = Database::db("SELECT * FROM
            emailsMessages WHERE
            emailMessageId='$id'");
            foreach ($getTpl as $row) {
                extract($row);
            }
        } else {
            Bugs::setError("save", "WARNING no email message identified to send email");
            return false;
        }
        $vars = self::setSource($emailFrom);
        if (!$vars) {
            return false;
        }
        if ($vars) {
            extract($vars);
        }
        $from = "$from_name <$u>";

        if ($emailTo == "Customer" || $emailTo == "Member") {
            $recipient = "$to <$toEmail>";
        } else {
            $recipient = str_replace('@', '', $emailTo)." <".$emailTo.DOMAIN>";

        }

        if (is_null($subject)) {
            $subject = $emailSubject;
        }

        $msg = $emailMessageTxt;
        if (!is_null($htmlVars)) {
            foreach ($htmlVars as $name => $value) {
                $msg = str_replace("$name", $value, $msg);
            }
        }
        return self::sendEmail($recipient, $subject, $msg, $from);
    }

    /**
     * Set the SMTP settings for sending mail
     *
     * @param $choice
     * @return array
     */
    function setSource($choice)
    {
        $choice = str_replace('@', '', $choice);

        $host = "mail.julaine.ca";

        $u        = "webmaster@julaine.ca";
        $password = "webmaster";

        $from_name = "Julaine Scott";

        return array("host" => $host, "u" => $u, "password" => $password, "from_name" => $from_name);
    }


    function sendEmails($to, $subject, $msg, $from, $msg_html=NULL, $attachment=NULL) {
        require PATH_SITE.'components/PHPMailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;
        if ($mail->validateEmail($to) == FALSE) {
            Bugs::setError("save","WARNING $to is an invalid email","sendEmails");
            $_SESSION['errors'] = 'Invalid email address';
            return FALSE;
        }
        $vars = self::setSource($from);
        if (!$vars) {
            return false;
        }
        if ($vars) {
            extract($vars);
        }

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $host;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $u;                 // SMTP username
        $mail->Password = $password;                           // SMTP password
        //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 25;                                    // TCP port to connect to

        $mail->setFrom($u, $from_name);
        $mail->addAddress($to);     // Add a recipient
        $mail->addReplyTo($to);
        $mail->addCC($u);
        $mail->addBCC('work@julaine.ca');

        $mail->addAttachment($attachment);         // Add attachments
        if (!is_null($msg_html)) {
            $mail->isHTML(TRUE);                                  // Set email format to HTML
            $mail->Body    = $msg_html;
            $mail->AltBody = $msg;
        } else {
            $mail->isHTML(FALSE);
            $mail->Body    = $msg;
            $mail->AltBody = $msg;
        }
        $mail->Subject = $subject;


        if(!$mail->send()) {
            //echo 'Message could not be sent.';
            Bugs::setError("debug",'Mailer Error: ' . $mail->ErrorInfo);
            return FALSE;
        } else {
            return TRUE;
        }
    }
}