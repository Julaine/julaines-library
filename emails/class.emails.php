<?php
/**
 * Email handler
 * @package julaines-library
 */
/**
 * Class Emails
 * @package julaines-library
 * @version 1.0.3
 *
 */
class Emails {
	protected $today;


	//database table 'emails'
	protected  $table_emails_fields;

	//database table 'messages'
	protected $table_messages_fields;
	protected $db;

	function __construct() {
		$this->db = new SpoonDatabase(DB_TYPE, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (!$this->db) {
			Bugs::setError('notify','Due to unforeseen circumstances, the site is unavaiable.
				Please try again later.');
		}
		$this->today =date("Y-m-d",time());
		$this->table_emails_fields = array('email_id', 'message_id',
			'email_to', 'email_from', 'email_subject', 'email', 'email_sent', 'email_update');
		$this->table_messages_fields = array('message_id', 'message',
			'message_from', 'message_to', 'message_subject', 'message_format', 'message_last_update', 'message_update');

	}	
	/**
	 * Prepare file that contains message HTML
	 * For now we are using message_id for the filename
	 * 
	 * Am I using this code?
	 * Or am I storing the HTML in the db?
	 *
	 * @todo Make it more generic
	 * @return  boolean 
	 * @param   $message
	 * @param $message_id
	 */
	function createMessageFile($message,$message_id) {
		$message_file = 'msg_'.$message_id.'.tpl';
		
		//need the full path to where we store email templates
		$long_name = PATH_ROOT.'/email_path_here/'.$message_file;
		/**
		 * Do I need to decode the message?
		 * SpoonFilter::htmlentitiesDecode($message)
		 */
		return SpoonFile::setContent($long_name,$message);
	}
	/**
	 * Retrieve a message from the db
	 *
	 * @return array of message and message_file
	 * @param  int $message_id optional
	 * @param  string $message optional
	 */
	function getMessage($message_id=NULL,$message=NULL) {
		if (!is_null($message_id)) {
			$find = 'SELECT message
					FROM messages
					WHERE message_id=?';
			$message = $this->db->getVar($find, $message_id);
		}
		if (!is_null($message)) {
			$find = 'SELECT message
					FROM messages
					WHERE message=?';
			$message = $this->db->getVar($find, $message);
		}
		if (!$message) return FALSE;
		return $message;
	}

	/**
	 * Get messages for someone
	 *
	 * @return array of db values to use in wrapEmailsAccordions
	 * @param  int $for $email_to value
	 */
	function getMessagesFor($for=NULL) {
		$findTo = 'SELECT account_email
					FROM accounts
					WHERE accountId=?';
		$email_to = $this->db->getVar($findTo,$for);
		$findEmails = 'SELECT email_id,message_id,email_to,email_from,email_subject,
						email,email_sent 
						FROM emails 
						WHERE email_to=?
						ORDER BY email_sent DESC';
		$rows = $this->db->getRecords($findEmails, $email_to);
		if (!$rows) return FALSE;
		return $rows;
	}
	/**
	 * Get messages from someone
	 *
	 * @return array of db values to use in wrapEmailsAccordions
	 * @param  int $from $email_to value
	 */
	function getMessagesFrom($from=NULL) {
		$findFrom = 'SELECT account_email
					FROM accounts
					WHERE accountId=?';
		$email_from = $this->db->getVar($findFrom,$from);
		$findEmails = 'SELECT email_id,message_id,email_to,email_from,email_subject,email,email_sent 
						FROM emails 
						WHERE email_form=? 
						ORDER BY email_sent DESC';
		$rows = $this->db->getRecords($findEmails, $email_from);
		if (!$rows) return FALSE;
		return $rows;
	}

	/**
	 * Saves a record of sending an email in the db
	 *
	 * @todo  Should I save accountId of person logged in?
	 *
	 * @param  string $sentTo
	 * @param  string $sentSubject
	 * @param  string $msg
	 * @return boolean
	 */
    function saveEmail($message_id,$sentTo=NULL,$sentSubject=NULL,$msg) {
		if (is_null($sentSubject) && is_null($sentTo)) {
			return FALSE;
		} else {
			//make array in order of db fields
			$email_array = array();
			$email_array['message_id'] = $message_id;
			$email_array['email_to'] = $sentTo;
			$email_array['email_from'] = 'Webmaster';
			$email_array['email_subject'] = $sentSubject;
			$email_array['email'] = $msg;
			$email_array['email_sent'] = date("Y-m-d h:i:s",time());
			$emailMessageId = $this->db->insert("emails",$email_array);

			 if ($emailMessageId > 0) {
				return TRUE;
			 } else {
				return FALSE;
			 }
		}
    }

	/**
	 * Sends an email using SpoonEmail
	 *
	 * @param      $emailToName
	 * @param      $emailTo
	 * @param      $emailSubject
	 * @param      $htmlBody
	 * @param null $htmlVars
	 * @param      $txtBody
	 * @param      $id
	 * @return bool
	 *
	 */
	function sendEmail($emailToName,$emailTo,$emailSubject,$htmlBody,$htmlVars=NULL,$txtBody,$id) {
		$email = new SpoonEmail();
		// this is the folder your compiled e-mail templates will end up in
		$email->setTemplateCompileDirectory(PATH_ROOT.'/cache_emails');
		
		// SMTP info, remember to use your Gmail account credentials
		//$email->setSMTPSecurity('tls');
		$email->setSMTPConnection('', '587');
		$email->setSMTPAuth('webmaster@', 'webmaster');
		
		// set e-mail headers
		$email->setFrom('webmaster@');
		$email->addRecipient($emailTo, $emailToName);
		
		if (SPOON_DEBUG == TRUE) {
			$email->addBCC('work@julaine.ca','Julaine Scott');
		}
		// set e-mail contents
		$email->setSubject($emailSubject);
		if ($id > 0) {
			$tpl_path = PATH_TEMPLATES.'emails/msg_'.$id.'.tpl';
		} else {
			$tpl_path = $htmlBody;
		}
		$email->setHTMLContent($tpl_path,$htmlVars); 
		$email->setPlainContent($txtBody);
		// send the e-mail
		if ($email->send() == TRUE) {
			//leave the send here as this is the only place it works to compile
			return $this->saveEmail($id,$emailTo,$emailSubject,$email->getTemplateContent($tpl_path, $htmlVars)); 
		} else {
			return FALSE;
		}			
	}

	/**
	 * Send a message as an email
	 *
	 * @todo  Resolve problem with the emailTo and messageTo fields
	 *        which one do I use and why?
	 *
	 * @todo  What about variables for the email template files?
	 *        Do I need to add a param for an array of fields?
	 *
	 * @param      $emailToName
	 * @param      $emailTo
	 * @param      $message_id
	 * @param null $htmlVars
	 * @return bool
	 */
	function sendMessageByEmail($emailToName,$emailTo,$message_id,$htmlVars=NULL) {
		$row = NULL;
		$find = 'SELECT message_id,message,message_to,
				message_subject
				FROM messages
				WHERE message_id=?';
		$row = $this->db->getRecord($find,$message_id);
		if (!$row) return FALSE;
		extract($row);
		//$htmlBody = SpoonFilter::htmlentitiesDecode($message);
		$htmlBody = $message;
		$message_plain = strip_tags($htmlBody);

		return $this->sendEmail($emailToName,$emailTo,$message_subject,$htmlBody,$htmlVars,$message_plain,$message_id);
		
	}	
	/**
	 * Returns form and code for adding a new message
	 * Uses SpoonForm
	 * 
	 */
	function showAddMessageForm() {
		$frm = new SpoonForm('message_add');
		$frm->setParameter("class","form-horizontal");
		$frm->addText('message_from', NULL, 40, 'form-control'); 
		$frm->addText('message_to', NULL, 40, 'form-control'); 
		$frm->addText('message_subject', NULL, 40, 'form-control')
			->setAttribute('required'); 
		$frm->addTextarea('message', NULL, 'textarea', TRUE); 
		// add hidden value(s)
		$frm->addHidden('process', '1');
		// add submit button
		$frm->addButton('submit', 'Save')
			->setAttribute('class', 'btn btn-primary btn-lg');

		if($frm->isSubmitted()) {
			$frm->getField('message')->isFilled('Please provide a message.');
			// email is required
			$frm->getField('message_subject')->isFilled('Please provide a subject.');
			// email is required
			if($frm->isCorrect()){
				// all the information that was submitted
				$postedValues = $frm->getValues();
				extract($postedValues);			
				$message_html = SpoonFilter::htmlentitiesDecode($message);
				if (hasTags($message_html)) {
					$message = SpoonFilter::stripHTML($message_html);
				} 	
				$message_array = array('message' => $message,
					'message_to'=> $message_to,
					'message_from' => $message_from, 
					'message_subject' => $message_subject,
					'message_format' => 'HTML',  
					'message_last_update' => (int)$_SESSION['id']);
				$message_id = $this->db->insert('messages', $message_array);
				
				if ($message_id > 0) {
					$this->createMessageFile($message_html,$message_id);
					$_SESSION['success'] = 'Thank you, the new message was added.';
				} else {
					$_SESSION['errors'] ='Due to unforseen circumstances,
					the information was not saved. Please try again.';
				}
				foreach($_POST as $key => $value) {
					unset($_POST[$key]);
				}
				//reload the page or the new item does not show on the list
				header('Location: messages.php');
				die;
			} 
			
			
		}
		$tpl = new SpoonTemplate();
		$tpl->setForceCompile(true);
		$tpl->setCompileDirectory('/path_here/cache');
		$frm->parse($tpl);
		$tpl->display(PATH_ROOT.'/email_path_here/form_message_add.tpl');	
	}
	/**
	 * Returns form and code for adding a new message
	 * @todo  Finish checking process and id values after submitting
	 * 
	 * @return form and code
	 * @param int $id message_id
	 */
	function showMessageForm($id) {
		//find data
		if ((int)$id > 0) {
			$find = 'SELECT message,message_to,message_from,
					message_subject,message_format
					FROM messages
					WHERE message_id=?';
			$rows = $this->db->getRecord($find,$id);
			extract($rows);
		} else {
			$_SESSION['errors'] = 'Please try again.';
		}
		$message_from_file = SpoonFile::getContent(PATH_ROOT.'/email_path_here/msg_'.$id.'.tpl');
		// create form
		$frm = new SpoonForm('message'); 
		$frm->setParameter("class","form-horizontal");
		$frm->addText('message_from', $message_from, 40, 'form-control'); 
		$frm->addText('message_to', $message_to, 40, 'form-control'); 
		$frm->addText('message_subject', $message_subject, 40, 'form-control')
			->setAttribute('required'); 
		$frm->addTextarea('message', $message_from_file, 'textarea', TRUE); 
		

		// add hidden value(s)
		$frm->addHidden('process', '1');
		$frm->addHidden('id', $id);
		// add submit button
		$frm->addButton('submit', 'Save')
			->setAttribute('class', 'btn btn-primary btn-lg');

		if($frm->isSubmitted()) {
			$frm->getField('message')->isFilled('Please provide a message.');
			$frm->getField('message_subject')->isFilled('Please provide a subject.');

			//do I need to check that the id value is set?
			//$frm->getField('process')->isNumeric('Please try again.');
			//can I check if process == 1?

			if($frm->isCorrect()){
				// all the information that was submitted
				$postedValues = $frm->getValues();
				extract($postedValues);	

				$message_html = SpoonFilter::htmlentitiesDecode($message);
				if (hasTags($message_html)) {
					$message_clean = SpoonFilter::stripHTML($message_html);
				} else {
					$message_clean = $message_html;
				}
		
				$message_array = array('message' => $message_clean,			
					'message_to'=> $message_to,
					'message_subject' => $message_subject,
					'message_format' => $message_format, 
					'message_last_update' => (int)$_SESSION['id']);
				$update = $this->db->update('messages', $message_array, 'message_id=?',$id);
				
				if ($update > 0) {
					$this->createMessageFile($message_html,$id);
					$_SESSION['success'] = 'Thank you, the message was update.';
				} else {
					$_SESSION['errors'] = 'Due to unforseen circumstances,
						the database did not save the message. Please try again.';
				}
				foreach($_POST as $key => $value) {
					unset($_POST[$key]);
				}
				header('Location: messages.php');
				die;	
			} 			
			
		}
		$tpl = new SpoonTemplate();
		$tpl->setForceCompile(true);
		$tpl->setCompileDirectory('/path_here/cache');
		$frm->parse($tpl);
		$tpl->display(PATH_ROOT.'/email_path_here/form_messages.tpl');	
	}
	/**
	 * Show all the email messages
	 *
	 * @return  string HTML code for accordion
	 * @param  int $for 			accountId for person receiving emails
	 * @param  int $from 			accountId for the person sending emails
	 * 
	 */
	function wrapEmails($for=NULL,$from=NULL) {
		if (!is_null($for)) $rows = $this->getMessagesFor($for);
		if (!is_null($from)) $rows = $this->getMessagesFor($for);
		
		if (!$rows) { return FALSE; } //we do not want empty accordions
		$returnString = array();
		foreach ($rows as $value) {
			extract($value);
			$date = date("M d, y H:ia",strtotime($email_sent));
			if (substr($email,0,3) !== "<p>") {
				$email = str_replace("\n\n", "</p>\n<p>", '<p>'.$email.'</p>');
				$email = str_replace("\n", "\n<br>", $email);
			}
			$email_from = str_replace(">","&#8250;",$email_from);
			$email_from = str_replace("<","&#8249;",$email_from);
			$emails = SpoonFilter::htmlentitiesDecode($email);
			$returnString[] = <<<HEREDOC
<p><a data-target="#emailMessage$email_id" data-toggle="collapse" href="javascript:;">
[$date] $email_subject</a></p>
<div id="emailMessage$email_id" class="collapse">
<p><strong>From:</strong> $email_from</p>
<p><strong>Subject:</strong> $email_subject</p>
<hr>
$emails
</div>
HEREDOC;
		}
		return join("\n",$returnString);
	}
	/**
	 * 
	 * Warp all the email messages in an accordion
	 * ready for use
	 *
	 * @return  string HTML code for accordion
	 * @param  int $for 			accountId for person receiving emails
	 * @param  int $from 			accountId for the person sending emails
	 * 
	 */
	function wrapEmailsAccordions($for=NULL,$from=NULL) {
		if (!is_null($for)) $rows = $this->getMessagesFor($for);
		if (!is_null($from)) $rows = $this->getMessagesFor($for);
		
		if (!$rows) { return FALSE; } //we do not want empty accordions
		$returnString = array();
		$returnString[] = '<div class="panel-group col-lg-8 col-md-8 col-sm-8 col-xs-8" id="accordion">';
		foreach ($rows as $value) {
			extract($value);
			$date = date("M d, y H:ia",strtotime($email_sent));
			if (substr($email,0,3) !== "<p>") {
				$email = str_replace("\n\n", "</p>\n<p>", '<p>'.$email.'</p>');
				$email = str_replace("\n", "\n<br>", $email);
			}
			$email_from = str_replace(">","&#8250;",$email_from);
			$email_from = str_replace("<","&#8249;",$email_from);
			$emails = SpoonFilter::htmlentitiesDecode($email);
			$id = (int)$_SESSION['id'];
			//    <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#email$email_id">[$date] $email_subject</a></h4>
			//  <div id="email$email_id" class="panel-collapse collapse">

			$returnString[] = <<<HEREDOC
<div class="panel panel-primary">
  <div class="panel-heading">
    <h4 class="panel-title">[$date] $email_subject</h4>
  </div>
  <div class="panel-body">
		<p><strong>From:</strong> $email_from</p>
		<hr>
		$emails
  </div>
</div>
HEREDOC;
		}
		$returnString[] = '</div>';
		return join("\n",$returnString);
	}
}
?>
