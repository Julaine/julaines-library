{include|header.tpl}
<p class="pull-right"><a href="https://www.rescuetime.com/browse/activities/by/hour/for/the/day/of/{$logDate}" target="_blank">
    View {date_rescueTime} on RescueTime</a></p>
    
<h1>{ $pageTitle}</h1>
<!-- <table class="">
<tbody>
	<tr><td colspan="3"><form name="previousbutton" method="post" action="view.php">
	<input type="submit" name="previous" value="Previous" class="btn btn-small">
	<input type="hidden" name="td" value="{ $back }">
	<input type="hidden" name="tm" value="{ $backMonth }">
	<input type="hidden" name="ty" value="{ $ty }">
	<input type="hidden" name="p" value="1">
	</form></td>
	<td colspan="4"><form name="view" action="view.php" method="post" class="form-inline">
    <div class="form-group"><div class="col-lg-4">{ listbox_month("tm",$tm) }</div>
   <div class="col-lg-3"> { listbox_date("td",$td) }</div>
    <div class="col-lg-3">{ listbox_year("ty",currYear-3,currYear,$ty) }</div>
    <input name="View" class="btn btn-small btn-success" type="submit" value="View">
    <input type="hidden" name="p" value="1"></div>
    </form></td>
    <td><form name="nextbutton" method="post" action="view.php">
	<input type="submit" name="next" value="Next" class="btn btn-small">
	<input type="hidden" name="td" value="{ $next }">
	<input type="hidden" name="tm" value="{ $nextMonth }">
	<input type="hidden" name="ty" value="{ $ty }">
	<input type="hidden" name="p" value="1">
	</form></td></tr>
</tbody></table>
 -->
    <form action="delete.php" method="post" enctype="multipart/form-data" name="delete" id="delete">
    <table class="table table-striped">
    <thead>	
    <tr>
      <th>Delete</th>
      <th>Project</th>
      <th>Client IssueId</th>
    	<th>IssueId</th>
      <th>Task</th>
      <th>Hours</th>
      <th>Note</th>
      <th>Billable?</th>
    </tr></thead>
	<tfoot>
	
    <tr>
    <td colspan="3"><input name="submit" type="Submit" class="btn btn-small btn-info" value="Delete Selected Records"></td>
      <td colspan="2">Total</td>
      <td>{ $totalHours) }</td>
      <td colspan="2">&nbsp;</td>
    </tr>
	</tfoot>
    <tbody>
{iteration:records}
<tr>
      <td><input type="checkbox" name="lid[]" value="{ $lid }"></td>
      <td>{ $records:pname }</td>
      <td>{ $records:client_iid }</td>
      <td><?php if ((int)$issueId > 0) { ?><a href="respond.php?id={ $issueId }">{ $issueId }</a><?php } ?></td>
      <td>{ $records:tname }</td>
      <td>{ $records:hours }</td>
      <td>{ $records:lnote }</td>
      <td>{ $records:lbill }</td>
    </tr>
{/iteration:records}

	</tbody>
    </table>
    <input type="hidden" name="process" value="1">
    <input type="hidden" name="td" value="{ $td }">
    <input type="hidden" name="tm" value="{ $tm }">
    <input type="hidden" name="ty" value="{ $ty }">
    </form>

<form name="view" action="view.php" method="post" class="form-vertical">
    <p><label>Date</label> <input type="text" size="10" class="input-sm datepicker" name="datestamp">
    <input name="View" type="submit" value="View" class="btn primary">
    </p>
</form>
	<div class="row-fluid">
	<?php if (date("Y-m-d",time()) == $logDate) {?>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	<?php } else { ?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<?php } ?>

{form:timesheet_add}
<div class="form-group">
	<div class="col-lg-6 col-md-4 col-sm-6 col-xs-6">
	<label for="pid" class="control-label">Projects</label> {$txtProjects} <br>{$txtProjectsError}</div>
	<div class="col-lg-6 col-md-4 col-sm-6 col-xs-6">
	<label for="clientIid" class="control-label">Client Issue Num</label> 
	{$txtClientIid} <br>{$txtClientIIdError}</div>
	<div class="col-lg-6 col-md-4 col-sm-6 col-xs-6">
	<label for="issueId" class="control-label">Issue Num</label> 
	{$txtIssueId} <br>{$txtIssueIdError}</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	<label for="tid" class="control-label">Task</label> 
	{$txtTid} <br>{$txtTidError}</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	<label for="h" class="control-label">Task</label> 
	{$txtH} <br>{$txtHError}</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	<label for="lbill" class="control-label">Billable</label> 
	{$txtLbill} <br>{$txtLbillError}</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	<label for="lnote" class="control-label">Note</label> 
	{$txtLnote} <br>{$txtLnoteError}</div>
</div>
<div class="form-group">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">{$btnSubmit} {$hidTy} {$hidTm} {$hidTd}</div>
</div>    

{/form:timesheet_add}	

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<h3>Tasks Updated Today</h3>
<table class="table-striped table-hover">
<tbody>{ $tasksToday} </tbody> </table></div>
</div><!--end of row-fluid-->
<!--end of col-lg-12 col-md-12 col-sm-12 col-xs-12-->
{include|footer.tpl}