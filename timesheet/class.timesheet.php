<?php
/**
 * Class Timesheet
 * Control the timesheet functions
 *
 * @todo FINISH
 * @package julaines-library
 * @version 0.0.2
 */
class Timesheet
{
    function findLog($issueId)
    {
        $find = Database::db("SELECT lid,hours,ldate FROM logs WHERE issueId='$issueId'");
        if (!$find) {
            return "<p>No time billed.";
        } else {
            $dateList   = array();
            $dateList[] = '<ul>';
            foreach ($find as $row) {
                extract($row);
                $ty         = substr($ldate, 0, 4);
                $tm         = substr($ldate, 5, 2);
                $td         = substr($ldate, 8, 2);
                $dateList[] = '<li><a
href="/jadmin/reports/view.php?ty='.$ty.'&tm='.$tm.'&td='.$td.'">'.number_format($hours, 2).'hrs '.date("M d, y",
                        strtotime($ldate)).


                    '</a></li>';
            }
            $dateList[] = '</ul>';
            return "<p>Time billed on: ".join("", $dateList);
        }
    }

    function projectSubNav()
    {
        $findProjects = Database::db("SELECT pid,tagName FROM projects
						WHERE pactive='1' ORDER BY pname");
        foreach ($findProjects as $row) {
            extract($row);
            $subNav[] = '<li><a href="#p'.$pid.'">'.$tagName.'</a></li>';
        }
        return join("\n", $subNav);
    }

    function getTimesheetDate()
    {
        if ((int)$_POST['p'] > 0) {
            if (isset($_POST['ty']) && isset($_POST['tm']) && isset($_POST['td'])) {
                $ty = (int)$_POST['ty'];
                $tm = (int)$_POST['tm'];
                $td = (int)$_POST['td'];
            }
        } elseif ((int)$_GET['p'] > 0) {
            if (isset($_GET['ty']) && isset($_GET['tm']) && isset($_GET['td'])) {
                $ty = (int)$_GET['ty'];
                $tm = (int)$_GET['tm'];
                $td = (int)$_GET['td'];
            }
        }
        return $ty.'-'.$tm.'-'.$td;
    }

    function addTimesheetLog($values)
    {
        if (is_array($values)) {
            extract($values);
        }
        $logDate = (int)$ty.'-'.(int)$tm.'-'.(int)$td;
        return Database::db("INSERT INTO logs
				(pid,tid,issueId,client_iid,hours,ldate,lnote,lbill,lupdate)
				VALUES
				('$p','$tid','$issueId','$client_iid','$h','$logDate','$lnote','Billable',NOW())");

    }
    /**
     * Get the values for a sparkline graph
     *
     * @param $pid
     * @param $when
     * @return string
     */
    public static function showGraphValuesThisYear($pid, $when = "this year")
    {
        $graphArray = array();
        if ($when == "this year") {
            $currMonth = date("M",time());
            for ($start = 1; $start <= $currMonth; $start++) {

                $y = date("Y", time());
                $m = str_pad($start, 2, 0, STR_PAD_LEFT);

                $searchBy = "$y-$m%";
                $getTotal = Database::db("SELECT SUM(hours) FROM logs WHERE pid='$pid' AND ldate LIKE ('$searchBy')");
                if ($getTotal) {
                    $graphArray[] = $getTotal[0];
                } else {
                    $graphArray[] = 0;
                }
            }
        } elseif ($when == "last year") {
            for ($start = 1; $start <= 12; $start++) {

                $y = date("Y", time()) - 1;
                $m = str_pad($start, 2, 0, STR_PAD_LEFT);

                $searchBy = "$y-$m%";
                $getTotal = Database::db("SELECT SUM(hours) FROM logs WHERE pid='$pid' AND ldate LIKE ('$searchBy')");
                if ($getTotal) {
                    $graphArray[] = $getTotal[0];
                } else {
                    $graphArray[] = 0;
                }
            }
        } elseif ((int)$when > 0) {
            for ($start = 1; $start <= 12; $start++) {

                $year = $when;
                $month = str_pad($start, 2, 0, STR_PAD_LEFT);

                $searchBy = "$year-$month%";
                $getTotal = Database::db("SELECT SUM(hours) FROM logs WHERE pid='$pid' AND ldate LIKE ('$searchBy')",1);

                if ($getTotal) {

                    $graphArray[] = $getTotal[0];
                } else {
                    $graphArray[] = "0";
                }
            }

        }

        return $graphArray;

    }
    public static function showGraphValuesTillNow($pid)
    {
        $find = Database::db("SELECT MIN(ldate) FROM logs WHERE pid='$pid'");


        $startYear = substr($find[0],0,4);
        $currYear = date("Y",time());
        $graphArray = array();

        for ($i=$startYear; $i <= $currYear; $i++) {

            for ($start = 1; $start <= 12; $start++) {
                $m = str_pad($start, 2, 0, STR_PAD_LEFT);

                $searchBy = "$i-$m%";
                $getTotal = Database::db("SELECT SUM(hours) FROM logs WHERE pid='$pid' AND ldate LIKE ('$searchBy')");
                if ($getTotal) {
                    $graphArray[] = $getTotal[0];
                } else {
                    $graphArray[] = 0;
                }
            }

        }
        return $graphArray;

    }
    /**
     * @param $id
     * @param $lastMonth
     * @param $tid
     * @return array
     */
    public static function getMonthTotal($id, $lastMonth,$tid=NULL)
    {
        if (!is_null($tid)) {
            $result = Database::db("SELECT SUM(hours) FROM logs WHERE pid='$id'
AND tid='$tid' AND ldate LIKE '$lastMonth'");
        } else {
            $result = Database::db("SELECT SUM(hours) FROM logs WHERE pid='$id'
AND ldate LIKE '$lastMonth'");
        }
        if ($result) {
            $totalForProjectLast = $result[0];
            return (int)$totalForProjectLast;
        }
        return 0;
    }
}

?>