<?php
/**
 * Generic notice method
 *
 * @package julaines-library
 * @version 2.0.8
 * @uses  Spoon (but does not have daterangepicker)
 *
 *
 */
class Notices {
	protected $db;
	protected $today;

    /**
     * Open a db connection
     *
     */
	function __construct() {
		// create database object
		$this->db = new SpoonDatabase(DB_TYPE, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (!$this->db) {
			Bugs::setError('notify','Due to unforeseen circumstances, the site is unavaiable.
				Please try again later.');
		}
		$today = date("Y-m-d",time());

	}

	function countNotices($id=NULL) {

		if (is_null($id)) {
			$find = 'SELECT COUNT(notice_id) FROM notices
				WHERE notice_end >=CURDATE()';
			$num = $this->db->getVar($find);
		} else {
			$find = 'SELECT COUNT(notice_id) FROM notices
				WHERE assigned_to=?
				AND notice_end >=CURDATE()';
			$num = $this->db->getVar($find,$id);
		}
		if (!$num) return 0;
		return $num;
	}

	/**
	 * Generate a list of notices for All Notices page
	 *
	 * @return array of HTML code to be send to showRealtorGrid()
	 * @param int $spanSize col-lg-Size of grid box
	 * @internal param string $type account_type
	 * @return array
	 */
	function getGrid($spanSize=3) {
		$query = 'SELECT notice_id,assigned_to,notice_start,notice_end,
				notice,notice_update
				FROM notices
				WHERE notice_end >=?
				ORDER BY notice_update';
		$records = $this->db->getRecords($query, date("Y-m-d",time()));
		if (!$records) return 'No notices';
		$display = array();

		foreach ($records as $row) {
			extract($row);
			if ($assigned_to > 0) {
				$query2 = 'SELECT account_firstname
						FROM accounts
						WHERE accountId=?';
				$assigned = $this->db->getVar($query2,$assigned_to);
			} else {
				$assigned = "All";
			}
			$notice = substr(strip_tags($notice), 0, 40).'...';
			$display[] = <<<DISPLAY
<a href="notice_edit.php?id=$notice_id" class="list-group-item"><i class="icon-user"></i> $notice
<col-lg- class="badge">$assigned</col-lg->
</a>
DISPLAY;
		}
		$ui = new UI;
		return $ui->createGrid($display,$spanSize);
	}

	public static function getNotice() {
        $current_date = date("Y-m-d",time());
		$getNotice = Database::db(
			"SELECT notice FROM notices WHERE notice_start <='$current_date'
    AND notice_end > '$current_date'
    ORDER BY
     notice_start DESC LIMIT 0,1");
		if ($getNotice) {
			$notice = $getNotice[0];
			//return filter_var($notice, FILTER_SANITIZE_MAGIC_QUOTES, 'utf-8');
			return $notice;
		} else {
			return FALSE;
		}
	}

	/**
	 * If there is a notices,
	 * wrap it and show on site
	 * @return bool|string
	 */
	public static function showNotice() {
		$notice = self::getNotice();
		if ($notice !== FALSE) {
			return <<<HEREDOC
<div class="row">
<div class="alert alert-dismissible alert-warning">
<button type="button" class="close" data-dismiss="alert">×</button>
$notice
</div>
</div>
HEREDOC;
		} else {
			return FALSE;
		}
	}

	/**
	 * Are there any notices to show?
	 * Pulled from functions file
	 *
	 * @internal param db connection for spoon
	 * @return string
	 */
	function notices() {
		$noticeDisplay = FALSE;
		$current_account = (int)$_SESSION['id'];

		$query = 'SELECT notice FROM notices 
		    	WHERE notice_start <=CURDATE() 
		    	AND notice_end >=CURDATE() 
		    	AND (assigned_to=? OR assigned_to=?)
		    	ORDER BY notice_start DESC';
		$notice = $this->db->getRecords($query,array($current_account,0));
		if ($notice) {
			$noticeDisplay .= '<div class="alert alert-success">';
			foreach ($notice as $rows) {
				$noticeDisplay .= 	$rows['notice'].'<br>';
			}
			$noticeDisplay .= '</div>';
		}
		return $noticeDisplay;
	}

	/**
	 * Send a notification to a user
	 *
	 * @todo     The notice saved does not have the variables replaced
	 *
	 * @param            $notification
	 * @param string     $to       accountId
	 * @param            $lead_id
	 * @param int|string $length   Number of intervals
	 * @param string     $interval Type of interval = hour,day
	 * @param  date      $start    default is NULL or today
	 *                             Format is Y-m-d for db
	 * @internal param int $message_id message to send
	 * @internal param string $type email,notification,both
	 * @return boolean
	 */

	function notify($notification,$to,$lead_id,$length=2,$interval="day",$start=NULL) {

		$emails = new Emails;
		if ((int)$to > 0) {
			$admin = new Admin;
			$account_info = $admin->getNameEmail($to);
			extract($account_info);
		} else {
			return FALSE;
		}
		//notify
		$values2save = array();
		$values2save['assigned_to'] = $to;
		$values2save['lead_id'] = $lead_id;
		$values2save['notice'] = $notification;
		if (is_null($start)) {
			$start =  date('Y-m-d',time());
		}
		$values2save['notice_start'] = $start;
		if ($interval == "day") {
			$end = date("Y-m-d",time()+ (60*60*24*(int)$length));
		} elseif ($interval == "hour") {
			$end = date("Y-m-d",time()+ (86400)); //one day
		}
		$values2save['notice_end'] = $end;
		return $this->saveNotification($values2save);
	}
	/**
	 * Send an email to admin when a new issue is added
	 *
	 * @param  string $firstname
	 * @param  string $ititle
	 * @param  string $priority
	 * @param         $issueId
	 * @return boolean
	 */
	function notifyAdminNewIssue($firstname,$ititle,$priority,$issueId) {
		$emails = new Emails;
		$replacements = array();
		$replacements['firstname'] = $firstname;
		$replacements['ititle'] = $ititle;
		$replacements['priority'] = $priority;
		$replacements['link'] = SITE_URL.PATH_ADMIN.'/issues/respond.php?id='.$issueId;
		return $emails->sendMessageByEmail('Julaine Scott','work@julaine.ca',6,$replacements);
	}

	function notifyNewPassword($password,$accountId) {
		$admin = new Admin;
		$rows = $admin->getNameEmail($accountId);
		extract($rows);

		$emails = new Emails;
		$replacements = array();
		//$replacements['firstname'] = $account_firstname;
		$replacements['password'] = $password;
		return $emails->sendMessageByEmail($account_firstname,$account_email,3,$replacements);
	}
	function notifyIssueComment($firstname,$ititle,$priority,$issueId) {
		$emails = new Emails;
		$replacements = array();
		$replacements['firstname'] = $firstname;
		$replacements['ititle'] = $ititle;
		$replacements['priority'] = $priority;
		$replacements['link'] = SITE_URL.PATH_ADMIN.'/issues/respond.php?id='.$issueId;
		return $emails->sendMessageByEmail('Julaine Scott','work@julaine.ca',12,$replacements);
	}

	function removeNotices($lead_id) {
		return $this->db->delete('notices','lead_id=?', $lead_id);
	}

	/**
	 * Adjusted to be more compatible
	 * @param $values2save
	 * @return mixed
	 */
	function saveNotification($values2save) {
		extract($values2save);
		$notice_array = array('accountId' => Admin::getUserId(),
			'assigned_to' => $assigned_to,
			'notice' => $notice,
			'notice_start'=> date("Y-m-d",strtotime($notice_start)),
			'notice_end' => date("Y-m-d",strtotime($notice_end))
		);

		return $this->db->insert('notices', $notice_array);
	}
	/**
	 * Create form and code to Add a Notice
	 *
	 * @todo  Must make assignedTo a dropdown
	 *        Do I need one for realtor and one for lead?
	 *        What about broker?
	 *
	 */
	function showAddNoticeForm() {
		$realtors = new Realtors;

		$frm = new SpoonForm('notice_add');
		$frm->setParameter('class','form-horizontal');
		$frm->addTextarea('notice', NULL, 'textarea', '',TRUE) ;  //yes we want HTML

		$frm->addDate('notice_start', NULL, NULL, 'datepicker form-control')
			->setAttribute('required');
		$frm->addText('notice_end', NULL, NULL, 'datepicker form-control');

		// add hidden value(s)
		$frm->addHidden('process', '1');
		// add submit button
		$frm->addButton('submit', 'Save')
			->setAttribute('class', 'btn btn-primary btn-medium');

		if($frm->isSubmitted()) {
			$frm->getField('notice_start')->isFilled('Please provide a start date.');
			$frm->getField('notice')->isFilled('Please provide a notice message.');

			if($frm->isCorrect()){
				// all the information that was submitted
				$postedValues = $frm->getValues();
				extract($postedValues);
				$notice_array = array('accountId' => (int)$_SESSION['id'],
					'assigned_to' => $assigned_to_realtor,
					'notice' => $notice,
					'notice_start'=> date("Y-m-d",strtotime($notice_start)),
					'notice_end' => date("Y-m-d",strtotime($notice_end))
				);

				$notice_id = $this->db->insert('notices', $notice_array);

				if ($notice_id > 0) {
					$_SESSION['success'] = 'Thank you, the new notice was added.';

				} else {
					Bugs::setError('notify', 'Due to unforseen circumstances,
						the database did not save the notice. Please try again.');
				}
			} else {
				Bugs::setError('notify', 'Due to unforseen circumstances,
						the database did not save the notice. Please try again.');
			}

		}
		$tpl = new SpoonTemplate();
		$tpl->setForceCompile(true);
		$tpl->setCompileDirectory(PATH_ROOT.'cache');
		$frm->parse($tpl);
		$tpl->display(PATH_TEMPLATES.'notices/form_notice_add.tpl');
	}
	/**
	 * Show form for editing notices
	 *
	 * @param  int $id notice_id
	 * @return form and its code
	 */
	function showNoticeForm($id) {
		$find = 'SELECT notice_start,notice_end,notice 
				FROM notices 
				WHERE notice_id=?';
		$row = $this->db->getRecord($find,$id);
		if (!$row) return FALSE;
		if ($row['notice_start'] !== "0000-00-00") {
			$start_date = date("m/d/Y",strtotime($row['notice_start']));
		}
		if ($row['notice_end'] !== "0000-00-00") {
			$end_date = date("m/d/Y",strtotime($row['notice_end']));
		}
		// create form
		$frm = new SpoonForm('notices');
		$frm->setParameter('class','form-horizontal');
		$frm->addTextarea('notice', $row['notice'], 'textarea', '',TRUE)
			->setAttribute('required');  //yes we want HTML

		$frm->addText('notice_start', $start_date, 20, 'datepicker form-control');
		$frm->addText('notice_end', $end_date, 20, 'datepicker form-control');


		// add hidden value(s)
		$frm->addHidden('process', 1);
		$frm->addHidden('id', $id);
		// add submit button
		$frm->addButton('submit', 'Save')
			->setAttribute('class', 'btn btn-primary btn-medium');

		if($frm->isSubmitted()) {
			$frm->getField('notice_start')->isFilled('Please provide a start date.');
			// email is required
			$frm->getField('notice')->isFilled('Please provide a notice message.');
			// email is required
			if($frm->isCorrect()){
				// all the information that was submitted
				$postedValues = $frm->getValues();
				//extract($postedValues);
				// if ($assigned_to_realtor > 0) {
				// 	$assigned_to = $assigned_to_realtor;
				// } elseif ($assigned_to_lead > 0) {
				// 	$assigned_to = $assigned_to_lead;	
				// }
				$notice_array = array(
					'accountId' => (int)$_SESSION['id'],
					'assigned_to' => $postedValues['assigned_to_realtor'],
					'notice' => $postedValues['notice'],
					'notice_start'=> date("Y-m-d",strtotime($postedValues['notice_start'])),
					'notice_end' => date("Y-m-d",strtotime($postedValues['notice_end']))
				);
				$affected = $this->db->update('notices', $notice_array,'notice_id=?',$postedValues['id']);

				if ($affected > 0) {
					$_SESSION['success'] = 'Thank you, the notice was updated.';
				} else {
					Bugs::setError('notify', 'Due to unforseen circumstances,
						the database did not save the notice. Please try again.');

				}
				foreach($_POST as $key => $value) {
					unset($_POST[$key]);
				}
				header('Location: '.SITE_URL.PATH_ADMIN'notices.php');
				die;
			}
		}
		$tpl = new SpoonTemplate();
		$tpl->setForceCompile(true);
		$tpl->setCompileDirectory(PATH_ROOT.'cache');
		$frm->parse($tpl);
		$tpl->display(PATH_TEMPLATES.'notices/form_notice.tpl');
	}
	/**
	 *
	 */
	function showNotices($id=NULL,$type="list") {
		$display = array();
		if (is_null($id)) {
			$query = 'SELECT * FROM notices WHERE notice_end >=CURDATE()';
			$rows = $this->db->getRecords($query);
		} else {
			$query = 'SELECT * FROM notices WHERE assigned_to=? AND notice_end >=CURDATE()';
			$rows = $this->db->getRecords($query,$id);
		}
		if ($rows) {

			foreach ($rows as $values) {
				extract($values);
				// $realtors = new Realtors;
				//$name = $realtors->getRealtorName($assigned_to);
				if ((int)$assigned_to > 0) {
					$query2 = 'SELECT account_firstname
						FROM accounts
						WHERE accountId=?';
					$name = $this->db->getVar($query2,$assigned_to);
				} else {
					$name = "All";
				}

				$listings = new Listings;
				$address = $listings->showAddress($lead_id);


				$admin = new Admin;
				if ($admin->getAccess() == "Broker" || $admin->getAccess() == "Admin") {
					$notice = $notice.' <span class="label label-success">'.$name.'</span>';
					//$notice = str_replace(array("Please confirm your new lead ","."),array("$name has yet to confirm ",""),$notice);
				} else {
					//$notice = str_replace(".","",$notice);
				}
				/*
                <span class="time">
    $notice_start
    </span>
                 */
				if ($type == "list") {
					$display[] = <<<NOTICES
<li>$notice</li>
NOTICES;
				} elseif ($type == "p") {
					$display[] = <<<NOTICES
<p>$notice</p>
NOTICES;
				}
			}
		}
		if (sizeof($display) >= 1) {
			return '<div class="alert alert-block alert-warning alert-dismissable">
	          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	          '.join("\n",$display).'</div>';
		}
		return FALSE;
	}
}?>