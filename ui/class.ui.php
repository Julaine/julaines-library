<?php
/**
 * Standard UI component thingies
 * 
 * @author Julaine Scott
 * @version 1.0.5 
 * @package julaines-library
 */

/**
 * Class UI
 * @package julaines-library
 * @version 1.0.8 new methods
 */
class UI {
	/**
	 * check a string for a link
     *
     * @param $url
     * @return string
	 */
    public static function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
    /**
     * Did someone post to the form?
     * if so, filter the input and pass it back
     *
     * Creates $postedValues
     *
     * @return mixed
     */
    public static function check4Post()
    {
        $postedValues = filter_input_array(INPUT_POST, FILTER_SANITIZE_MAGIC_QUOTES);
        if (!isset($postedValues['me']) || $postedValues['me'] !== '') {
            return $postedValues;
        } else {
            return false;
        }
    }
    /**
     * Is my variable set?
     *
     * @return bool
     */
    public static function check4Process()
    {
        if (isset($_POST['process']) && (int)$_POST['process'] == 1) {
            return true;
        }

        return false;
    }
    /**
     * Was originally in Checkout
     * But useful everywhere
     *
     * @param $number
     * @return string
     */
    public static function convert2Currency($number){
        //setlocale(LC_MONETARY, 'en_CA');
        //return money_format('%n', $number);
        return '$'.number_format($number,2);
    }
	/**
	 * Organize the grid items into a proper grid
	 * 
	 * @return string HTML for the grid
	 * @param array $display Contains all the individual grid elements
	 *
	 * @todo Fix this function as the $j and the col-lg-Size are not correct
	 */
	function createGrid($display) {
		if (!is_array($display)) return FALSE;
		$showGrid = array();
		$numItems = sizeof($display);
		$showGrid[] = '<div class="list-group">';
		for ($i=0; $i <$numItems; $i++) {
			$showGrid[] = $display[$i];
		}
		$showGrid[] = '</div><!--end of list-group-->';
		return join("\n", $showGrid);
	}
    /**
     * Organize the grid items into a proper grid
     *
     * @return string HTML for the grid
     * @param array $display Contains all the individual grid elements
     *
     * @todo Fix this function as the $j and the col-lg-Size are not correct
     */
    function createListGroup($display) {
        if (!is_array($display)) return FALSE;
        $showGrid = array();
        $numItems = sizeof($display);
        $showGrid[] = '<ul class="list-group">';
        for ($i=0; $i <$numItems; $i++) {
            $showGrid[] = '<li class="list-group-item">'.$display[$i].'</li>';
        }
        $showGrid[] = '</ul>';
        return join("\n", $showGrid);
    }
	/**
	 * Added to format dates on offers pages
	 * 
	 * @param  string $date Raw Date
	 * @return string 		Clean Date
	 */
    public static function checkDate($date) {
		$clean_date = '0000-00-00';	
		if ($date == '1969-12-31' || $date == 'Dec 31, 1969') {
			$clean_date = '0000-00-00';
		} else {
			if ($date !== '0000-00-00') {
				$clean_date = date("Y-m-d",strtotime($date));
			} 
		}
		if ($clean_date !== '1969-12-31') {
			return $clean_date;
		} else {
			return '';
		}
	}

    /**
     * Convert Smart Quotes to straight quotes
     *
     * @param $string
     * @return mixed
     */
    public static function convert_smart_quotes($string) {
        /*
        * @author Chris Shiflett.
        * @link http://shiflett.org/blog/2005/oct/convert-smart-quotes-with-php
        * @version 1.0
        */
        $search = array(chr(145),
            chr(146),
            chr(147),
            chr(148),
            chr(151));

        $replace = array("'",
            "'",
            '"',
            '"',
            '-');

        return str_replace($search, $replace, $string);
    }

    /**
     *
     * Create a select box
     *
     * Get the information from the database
     * and put it in a list of options
     *
     * @param        $Sname
     * @param        $table
     * @param        $field1
     * @param        $field2
     * @param        $keyterm
     * @param        $svalue
     * @param string $select_id
     * @return string
     */
    public static function createSelect($Sname, $table, $field1, $field2, $keyterm, $svalue, $select_id="") {
        $displayProjects = array();
        if (empty($keyterm)) {
            $functionSql = "SELECT $field1,$field2 FROM $table ORDER BY $field2";
        } else {
            $functionSql = "SELECT $field1,$field2 FROM $table WHERE $keyterm='$svalue' ORDER BY $field2";
        }
        $functionResults = Database::db($functionSql);
        $displayProjects[] = '<select name="'.$Sname.'" class="form-control">';
        $displayProjects[] = '<option value="0"> </option>';
        foreach ($functionResults as $row) {
            $key12 = $row["$field1"];
            $value12 = $row["$field2"];
            if ($key12 == $select_id) {
                $displayProjects[] = '<option value="'.$key12.'" selected>'.$value12;
            } else {
                $displayProjects[] = '<option value="'.$key12.'">'.$value12;
            }
            $displayProjects[] = "</option>";
        }
        $displayProjects[] = "</select>\n";
        return join("\n",$displayProjects);
    }

    /**
     * Create a dropdown
     *
     * It should consist of options
     * from a supplied array of values
     *
     * @param       $name
     * @param array $options
     * @param null  $selected
     * @param null  $spanSize
     * @return string
     */
	public static function dropdown($name, array $options, $selected=NULL, $spanSize=NULL)	{
		//added 25Jun12 for custom_orders
		//removed id="'.$name.'"
		$dropdown = '<select name="'.$name.'" ';
		if (!is_null($spanSize)) { $dropdown .= ' class="form-control"'; }
		$dropdown .= ">\n";
		$selected2 = $selected;
		foreach($options as $key=>$option) {
			$select = $selected2==$key ? ' selected' : NULL;
			$dropdown .= '<option value="'.$key.'"'.$select.'>'.$option.'</option>'."\n";
		}
		$dropdown .= '</select>'."\n";
		return $dropdown;
	}

    /**
     * Count the words in a string
     * and return by a set amount
     * with an option tail
     *
     * New 2014-11-12
     *
     * @param        $string
     * @param        $num
     * @param string $tail
     * @return string
     */
    public static function first_words($string, $num, $tail='&nbsp;...') {
        $words = str_word_count($string, 2);

        $firstwords = array_slice( $words, 0, $num);
        return  implode(' ', $firstwords).$tail;
    }
	/**
	 * Prepare HTML code for modals
	 * Do not use modal-dialog with Modal Manager
	 * 
	 * @param  int $id       
	 * @param  string $title    
	 * @param  string $contents 
	 * @return string
	 */
	function generateModal($id,$title,$contents=NULL) {
  		return <<<HEREDOC
<div class="modal container fade" id="$id" role="dialog" aria-labelledby="$id" aria-hidden="true">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">$title</h4>
      </div>      
        <div class="{$id}_modal-body modal-body">$contents       
        </div>
          
    </div><!-- /.modal-content -->
</div><!-- /.modal -->  
HEREDOC;
	}
    /**
     * @return array of HTML code to be send to createGrid()
     * @param int $spanSize col-lg-Size of grid box
     */
    function getGrid($spanSize=3) {
        $query = 'SELECT message_id,message,message_to,message_from,
				message_subject,message_format
				FROM messages
				ORDER BY message_subject';
        $records = $this->db->getRecords($query);
        if (!$records) return FALSE;
        $display = array();

        foreach ($records as $row) {
            extract($row);
            $display[] = <<<HEREDOC
<li class="list-group-item"><a href="message_edit.php?id=$message_id">
	$message_subject</a>
	<col-lg- class="badge">$message_id</col-lg->
</li>
HEREDOC;
        }
        return self::createGrid($display,$spanSize);
    }

    /**
     * Remove punctuation from a string
     * @param $string
     * @return mixed|string
     */
    public static function strip_punctuation($string) {
        $string = strtolower($string);
        $string = filter_var($string, FILTER_SANITIZE_NUMBER_INT);
        return $string;
    }
    /**
     * Check for injection characters
     * @param $s
     * @return string|null
     */
    public static function injection_chars($s) {
        // returns TRUE if 'bad' characters are found
        return (preg_match("\r", $s) || preg_match("\n", $s) || preg_match("%0a", $s) || preg_match("%0d", $s)) ? TRUE : FALSE;
    }

    /**
     * Clean Phone number
     * and show it pretty like
     *
     * @param $data
     * @return null|string
     */
    public static function phone_clean($data){
// Cleans phone numbers and strips extensions
// Returns array [number,extension]
        $pattern = '/\D*\(?(\d{3})?\)?\D*(\d{3})\D*(\d{4})\D*(\d{1,8})?/';
        if (preg_match($pattern, $data, $match)){
            if ($match[3]){
                if ($match[1]){
                    $num = $match[1].'-'.$match[2].'-'.$match[3];
                }
                else{
                    $num = $match[2].'-'.$match[3];
                }
            }

            else{
                $num = NULL;
            }
            //$match[4] ? $ext = " x".$match[4] : $ext = NULL;
        }
        else{
            $num = NULL;
            //$ext = NULL;
        }
        //$clean = array($num);
        return $num;
    }
    /**
     * Used for rounding up numbers
     *
     * @param $subtotal
     * @return float
     */
    public static function roundUp($subtotal) {
        return round($subtotal, 2);//, PHP_ROUND_HALF_UP);
    }

    /**
     * Clean strings
     * like from forms
     *
     * @param $string
     * @return string
     */
    public static function sanitizeString($string) {
        $string = strip_tags($string);
        return htmlentities($string);
    }
    /**
     * Remove colons
     * from a string
     * Useful for forms
     *
     * @param $s
     * @return mixed
     */

    public static function strip_colons($s) {
        return str_replace(array(':', '%3a'), " ", $s);
    }

    /**
     * Move to class images
     *
     * Is an image path an image?
     *
     * @param $image_path
     * @return bool|string
     */
    public static function is_image($image_path)    {
        if (!$f = fopen($image_path, 'rb')) {
            return false;
        }

        $data = fread($f, 8);
        fclose($f);

        // signature checking
        $unpacked = unpack("H12", $data);
        if (array_pop($unpacked) == '474946383961' || array_pop($unpacked) == '474946383761') return "gif";
        $unpacked = unpack("H4", $data);
        if (array_pop($unpacked) == 'ffd8') return "jpg";
        $unpacked = unpack("H16", $data);
        if (array_pop($unpacked) == '89504e470d0a1a0a') return "png";

        return false;
    }
	/**
	 * Make a static file after deleting it first
     *
	 * 
	 * @return boolean
	 * @param string $destinationFilename full path of file to make
	 * @param string $newFileContents stuff to put in file
     * @todo switch to new one in class.files
	 */
	public static function updateFile($destinationFilename,$newFileContents) {
        $SpoonFile = new SpoonFile;
		$SpoonFile->delete($destinationFilename);
		return $SpoonFile->setContent($destinationFilename, $newFileContents, $createFile = true, $append = false, $chmod = 0755);
	}

    /**
     * Fix problem with extra slashes
     * @param $string
     * @return string
     */
    public static function removeSlashes($string)  {
        $string=implode("",explode("\\",$string));
        $string=implode("",explode("\\",$string));
        return stripslashes(trim($string));
    }
}
?>