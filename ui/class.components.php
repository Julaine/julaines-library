<?php
/**
 * I decided to create this because the PHP
 * include page has advice that it is slow
 * and insecure to include() files
 *
 */
/**
 * Class Components
 *
 * @package julaine
 * @version 0.0.1
 *
 */
Class Components {
    /**
     * if I have a default filename extension
     * constatn like I want to
     * I could use it for the file includes
     *
     * @since 0.0.1
     * @param $name
     * @return file guts
     */
    public static function get($name) {
        $file = PATH_COMPONENTS.$name.".php";

        /*
        if (SpoonFile::exists($file)) {
            return include($file);
        } else {
            return "cannot find ".$file;
        }
        */
        include("$file");

    }
}
?>