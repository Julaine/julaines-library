<?php
/**
 * I had this as a page on my site
 * converted to a class Jan 2015
 *
 * @package julaines-library
 */
/**s
 * Class LinkChecker
 * @package julaines-library
 * @version 0.0.1
 */
class LinkChecker {
    /**
     * Consider changing to use globals
     * SITE_PATH, etc
     *
     *
     * @param $url
     * @return mixed
     */
    function geturls($url) {
        $info = parse_url($url);
        $fp = @fsockopen($info["host"], 80, $errno, $errstr, 10);
        if (!$fp) {
        } else {
            if(empty($info["path"])) {
                $info["path"] = "/";
            }
            if(isset($info["query"])) {
                $query = $info["query"];
            } else {
                $query = "";
            }
            $out  = "GET ".$info["path"]."".$query." HTTP/1.1\r\n";
            $out .= "Host: ".$info["host"]."\r\n";
            $out .= "Connection: close \r\n";
            $out .= "User-Agent: free-phpdotorgdotuk link checker/1.0\r\n\r\n";
            fwrite($fp, $out);
            $html = '';
            while (!feof($fp)) {
                $html .= fread($fp, 8192);
            }
        }
        fclose($fp);
        $pieces = explode("\r\n\r\n", $html,2);
        $html =  $pieces[1];
        unset($pieces);
        $types = array("href", "src", "url");
        while(list(,$type) = each($types)) {
            preg_match_all ("|$type\=\"?'?`?(http://[[:alnum:]:?=&@/;._-]+)\"?'?`?|i", $html, $matches);
            $matches[1] = str_replace("&amp;","&",$matches[1]);
            $ret[$type] = $matches[1];
        }
        return $ret;
    }

    /**
     * Get the page's header
     *
     * @param $url
     * @return mixed
     */
    function getheaders($url) {
        $info = parse_url($url);
        $fp = @fsockopen($info["host"], 80, $errno, $errstr, 10);
        if (!$fp) {
            print "Error: that url you entered seems not to exist.\n";
        } else {
            if(empty($info["path"])) {
                $info["path"] = "/";
            }
            if(isset($info["query"])) {
                $query = $info["query"];
            } else {
                $query = "";
            }
            $out  = "GET ".$info["path"]."".$query." HTTP/1.1\r\n";
            $out .= "Host: ".$info["host"]."\r\n";
            $out .= "Connection: close \r\n";
            $out .= "User-Agent: free-phpdotorgdotuk link checker/1.0\r\n\r\n";
            fwrite($fp, $out);
            $html = '';
            while (!feof($fp)) {
                $html .= fread($fp, 8192);
            }
            fclose($fp);
        }
        $pieces = explode("\r\n\r\n", $html,2);
        $headerinfo =  $pieces[0];
        unset($pieces);
        return $headerinfo;
    }

    /**
     * Compare header code
     * to list of knowns
     *
     * @param $header
     * @return string
     */
    function getsatuscode($header) {
        if (preg_match("/http\/[0-9]+\.[0-9]+ (200) [a-z0-9]+/i", $header)){     $status = "200 OK";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (404) [a-z0-9]+/i", $header)) {     $status = "404 Not Found";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (505) [a-z0-9]+/i", $header)) {     $status = "505 HTTP Version Not Supported";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (504) [a-z0-9]+/i", $header)) {     $status = "504 Gateway Timeout";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (503) [a-z0-9]+/i", $header)) {     $status = "503 Service Unavailable";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (502) [a-z0-9]+/i", $header)) {     $status = "502 Bad Gateway";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (501) [a-z0-9]+/i", $header)) {     $status = "501 Not Implemented";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (500) [a-z0-9]+/i", $header)) {     $status = "500 Internal Server Error";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (417) [a-z0-9]+/i", $header)) {     $status = "417 Expectation Failed";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (100) [a-z0-9]+/i", $header)) {     $status = "100 Continue";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (101) [a-z0-9]+/i", $header)) {     $status = "101 Switching Protocols";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (201) [a-z0-9]+/i", $header)) {     $status = "201 Created";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (202) [a-z0-9]+/i", $header)) {     $status = "202 Accepted";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (203) [a-z0-9]+/i", $header)) {     $status = "203 Non-Authoritative Information";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (204) [a-z0-9]+/i", $header)) {     $status = "204 No Content";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (205) [a-z0-9]+/i", $header)) {     $status = "205 Reset Content";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (206) [a-z0-9]+/i", $header)) {     $status = "206 Partial Content";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (300) [a-z0-9]+/i", $header)) {     $status = "300 Multiple Choices";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (301) [a-z0-9]+/i", $header)) {     $status = "301 Moved Permanently";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (302) [a-z0-9]+/i", $header)) {     $status = "302 Found";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (303) [a-z0-9]+/i", $header)) {     $status = "303 See Other";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (304) [a-z0-9]+/i", $header)) {     $status = "304 Not Modified";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (305) [a-z0-9]+/i", $header)) {     $status = "305 Use Proxy";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (307) [a-z0-9]+/i", $header)) {     $status = "307 Temporary Redirect";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (400) [a-z0-9]+/i", $header)) {     $status = "400 Bad Request";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (401) [a-z0-9]+/i", $header)) {     $status = "402 Unauthorized";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (402) [a-z0-9]+/i", $header)) {     $status = "403 Payment Required";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (403) [a-z0-9]+/i", $header)) {     $status = "404 Forbidden";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (405) [a-z0-9]+/i", $header)) {     $status = "405 Method Not Allowed";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (406) [a-z0-9]+/i", $header)) {     $status = "406 Not Acceptable";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (407) [a-z0-9]+/i", $header)) {     $status = "407 Proxy Authentication Required";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (408) [a-z0-9]+/i", $header)) {     $status = "408 Request Timeout";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (409) [a-z0-9]+/i", $header)) {     $status = "409 Conflict";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (410) [a-z0-9]+/i", $header)) {     $status = "410 Gone";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (411) [a-z0-9]+/i", $header)) {     $status = "411 Length Required";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (412) [a-z0-9]+/i", $header)) {     $status = "412 Precondition Failed";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (413) [a-z0-9]+/i", $header)) {     $status = "413 Request Entity Too Large";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (414) [a-z0-9]+/i", $header)) {     $status = "414 Request-URI Too Long";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (415) [a-z0-9]+/i", $header)) {     $status = "415 Unsupported Media Type";
        } elseif (preg_match("/http\/[0-9]+\.[0-9]+ (416) [a-z0-9]+/i", $header)) {     $status = "416 Requested Range Not Satisfiable";
        } else {
            $status = "Unknow: means either error in the script, headers or the url dose not exist.";
        }
        return $status;
    }
}
?>