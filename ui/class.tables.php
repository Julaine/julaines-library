<?php
/**
 * create an html table
 *
 * I created this class to manage the tedious task of creating tables
 * I like its usage and decided to merge into the julaine library
 *
 * @package julaines-library
 */
/**
 * Class EditTables
 * 
 * @package julaines-library
 * @version 	1.1.1 Added tfoot and cleanup
 *           removed makeCellAddressLong
 */
class EditTables {
	/**
	 * End the table
	 * @return string
	 */
	public static function endTable() {
		return '</tbody></table>';
	}
	/**
	 * Add contents of a cell to array for a table
	 * 
	 * @param  $addressValues
	 * @return string HTML code of table cell
	 */
	public function makeCellAddress($addressValues) {
		extract($addressValues);
		return "<address>$address<br>$city, $province<br>$pcode</address>\n";
	}
	/**
	 * Editable cell that contains a date
	 *
	 * Relies on an icon class
	 * works with icon-*
	 *
	 * @todo should I change to an input-addon span?
	 *
	 * @param $date_string
	 * @return bool|string
	 */
	public static function makeCellDate($date_string) {
		if ($date_string == '0000-00-00') {
			$date = '+ <i class="icon-calendar"></i>';
		} else {
			$date = date('M d,y',strtotime($date_string));
		}
		return $date;
	}

	/**
	 * creates the <thead>
	 *
	 * @param $headers
	 * @return bool|string
	 */
	public static function makeCellHeading($headers) {
		if (!is_array($headers)) return FALSE;
		$display = '<thead><tr>';
		foreach ($headers as $name) {
			$display .= "<th>$name</th>\n";
		}
		$display .= '</tr></thead><tbody>';
		return $display;
	}

	/**
	 * Start a table
	 *
	 * Relies on Bootstrap
	 * would need to expand if using a table plugin
	 * like tablecloth or DataTables
	 *
	 * I use sample_6 for DataTables
	 *
	 * @param null $class
	 * @param null $id
	 * @return string
	 */
	public static function startTable($class=NULL,$id=NULL) {
		if (!is_null($id)) {
			return '<table class="table '.$class.' table-hover" id="'.$id.'">';
		} else {
			return '<table class="table '.$class.' table-hover">';
		}
	}

	/**
	 * Wrap a string with a link tag in a table cell
	 *
	 *
	 * @param $link
	 * @param $string
	 * @return bool|string
	 */
	public static function wrapAInCell($link,$string) {
		if (!isset($string)) return FALSE;
		return <<<HEREDOC
<td><a href="$link">$string</a></td>
HEREDOC;
	}

	/**
	 * Wrap a string in a table cell
	 *
	 * @param $string
	 * @return bool|string
	 */
	public static function wrapInCell($string) {
		if (isset($string)) {
			return "<td>$string</td>";
		}
		return FALSE;
	}

	/**
	 * Decided not to use \n on this one
	 * I should be adding that with the join method
	 *
	 * @since 1.0.4
	 * @param $string
	 * @return bool|string
	 */
	public static function wrapInRow($string) {
		if (isset($string)) {
			return "<tr>$string</tr>";
		}
		return FALSE;
	}

	/**
	 * Add a footer to the table
	 * @param $footer
	 * @return bool|string
	 */
	public static function makeTableFooter($footer) {
		if (!is_array($footer)) return FALSE;
		$display = '<tfoot><tr>';
		foreach ($footer as $name) {
			$display .= "<th>$name</th>\n";
		}
		$display .= '</tr></tfoot>';
		return $display;
	}
}
?>