<?php
/**
 * @package julaines-library
 * @version 2.0.5
 * @uses 		Spoon
 *
 * Setup standard db tables
 * Changed to opened and closed and added assigned
 * added severity and removed ranking
 */
class Tracking {
	const adminPerson = "Julaine Scott <work@julaine.ca>";
	const from = "name <email>";
	private $status = array('Reported', 'Fixed', 'In Progress', 'Questions', 'Wish List', 'On Hold', 'Needs Testing');
	protected $db;
	protected $today;

	/**
	 * Setup a db connection using spoon
	 * Generate today's date
	 */
	function __construct() {
		// create database object
		$this->db = new SpoonDatabase(DB_TYPE, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		$this->today = date("Y-m-d",time());
	}
	/**
	 * Removed from inside form
	 * Need it independent so I can call it 
	 * when debugging
	 * 
	 * @param array $postedValues 
     * @return int
	 */
	function addIssue($postedValues) {
		if (!is_array($postedValues)) return FALSE;
		extract($postedValues);
		$id = Admin::getUserId();
		$issues = array('opened' => $id,
					'ititle' => $ititle, 
					'idescription' => $idescription,
					'iurl' => $iurl,
					'isection' => 'All',
					'openDate' => date("Y-m-d",time()),  //1
					'status' => 'Reported',
					'type' => $type,
					'priority' => $priority,
					'lastUser' => $id); 
		
		$issue_id = $this->db->insert('issues',$issues);
		return $issue_id;
	}
	/**
	 * Add Issue Form
	 */
	function addIssueForm() {
		// create form
		$frm = new SpoonForm('issue_add'); 
		$frm->setParameter('class','form-horizontal');
		$frm->setAction('report.php');
		$frm->addText('ititle', NULL, 100, 'form-control')
			->setAttribute('required')
			->setAttribute('placeholder', 'Short description'); 
		$frm->addTextarea('idescription', NULL, 'textarea form-control')
			->setAttribute('placeholder', 'Long description'); 
		$frm->addText('iurl', NULL, 200, 'form-control')
			->setAttribute('type', 'text') 
			->setAttribute('placeholder', 'URL or page name'); 
		//isection is dropdown
		//$frm->addDropdown('isection', $this->sections, NULL, FALSE, 'form-control');
		//type
		$type_choices = array('Bug','Task','Request','Improvement');
		$frm->addDropdown('type', $type_choices, 'Bug', FALSE, 'form-control');
		//priority
		$priority_choices = array();
		$priority_choices['Urgent'] = 'Urgent';
		$priority_choices['Important'] = 'Important';
		$priority_choices['Normal'] = 'Normal';
		$frm->addDropdown('priority', $priority_choices, 'Normal', FALSE, 'form-control');

		// add hidden value(s)
		$frm->addHidden('process', 1);
		// add Save button
		$frm->addButton('submit', 'Add')
			->setAttribute('class', 'btn btn-primary');

		if($frm->isSubmitted()) {
			$frm->getField('ititle')->isFilled('Please provide a name.');		
			if($frm->isCorrect()){
				// all the information that was submitted
				$postedValues = $frm->getValues();
				extract($postedValues);

				$issue_id = $this->addIssue($postedValues);
				if ($issue_id > 0) {
					$admin = new Admin;
					//$emails = new Emails;
					$notices = new Notices;
					//notify Julaine of the new issue
					$firstname = $admin->getName();
					if ($notices->notifyAdminNewIssue($firstname,$ititle,$priority,$issue_id) == TRUE) {
						$_SESSION['success'] = 'Thank you, the new issue was added.';
					} else {
					
						Bugs::setError('track', 'Failed to send email to Julaine when creating new issue ('.$issue_id.')');
					}
				} else {
					Bugs::setError('notify', 'Did not create a lead');
				}	

				foreach($_POST as $key => $value) {
					unset($_POST[$key]);
				}
				header('Location: index.php');
				die;

			} else {
				Bugs::setError('errors', 'Did not create an issue');
			}
		} 
		$tpl = new SpoonTemplate();
		$tpl->setForceCompile(true);
		$tpl->setCompileDirectory(PATH_ROOT.'cache');
		$frm->parse($tpl);
		$tpl->display(PATH_SITE.'projectAdmin/tracking/form_issue_add.tpl');
	}

	/**
	 * Generate a list of issues
	 *
	 * @param string $type2show
	 * @return string
	 */
	function findIssues($type2show="Outstanding") {		
		if ($type2show !== "Outstanding") {
			$query = 'SELECT i.issueId,i.ititle,i.idescription,i.status,i.type,i.priority,a.account_firstname,
						i.lastAccessed 
						FROM issues AS i
						LEFT JOIN accounts AS a ON a.accountId=i.lastUser
						WHERE i.status=?
						ORDER BY i.lastAccessed DESC';
			$find = $this->db->getRecords($query,$type2show);
		} else {
			$query = 'SELECT i.issueId,i.ititle,i.idescription,i.status,i.type,i.priority,a.account_firstname,
					i.lastAccessed 
					FROM issues AS i
					LEFT JOIN accounts AS a ON a.accountId=i.lastUser
					WHERE i.status !=? 
					AND i.status !=? 
					AND i.status !=?
					ORDER BY i.lastAccessed DESC';
			$find = $this->db->getRecords($query,array('Fixed','On Hold','Wish List'));

		}
		if ($find) {
			$tables = new EditTables;
			if ($type2show !== "Outstanding") {
				echo "<h5>$type2show Issues</h5>";
			}
			echo $tables->startTable($class="table-condensed table-striped",$type2show);		
			echo $tables->makeCellHeading(array('#','Issue', 'Date'));	

			foreach ($find as $row) {				
				echo "<tr>";
				extract($row);
				if ($account_firstname == "") {
					$account_firstname = $this->getLastCommentFrom($issueId);
				}
				$last = NULL;
				if ($lastAccessed !== '0000-00-00 00:00:00') {
					//if today, show time
					//if not today show date
					if (substr($lastAccessed, 0,10) == $this->today) {
						$last = '<b>'.date("g:iA",strtotime($lastAccessed)).'</b>';

					} else {
						$last = '<b>'.date("M-d-y",strtotime($lastAccessed)).'</b>';
					}
				}
				echo $tables->wrapInCell(str_pad($issueId,2,0,STR_PAD_LEFT));
				//echo $tables->wrapInCell($account_firstname);
				$issue_desc = NULL;
				if ($priority == "Important") {
					//echo $tables->wrapInCell('<i class="fa fa-star"></i> ');
					$issue_desc .= '<i class="fa fa-star"></i> ';
				} elseif ($priority == "Urgent") {
					//echo $tables->wrapInCell('<i class="fa fa-flag"></i> ');
					$issue_desc .= '<i class="fa fa-flag"></i> ';
				} 
				if ($type == "Improvement") {
					$issue_desc .= '<i class="fa fa-plus-sign-alt"></i> ';
				} elseif ($type == "Bug") {
					$issue_desc .= '<i class="fa fa-bug"></i> ';
				}
				$issue_desc .= '<strong>'.stripslashes($ititle).'</strong> - <small>'.strip_tags(SpoonFilter::htmlentitiesDecode(substr($idescription,0,120), null, $quoteStyle = ENT_NOQUOTES)).'...</small>';				
				echo $tables->wrapAInCell('respond.php?id='.$issueId,$issue_desc);
				echo $tables->wrapInCell($last);
				echo "</tr>";
			}		
			echo $tables->endTable();
		} else {
			return "There are no issues";

		}
	}
	/**
	 * @uses NONE
	 * 
	 * @param  $issueId
	 * @return string
	 */
	function findIssueOpener($issueId) {
		$query = 'SELECT a.firstname AS from_name,a.email AS from_email
						FROM accounts AS a
						LEFT JOIN issues AS i ON a.accountId=i.opened
						WHERE i.issueId=?';
		$findOpener = $this->db->getRecords($query,$issueId);
		if ($findOpener) {
			foreach ($findOpener as $row) {
				extract($row);
				return $from_name.' <'.$from_email.'>';	
			}
		}		
		
	}
	/**
	 * @uses NONE
	 * @param  [type] $lastUserId [description]
	 * @return string
	 */
	function findLastUser($lastUserId) {
		$query = 'SELECT firstname AS from_name,email AS from_email
					FROM accounts 
					WHERE accountId=?';
		$last = $this->db->getRecords($query,$lastUserId);
		if ($last) {
			foreach ($findOpener as $row) {
				extract($row);
				return $from_name.' <'.$from_email.'>';	
			}
		}				
	}
	/**
	* I have two versions
	* one uses opened and the  other comment_by
	*/
	function getLastCommentFrom($issueId) {
		$query_last = 'SELECT a.account_firstname FROM comments AS c
									LEFT JOIN accounts AS a
									ON a.accountId=c.comment_by
									WHERE c.commentTypeId=? AND c.commentType=? 
									ORDER BY c.commentId DESC LIMIT 0,1';
		return $account_firstname = $this->db->getVar($query_last,array($issueId,'issue'));
	}
	function newCommentEmail($issueId,$lastUserId=1,$debug=NULL) {
		$findInfo = Database::db("SELECT ititle,status,priority FROM issues WHERE issueId='$issueId'",$debug);
		if (!$findInfo) {
			return FALSE;
		}
		foreach ($findInfo as $row) { 
			extract($row);	
		}	
		$firstPerson = $this->findIssueOpener($issueId);
		$lastPerson = $this->findLastUser($lastUserId);
		
		if ($lastUserId == 1) { //ie not Julaine
			if ($firstPerson !== $lastPerson) { $sendTo = $firstPerson; }
		} else {
			//someone at epc makes a comment			
			if ($status == "Fixed") {						
				if ($firstPerson !== $lastPerson) {
					//if someone other than the opener closes the issue,
					$sendTo = $firstPerson;
				} //else do nothing
			} else {
				$sendTo = self::adminPerson;
				
				if ($firstPerson == $lastPerson) {
					//do nothing 
				} else {
					$sendTo = $firstPerson;
					//someone other than the opener is adding a comment
				}
			}
		}
		
		if ($status == "Fixed") {						
			$saveStatus = Database::db("UPDATE issues SET closed='$lastUserId',closeDate=NOW(),
							status='$status',lastUser='$lastUserId',lastAccessed=NOW() 
							WHERE issueId='$issueId'");
			$Message = "<p>Issue ".stripslashes($ititle)." was closed.";
			//override existing settings for this instance
		} else {
			$saveStatus = Database::db("UPDATE issues SET lastUser='$accountId',status='$status',lastAccessed=NOW()
							WHERE issueId='$issueId'");
			$Message = "<p>Updated Issue ".stripslashes($ititle).".";
		}
		$Message .= '<p><a href="'.SECUREURL.'/epcAdmin/tracking/respond.php?id='.$issueId.'">View Issue</a>.';
		$subject = "Updated issue on the e-patches and crests website.";		 
		return $this->emailNotification($issueId,$Message,$subject,$sendTo); 
	}
	/**
	 * @todo Switch to $emails->sendEmail()
	 * 		Keep the function name and change the guts
	 *		use it for both new issues and comments on issues
	 * 
	 * @param  [type] $issueId [description]
	 * @param  [type] $Message [description]
	 * @param  [type] $subject [description]
	 * @param  [type] $sendTo  [description]
	 * @return boolean
	 */
	
	function emailNotification($issueId,$Message,$subject,$sendTo) {
		include('Mail.php');
		include('Mail/mime.php');
		$headers["From"] = self::from;
		$headers["To"]    = $sendTo; 
		$headers["Bcc"]    = "Julaine Scott <work@julaine.ca>"; 
		$headers["Subject"] = $subject; 			
		$crlf = "\n"; 
		$mime = new Mail_mime($crlf); 
		$mime->setHTMLBody($Message);
		$body = $mime->get();
		$hdrs = $mime->headers($headers);
		$mime =& Mail::factory('mail');			
		if($mime->send($sendTo, $hdrs, $body)) {
			return TRUE;	
		} else {

			Bugs::setError('debug',"failed to send email when new issue addded");
			$_SESSION['errors'] = 'I am sorry, am error occurred sending the email notification.';
			return FALSE;
		}	
	}	
	/**
	* @todo Can I change db. Use lastUser for closed
	* 		then I could just use one array for saving to db
	*		and not need to differentiate by status?
	*/
	function changeIssue($id,$postedValues,$debug) {
		//what is in postedValues?
		//must I prepare the array instead?
		$issues = array();
		$issues['closed'] = $accountId; //only for closed issues.
		$issues['closeDate'] = $this->today;
		$issues['status'] = $status;
		$issues['lastUser'] = $accountId;
		$issues['lastAccessed'] = date("Y-m-d h:i:a",time());
		$rows = $this->db->update("issues","issueId=?",$id,$postedValues);
		if ($rows > 0) {
			if ($postedValues['status'] == 'Fixed') {
				$Message = "<p>Issue ".stripslashes($ititle)." was closed by $firstname.";
			} else {
				$Message = "<p>$firstname added a comment to Issue ".stripslashes($ititle).".";
			}
			return $Message;
		} else {
			Bugs::setError("debug","ERROR updating issue $id");
			return FALSE;
		}
	}	
	/**
	 * Examine the usefulness of this function
	 */
	function uploadImageFile($uploadedFile,$destination,$destinationFolder,$debug=0) { 
	     //$uploadedFile is in the form of $_FILES['nameHere']
		$fileatt      = $uploadedFile['tmp_name'];
		$fileatt_type = $uploadedFile['type'];
		$fileatt_name = $uploadedFile['name'];
		$file_ext = strrchr($fileatt_name, '.');
		$newFilename1 = str_replace("'", "", $fileatt_name);
		$newFilename = str_replace(" ", "_", $newFilename1);    
		if (is_uploaded_file($fileatt)) {
			$file_ext = strrchr($fileatt_name, '.');
			$valid_range = array(".gif",".png",".zip",".sit",".pdf",".jpg",".doc",".docx",".tif",".tiff",".epc",".sea",".bmp",".GIF",".PNG",".ZIP",".SIT",".PDF",".JPG",".DOC",".DOCX",".TIF",".TIFF",".EPC",".SEA",".BMP");
			if (in_array($file_ext,$valid_range)) {
				Bugs::setError('debug',"destination $destination and destinationFolder $destinationFolder for $fileatt");
				chmod($destinationFolder, intval(0777));
				if (move_uploaded_file($fileatt, $destination)) { 
					chmod($destination, intval(0755));
					$uidnumber = 1447;
					chown($destination,intval($uidnumber));	
					chmod($destinationFolder, intval(0755));			
					return TRUE;
				} else {
					Bugs::setError('track',"An error occurred saving the link to the uploaded file, $destination");
					return FALSE;
				}
			} else {
				Bugs::setError("debug","The file uploaded to $source does not have a valid extension. It is $file_ext
				.");
				return FALSE;
			}
		} else {
			Bugs::setError("debug","There is no uploaded file.");
			return FALSE;
		}         
	}
	/**
	 * added here from the functions file
	 */
	function uploadIssueImage($uploadedFile,$issueId,$commentId,$debug=0) {
		 //$uploadedFile is in the form of $_FILES['nameHere']
		$fileatt      = $uploadedFile['tmp_name'];
		$fileatt_type = $uploadedFile['type'];
		$fileatt_name = $uploadedFile['name'];
		$file_ext 	  = strrchr($fileatt_name, '.');
		$newFilename1 = str_replace("'", "", $fileatt_name);
		$newFilename  = $issueId."_".str_replace(" ", "_", $newFilename1);
		
		if (is_uploaded_file($fileatt)) {
			// Read the file to be attached ('rb' = read binary)
			$file_ext = strrchr($fileatt_name, '.');
			//check the file extension
			$valid_range = array(".gif",".png",".zip",".sit",".pdf",".jpg",".doc",".docx",".tif",".tiff",".epc",".sea",".bmp",".GIF",".PNG",".ZIP",".SIT",".PDF",".JPG",".DOC",".DOCX",".TIF",".TIFF",".EPC",".SEA",".BMP",".txt");
			if (in_array($file_ext,$valid_range)) {
				$uploadPath = PATH_ROOT."login/tracking/";
				chmod($uploadPath, intval(0777));
				$fromDesigner = $uploadPath.$newFilename;
				if (move_uploaded_file($fileatt, $fromDesigner)) {
					chmod($uploadPath, intval(0755));
					return $newFilename;
				} else {
					Bugs::setError('debug',"The file did not upload");
					return FALSE;				
				}
			} else {
				Bugs::setError('debug',"The file uploaded for the issue is not a valid extension. It is $file_ext.");
				return FALSE;
			}
		} else {
			Bugs::setError('debug',"There is no uploaded file.");
			return FALSE;
		}         
	}
}
?>
