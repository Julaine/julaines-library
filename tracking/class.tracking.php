<?php
/**
 * My tracking tool or
 * project development tracking tool
 * May need a better name
 *
 * @package julaine
 */
/**
 * @package julaines-library
 * @version 2.0.6a 
 * @uses 		Spoon
 *
 * Setup standard db tables
 * Changed to opened and closed and added assigned
 * added severity and removed ranking
 * removing spoon db
 */
class Tracking {
	protected $adminPerson; //can I use this and then set it in construct?
	protected $from; //can I use this and then set it in construct
	protected $db;

	/**
	 * Setup a db connection using spoon
	 * Generate today's date
	 */
	function __construct() {
		// create database object
		$this->db = new SpoonDatabase(DB_TYPE, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		$this->adminPerson = "Julaine Scott <work@julaine.ca>";
		$this->from = "Julaine Scott <webmaster@julaine.ca>";
		}
	/**
	 * Removed from inside form
	 * Need it independent so I can call it 
	 * when debugging
	 * 
	 * @param array $postedValues 
     * @return int
	 */
	function addIssue($postedValues) {
		if (!is_array($postedValues)) return FALSE;
		extract($postedValues); //already sanitized
		if ($ititle == "") {
			$_SESSION['errors'] = 'Please provide a title for the issue.';
			return FALSE;
		}
		return Database::db("INSERT INTO issues
					(projectId,taskId,client_iid,opened,ititle,idescription,iurl,isection,openDate,
					type,priority,estimate,lastUser,lastAccessed)
					VALUES ('$projectId','$tid','$client_iid','1','$ititle','$idescription','$iurl','$isection',NOW(),
					'$type','$priority','$estimate','1',NOW())");

	}
	/**
	 * Add Issue Form
	 *
	 * @uses SpoonForm
	 */
	function addIssueForm() {
		// create form
		$frm = new SpoonForm('issue_add'); 
		$frm->setParameter('class','form-horizontal');
		$frm->setAction('report.php');
		$frm->addText('ititle', NULL, 100, 'form-control')
			->setAttribute('required')
			->setAttribute('placeholder', 'Short description'); 
		$frm->addTextarea('idescription', NULL, 'textarea form-control')
			->setAttribute('placeholder', 'Long description'); 
		$frm->addText('iurl', NULL, 200, 'form-control')
			->setAttribute('type', 'text') 
			->setAttribute('placeholder', 'URL or page name'); 
		//isection is dropdown
		//$frm->addDropdown('isection', $this->sections, NULL, FALSE, 'form-control');
		//type
		$type_choices = array('Bug','Task','Request','Improvement');
		$frm->addDropdown('type', $type_choices, 'Bug', FALSE, 'form-control');
		//priority
		$priority_choices = array();
		$priority_choices['Urgent'] = 'Urgent';
		$priority_choices['Important'] = 'Important';
		$priority_choices['Normal'] = 'Normal';
		$frm->addDropdown('priority', $priority_choices, 'Normal', FALSE, 'form-control');

		// add hidden value(s)
		$frm->addHidden('process', 1);
		// add Save button
		$frm->addButton('submit', 'Add')
			->setAttribute('class', 'btn btn-primary');

		if($frm->isSubmitted()) {
			$frm->getField('ititle')->isFilled('Please provide a name.');		
			if($frm->isCorrect()){
				// all the information that was submitted
				$postedValues = $frm->getValues();
				extract($postedValues);

				$issue_id = $this->addIssue($postedValues);
				if ($issue_id > 0) {
					$admin = new Admin;
					//$emails = new Emails;
					$notices = new Notices;
					//notify Julaine of the new issue
					$firstname = $admin->getName();
					if ($notices->notifyAdminNewIssue($firstname,$ititle,$priority,$issue_id) == TRUE) {
						$_SESSION['success'] = 'Thank you, the new issue was added.';
					} else {
					
						Bugs::setError('track', 'Failed to send email to Julaine when creating new issue ('.$issue_id.')');
					}
				} else {
					Bugs::setError('notify', 'Did not create a lead');
				}	

				foreach($_POST as $key => $value) {
					unset($_POST[$key]);
				}
				header('Location: index.php');
				die;

			} else {
				Bugs::setError('errors', 'Did not create an issue');
			}
		} 
		$tpl = new SpoonTemplate();
		$tpl->setForceCompile(true);
		$tpl->setCompileDirectory(PATH_ROOT.'cache');
		$frm->parse($tpl);
		$tpl->display(PATH_TEMPLATES.'tracking/form_issue_add.tpl');	
	}
	/**
	 * Handler to generate DB script for finding issues
	 * Unique to animated-jellybeans
	 *
	 * @param string $pid
	 * @param string $status
	 * @param string $date
	 * @param string $priority
	 * @param string $estimate
	 * @param string $tid
	 * @return string
	 */
	function findIssues($pid="All",$status="All",$date="All",$priority="All",$estimate="All",$tid="All") {
		/**
		 * changed to allow priority to handle sorting outside ipriority field
		 *
		 */
		if($status !== "All") {
			$sqlInsert= "i.status ='$status'";
		} else {
			$sqlInsert = "i.status != 'Fixed'
					AND i.status != 'Wish List' ";
		}


		if ($pid !== "All" && (int)$pid > 0) {
			$sqlInsert2 = " AND i.projectId='$pid' ";
		} else {
			$sqlInsert2 = '';
		}
		if ($date !== "All") {
			$sqlInsert3 = " AND i.lastAccessed LIKE ('$date%')";
		} else {
			$sqlInsert3 = '';
		}
		if ($priority !== "All") {
			if ($priority == "Urgent") {
				/**
				 * Yes you want to reset $sqlInsert
				 */
				$sqlInsert = "i.status != 'Fixed'
					AND i.status != 'Wish List' AND i.status !='On Hold'";
				$sqlInsert4 = '';
			} elseif ($priority == "Important") {
				$sqlInsert4 = " AND (i.priority='Important' OR i.priority='Urgent') ";
			} elseif ($priority == "Normal") {
				$sqlInsert4 = "";
			} else {
				$sqlInsert4 = " AND i.priority='$priority'";
			}
		} else {
			$sqlInsert4 = '';
		}
		if ($estimate !== "All") {
			$sqlInsert5 = " AND i.estimate='$estimate'";
		} else {
			$sqlInsert5 = '';
		}
		if ($tid !== "All") {
			$sqlInsert6 = " AND i.taskId='$tid'";
		} else {
			$sqlInsert6 = '';
		}
		if ($priority == "Normal") {
			$sortOrder = " ORDER BY i.issueId ASC";
		} elseif ($priority == "Urgent") {
			$sortOrder = " ORDER BY i.issueId ASC";
		} else {
			$sortOrder = ' ORDER BY i.lastAccessed DESC';
		}
		//status matches db field
		//must add an accounts table to make this word
		//need a section in the admin to make it work
		//find adminAccess class - from demo

		$find = Database::db("SELECT i.issueId,i.client_iid,t.tname,i.priority,i.ititle,i.idescription,
		            i.openDate,i.status,
					i.type,i.estimate,i.lastAccessed,p.tagName
					FROM issues AS i
					LEFT JOIN projects AS p ON i.projectId=p.pid
					LEFT JOIN tags AS t ON t.tid=i.taskId
					LEFT JOIN accounts AS a ON a.accountId=i.opened
					WHERE  $sqlInsert $sqlInsert2 $sqlInsert3 $sqlInsert4 $sqlInsert5 $sqlInsert6
					$sortOrder");

		if ($find) {
            $numIssues = count($find);
			foreach ($find as $row) {
				extract($row);
				if ($lastAccessed !== '0000-00-00 00:00:00') {
					//if today, show time
					//if not today show date
					if (substr($lastAccessed, 0,10) == date("Y-m-d",time())) {
						$last = '<b>'.date("g:i a",strtotime($lastAccessed)).'</b>';

					} else {
						$last = '<b>'.date("M d y",strtotime($lastAccessed)).'</b>';
					}
				} else {
					$last = '';
				}
				$open = '<b>'.date("M d y",strtotime($openDate)).'</b>';
				$issue_desc = NULL;

				$ticon = self::getFontAwesomeIcon($tname);

				$status_icon2 = self::getFontAwesomeTypeIcon($type,$priority);
				$status_icon = self::getFontAwesomePriorityIcon($status);
				$comment = strip_tags(html_entity_decode(substr($idescription,0,120)));

				//find last comment
				$query = "SELECT comment FROM comments WHERE commentTypeId='$issueId' ORDER BY commentUpdate DESC LIMIT 0,1";
				$find = Database::db($query);
				if ($find) {
					$comment = strip_tags(html_entity_decode(substr($find[0],0,120)));
				}

				$issue_desc .= '<strong>'.stripslashes($ititle).'</strong> - <small>'.$comment.'...</small>';
				$path = PATH_ADMIN.'tracking/respond.php';
				$returnDisplay[] = <<<HEREDOC
<tr>
<td>$tagName</td>
<td>$status</td>
<td>$estimate</td>
<td>$client_iid</td>
<td>$issueId</td>
<td>$ticon</td>
<td>$status_icon $status_icon2</td>
<td><a href="$path?id=$issueId">$issue_desc</a></td>
<td>$open</td>
<td>$last</td>
</tr>
HEREDOC;
			}

			return array($numIssues,join("\n",$returnDisplay));
		}
	}
	/**
	 * Generate a list of issues
	 *
	 * @param string $type2show
	 * @return string
	 */
	function findIssuesOld($type2show="Outstanding") {
		if ($type2show !== "Outstanding") {
			$query = 'SELECT i.issueId,i.ititle,i.idescription,i.status,i.type,i.priority,a.account_firstname,
						i.lastAccessed 
						FROM issues AS i
						LEFT JOIN accounts AS a ON a.accountId=i.lastUser
						WHERE i.status=?
						ORDER BY i.lastAccessed DESC';
			$find = $this->db->getRecords($query,$type2show);
		} else {
			$query = 'SELECT i.issueId,i.ititle,i.idescription,i.status,i.type,i.priority,a.account_firstname,
					i.lastAccessed 
					FROM issues AS i
					LEFT JOIN accounts AS a ON a.accountId=i.lastUser
					WHERE i.status !=? 
					AND i.status !=? 
					AND i.status !=?
					ORDER BY i.lastAccessed DESC';
			$find = $this->db->getRecords($query,array('Fixed','On Hold','Wish List'));

		}
		if ($find) {
			$tables = new EditTables;
			if ($type2show !== "Outstanding") {
				echo "<h5>$type2show Issues</h5>";
			}
			echo $tables->startTable($class="table-condensed table-striped",$type2show);		
			echo $tables->makeCellHeading(array('#','Issue', 'Date'));	

			foreach ($find as $row) {				
				echo "<tr>";
				extract($row);
				if ($account_firstname == "") {
					$account_firstname = $this->getLastCommentFrom($issueId);
				}
				$last = NULL;
				if ($lastAccessed !== '0000-00-00 00:00:00') {
					//if today, show time
					//if not today show date
					if (substr($lastAccessed, 0,10) == date("Y-m-d",time())) {
						$last = '<b>'.date("g:iA",strtotime($lastAccessed)).'</b>';

					} else {
						$last = '<b>'.date("M-d-y",strtotime($lastAccessed)).'</b>';
					}
				}
				echo $tables->wrapInCell(str_pad($issueId,2,0,STR_PAD_LEFT));
				//echo $tables->wrapInCell($account_firstname);
				$issue_desc = NULL;
				if ($priority == "Important") {
					//echo $tables->wrapInCell('<i class="fa fa-star"></i> ');
					$issue_desc .= '<i class="fa fa-star"></i> ';
				} elseif ($priority == "Urgent") {
					//echo $tables->wrapInCell('<i class="fa fa-flag"></i> ');
					$issue_desc .= '<i class="fa fa-flag"></i> ';
				} 
				if ($type == "Improvement") {
					$issue_desc .= '<i class="fa fa-plus-sign-alt"></i> ';
				} elseif ($type == "Bug") {
					$issue_desc .= '<i class="fa fa-bug"></i> ';
				}
				$issue_desc .= '<strong>'.stripslashes($ititle).'</strong> - <small>'.strip_tags(SpoonFilter::htmlentitiesDecode(substr($idescription,0,120), null, $quoteStyle = ENT_NOQUOTES)).'...</small>';				
				echo $tables->wrapAInCell('respond.php?id='.$issueId,$issue_desc);
				echo $tables->wrapInCell($last);
				echo "</tr>";
			}		
			echo $tables->endTable();
		} else {
			return "There are no issues";

		}
	}

	/**
	 * Brought in from jadmin specific
	 * Used on organizeTasks page
	 *
	 * @todo Consider creating an organizeTasks class??
	 *       Or do I want to delete as I no long use issueOrder field
	 * @param null $pid
	 * @return bool|string
	 */
	function findIssuesList($pid=NULL) {
		//does not work because the id for the list is not unique across projects
		$find = Database::db("SELECT i.issueId,i.client_iid,t.tname,i.status,i.ititle,
					DATE_FORMAT(i.lastAccessed,'%m %d %y %h:%i %p')
					FROM issues AS i
					LEFT JOIN projects AS p ON i.projectId=p.pid
					LEFT JOIN tags AS t ON t.tid=i.taskId
					LEFT JOIN accounts AS a ON a.accountId=i.opened
					WHERE  i.status != 'Fixed' AND i.projectId='$pid'
					ORDER BY i.lastAccessed DESC");
		if (!$find) {
			return FALSE;
		} else {
			$returnDisplay = array();
			$returnDisplay[] = '<ul id="sortList">';
			foreach ($find as $row) {
				extract($row);
				$returnDisplay[] = '<li id="arrayorder_'.$issueId.'">
					<a href="respond.php?id='.$issueId.'">'
					.$ititle.'</a><br>'.$tname.'</li>';
			}
			$returnDisplay[] = '</ul>';
			return join("\n",$returnDisplay);
		}
	}

	/**
	 * @uses newCommentEmail
	 * 
	 * @param  $issueId
	 * @return string
	 */
	function findIssueOpener($issueId) {
		$findOpener = Database::db("SELECT a.firstname AS from_name,a.email AS from_email
						FROM accounts AS a
						LEFT JOIN issues AS i ON a.accountId=i.opened
						WHERE i.issueId='$issueId'");
		if ($findOpener) {
			foreach ($findOpener as $row) {
				extract($row);
				return $from_name.' <'.$from_email.'>';	
			}
		}
		return FALSE;
		
	}
	/**
	 * @uses newCommentEmail
	 * @param  [type] $lastUserId [description]
	 * @return string
	 */
	function findLastUser($lastUserId) {
		$query = Database::db("SELECT firstname AS from_name,email AS from_email
					FROM accounts 
					WHERE accountId='$lastUserId'");
		if ($query) {
			foreach ($query as $row) {
				extract($row);
				return $from_name.' <'.$from_email.'>';	
			}
		}				
	}

	/**
	 * Find the icon from Font Awesome
	 *
	 * @see http://fortawesome.github.io/Font-Awesome/icons/
	 * @param $tname
	 * @return string
	 */
	function getFontAwesomeIcon($tname) {
		$ticon = '';//optional to set a default icon here
		if ($tname == "Administration") $ticon = '<i class="fa fa-print"></i>';
		if ($tname == "Bugs") $ticon = '<i class="fa fa-bug"></i>';
		if ($tname == "Content") $ticon = '<i class="fa fa-pencil"></i>';
		if ($tname == "Database") $ticon = '<i class="fa fa-table"></i>';
		if ($tname == "Graphics") $ticon = '<i class="fa fa-picture-o"></i>';
		if ($tname == "Layout") $ticon = '<i class="fa fa-columns"></i>';
		if ($tname == "Media Items") $ticon = '<i class="fa fa-puzzle-piece"></i>';
		if ($tname == "Meeting") $ticon = '<i class="fa fa-group"></i>';
		if ($tname == "Phone Call") $ticon = '<i class="fa fa-phone"></i>';
		if ($tname == "Research") $ticon = '<i class="fa fa-book"></i>';
		if ($tname == "Server") $ticon = '<i class="fa fa-hdd-o"></i>';
		if ($tname == "Technical Support") $ticon = '<i class="fa fa-comments"></i>';
		if ($tname == "Testing") $ticon = '<i class="fa fa-question"></i>';
		if ($tname == "Training") $ticon = '<i class="fa fa-user"></i>';
		if ($tname == "PHP" || $tname == "HTML" || $tname == "Flash" || $tname == "CSS") $ticon = '<i class="fa fa-code"></i>';
		if ($tname == "Writing") $ticon = '<i class="fa fa-pencil"></i>';
		return $ticon;
	}

	/**
	 * Get the Font Awesome icon
	 *
	 * Useful for some, but not for all
	 * @param $type
	 * @return string
	 */
	function getFontAwesomeTypeIcon($type,$priority){
		$status_icon2 = '';
		if ($type == "Improvement") {
			$status_icon2 = '<i class="fa fa-plus-sign-alt"></i> ';
		} elseif ($type == "Bug") {
			$status_icon2 = '<i class="fa fa-bug"></i> ';
		}
		if ($priority == "Important" || $priority == "Urgent") {
			$status_icon2 .= ' <i class="fa fa-star"></i> ';
		}

		return $status_icon2;
	}

	/**
	 * Find out which icon for priority
	 * @param $status
	 * @return string
	 */
	function getFontAwesomePriorityIcon($status) {
		$status_icon = '';
		if ($status == "On Hold") {
			$status_icon = '<i class="fa fa-clock-o"></i> ';
		} elseif ($status == "Reported" || $status == "In_Progress") {
			$status_icon = '';
		} elseif ($status == "Questions" || $status == "Clarification_Needed") {
			$status_icon = '<i class="fa fa-question"></i> ';
		}
		return $status_icon;
	}

	/**
	 * Find the icon from WebHostingHub Glyphs
	 * @see http://www.webhostinghub.com/glyphs/
	 * @param $tname
	 * @return string
	 */
	function getWGlyphIcon($tname) {
		$ticon = '';//optional to set a default icon here
		if ($tname == "Administration") $ticon = '<i class="icon-print"></i>';
		if ($tname == "Bugs") $ticon = '<i class="icon-bug"></i>';
		if ($tname == "Content") $ticon = '<i class="icon-pencil"></i>';
		if ($tname == "Database") $ticon = '<i class="icon-database"></i>';
		if ($tname == "Graphics") $ticon = '<i class="icon-picture"></i>';
		if ($tname == "Layout") $ticon = '<i class="icon-twocolumnsright"></i>';
		if ($tname == "Media Items") $ticon = '<i class="icon-puzzle-plugin"></i>';
		if ($tname == "Meeting") $ticon = '<i class="icon-groups-friends"></i>';
		if ($tname == "Phone Call") $ticon = '<i class="icon-phone-alt"></i>';
		if ($tname == "Research") $ticon = '<i class="icon-bookthree"></i>';
		if ($tname == "Server") $ticon = '<i class="icon-server"></i>';
		if ($tname == "Technical Support") $ticon = '<i class="icon-comment"></i>';
		if ($tname == "Testing") $ticon = '<i class="icon-question-sign"></i>';
		if ($tname == "Training") $ticon = '<i class="icon-user"></i>';
		if ($tname == "PHP" || $tname == "HTML" || $tname == "Flash" || $tname == "CSS") $ticon = '<i class="icon-code"></i>';
		if ($tname == "Writing") $ticon = '<i class="icon-pencil"></i>';

		return $ticon;
	}

	/**
	 * Find the icon from WebHostingHub Glyphs
	 * @see http://www.webhostinghub.com/glyphs/
	 *
	 * @param $type
	 * @return string
	 */
	function getWGlyphTypeIcon($type,$priority){
		$status_icon2 = '';
		if ($type == "Improvement") {
			$status_icon2 = '<i class="icon-plus-sign"></i> ';
		} elseif ($type == "Bug") {
			$status_icon2 = '<i class="icon-bug"></i> ';
		}
		if ($priority == "Important" || $priority == "Urgent") {
			$status_icon2 .= ' <i class="icon-fullstar"></i> ';
		}

		return $status_icon2;
	}

	/**
	 * Find out which icon for priority
	 * @param $status
	 * @return string
	 */
	function getWGlyphPriorityIcon($status) {
		$status_icon = '';
		if ($status == "On Hold") {
			$status_icon = '<i class="icon-clockalt-timealt"></i> ';
		} elseif ($status == "Reported" || $status == "In_Progress") {
			$status_icon = '';
		} elseif ($status == "Questions" || $status == "Clarification_Needed") {
			$status_icon = '<i class="icon-question-siqn"></i> ';
		}
		return $status_icon;
	}
	/**
	* I have two versions
	* one uses opened and the  other comment_by
	 * @param $issueId
	 * @return string
	*/
	function getLastCommentFrom($issueId) {
		$query_last = 'SELECT a.account_firstname FROM comments AS c
									LEFT JOIN accounts AS a
									ON a.accountId=c.comment_by
									WHERE c.commentTypeId=? AND c.commentType=? 
									ORDER BY c.commentId DESC LIMIT 0,1';
		return $account_firstname = $this->db->getVar($query_last,array($issueId,'issue'));
	}

	/**
	 *
	 * @param      $issueId
	 * @param int  $lastUserId
	 * @param null $debug
	 * @return bool
	 */
	function newCommentEmail($issueId,$lastUserId=1,$debug=NULL) {
		$findInfo = Database::db("SELECT ititle,status,priority FROM issues WHERE issueId='$issueId'",$debug);
		if (!$findInfo) {
			return FALSE;
		}
		foreach ($findInfo as $row) { 
			extract($row);	
		}	
		$firstPerson = $this->findIssueOpener($issueId);
		$lastPerson = $this->findLastUser($lastUserId);
		
		if ($lastUserId == 1) { //ie not Julaine
			if ($firstPerson !== $lastPerson) { $sendTo = $firstPerson; }
		} else {
			//someone at epc makes a comment			
			if ($status == "Fixed") {						
				if ($firstPerson !== $lastPerson) {
					//if someone other than the opener closes the issue,
					$sendTo = $firstPerson;
				} //else do nothing
			} else {
				$sendTo = self::adminPerson;
				
				if ($firstPerson == $lastPerson) {
					//do nothing 
				} else {
					$sendTo = $firstPerson;
					//someone other than the opener is adding a comment
				}
			}
		}
		/**
		 * @todo use changeIssue()
		 */
		if ($status == "Fixed") {						
			$saveStatus = Database::db("UPDATE issues SET closed='$lastUserId',closeDate=NOW(),
							status='$status',lastUser='$lastUserId',lastAccessed=NOW() 
							WHERE issueId='$issueId'");
			$Message = "<p>Issue ".stripslashes($ititle)." was closed.";
			//override existing settings for this instance
		} else {
			$saveStatus = Database::db("UPDATE issues SET lastUser='$accountId',status='$status',lastAccessed=NOW()
							WHERE issueId='$issueId'");
			$Message = "<p>Updated Issue ".stripslashes($ititle).".";
		}
		$Message .= '<p><a href="'.SITE_ADMIN.'tracking/respond.php?id='.$issueId.'">View Issue</a>.';
		$subject = "Updated issue on the e-patches and crests website.";		 
		return $this->emailNotification($issueId,$Message,$subject,$sendTo); 
	}

	/**
	 * @todo check usage
	 * @param      $issueId
	 * @param int  $lastUserId
	 * @param null $debug
	 * @return bool
	 */
	function newIssueEmail($issueId,$lastUserId=1,$debug=NULL) {
		$findInfo = db("SELECT ititle,status,priority FROM issues WHERE issueId='$issueId'",$debug);
		if (!$findInfo) {
			return FALSE;
		}
		foreach ($findInfo as $row) {
			extract($row);
		}
		$Message = "<p>".$firstname." reported ".stripslashes($ititle)." (priority <strong>$priority</strong>).";
		$Message .= '<p><a href="'.SITE_ADMIN.'tracking/respond.php?id='.$issueId.'">View Issue</a>.';
		$subject = "New Issue on the e-patches and crests website";
		error_log("epatches new issue send to $sendTo with subject $subject");
		$success = $this->emailNotification($issueId,$Message,$subject,$this->$adminPerson);
		if ($success == TRUE) {
			$_SESSION['success'] = 'Thank you, the issue was reported.<br><a href="respond.php?id='.$issueId.'">
								Continue to update this issue</a> or <a href="report.php">Add Another Issue</a>';
			return TRUE;
		} else {
			error_log("failed to send email when new issue addded");
			$_SESSION['errors'] = 'I am sorry, am error occurred sending the email notification.';
			return FALSE;
		}

	}
	/**
	 * @todo Switch to $emails->sendEmail()
	 * 		Keep the function name and change the guts
	 *		use it for both new issues and comments on issues
	 * 
	 * @param  [type] $issueId [description]
	 * @param  [type] $Message [description]
	 * @param  [type] $subject [description]
	 * @param  [type] $sendTo  [description]
	 * @return boolean
	 */
	
	function emailNotification($issueId,$Message,$subject,$sendTo) {
		include('Mail.php');
		include('Mail/mime.php');
		$headers["From"] = self::from;
		$headers["To"]    = $sendTo; 
		$headers["Bcc"]    = "Julaine Scott <work@julaine.ca>"; 
		$headers["Subject"] = $subject; 			
		$crlf = "\n"; 
		$mime = new Mail_mime($crlf); 
		$mime->setHTMLBody($Message);
		$body = $mime->get();
		$hdrs = $mime->headers($headers);
		$mime =& Mail::factory('mail');			
		if($mime->send($sendTo, $hdrs, $body)) {
			return TRUE;	
		} else {

			Bugs::setError('debug',"failed to send email when new issue addded");
			$_SESSION['errors'] = 'I am sorry, am error occurred sending the email notification.';
			return FALSE;
		}	
	}	
	/**
	* @todo Can I change db. Use lastUser for closed
	* 		then I could just use one array for saving to db
	*		and not need to differentiate by status?
	*/
	function changeIssue($id,$postedValues) {
        extract($postedValues);
		$userId = 1; //getUserId();
		if ($status == "Fixed") {
			Database::db("UPDATE issues SET closed='$userId',closeDate=NOW(),status='$status',
			lastUser='1',lastAccessed=NOW()
							WHERE issueId='$id'");
			$Message = "<p>Issue ".stripslashes($ititle)." was closed by $firstname.";
		} else {
			Database::db("UPDATE issues SET lastUser='1',status='$status',lastAccessed=NOW() WHERE issueId='$id'");
			$Message = "<p>Issue ".stripslashes($ititle)." was marked $status by $firstname.";
		}

		if ($find) {
			return $Message;
		} else {
			Bugs::setError("debug","ERROR updating issue $id");
			return FALSE;
		}
	}
	/**
	 * added here from the functions file
	 * @todo Simplify and use the UI::uploadImage
	 *       by sending the unique path (ie $uploadPath)
	 */
	function uploadIssueImage($uploadedFile,$issueId) {
		 //$uploadedFile is in the form of $_FILES['nameHere']
		$fileatt      = $uploadedFile['tmp_name'];
		$fileatt_name = $uploadedFile['name'];
		$file_ext 	  = strrchr($fileatt_name, '.');
		$newFilename1 = str_replace("'", "", $fileatt_name);
		$newFilename  = $issueId."_".str_replace(" ", "_", $newFilename1);
		
		if (is_uploaded_file($fileatt)) {
			// Read the file to be attached ('rb' = read binary)
			//check the file extension
			$valid_range = array(".gif",".png",".zip",".sit",".pdf",".jpg",".doc",".docx",".tif",".tiff",".epc",".sea",".bmp",".GIF",".PNG",".ZIP",".SIT",".PDF",".JPG",".DOC",".DOCX",".TIF",".TIFF",".EPC",".SEA",".BMP",".txt");
			if (in_array($file_ext,$valid_range)) {
				$uploadPath = PATH_ADMIN."tracking/";
				chmod($uploadPath, intval(0777));
				$fromDesigner = $uploadPath.$newFilename;
				if (move_uploaded_file($fileatt, $fromDesigner)) {
					chmod($uploadPath, intval(0755));
					return $newFilename;
				} else {
					Bugs::setError('debug',"The file did not upload");
					return FALSE;				
				}
			} else {
				Bugs::setError('debug',"The file uploaded for the issue is not a valid extension. It is $file_ext.");
				return FALSE;
			}
		} else {
			Bugs::setError('debug',"There is no uploaded file.");
			return FALSE;
		}         
	}
}
?>
