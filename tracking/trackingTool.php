<?php

/**
 * Class trackingTool
 *
 * @package julaines-library
 * @version 2.0.2
 */
class trackingTool {
	protected $adminPerson; //can I use this and then set it in construct?
	protected $from; //can I use this and then set it in construct

	/**
	 * @todo Do I know SITE_ROOT?
	 * @todo Should I change to a handler outside instead?
	 */
	function __construct() {
		$this->adminPerson = "Julaine Scott <work@julaine.ca>";
		$this->from = "e-patches & crests Web site <webmaster@e-patchesandcrests.com>";
	}
		
	function saveIssue($postedValues,$debug=NULL) {
		extract($postedValues);
		if (!isset($userId)) { $userId = getUserId("account"); }
		if ($status == "Fixed") {		
			$saveStatus = db("UPDATE issues SET closeUserId='$userId',closeDate=NOW(),status='$status',lastUser='$userId',lastAccessed=NOW() WHERE issueId='$issueId'");
		} else {
			$saveStatus = db("UPDATE issues SET lastUser='$userId',status='$status',lastAccessed=NOW() WHERE issueId='$issueId'");
		}	
		if ($saveStatus) {
			if (!empty($wysiwyg)) {
				//left assignedTo blank
				$commentId = db("INSERT INTO comments VALUES ('','$issueId','','$userId','','issue','$wysiwyg',NOW())",0);
				if ($commentId > 0) {
					 $_SESSION['success'] = "Comment saved successfully";
					return $commentId;
				} else {
					$_SESSION['errors'] ='<p>The comment was <strong>not</strong>saved.';
					return FALSE;
				}
			}
		} else {
			saveAction("ERROR did not add issue","Add Issue");//use on demo site to ensure save	
		}
	}
	function findIssues($status="Outstanding",$debug=0) {		
		if($status == "Outstanding") {
			$sqlInsert = "i.status != 'Fixed' 
					AND i.status != 'Wish List' 
					AND i.status != 'On Hold'";
		} else {
			$sqlInsert = "i.status='$status'";	
		}
		$find = db("SELECT i.issueId,i.status,i.ititle,i.priority,a.firstname,
					DATE_FORMAT(i.lastAccessed,'%m %d, %y (%h:%i %p)') AS lastUpdate  
					FROM issues AS i
					LEFT JOIN accounts AS a ON a.accountId=i.lastUser
					WHERE  $sqlInsert
					ORDER BY i.lastAccessed DESC",$debug);
		if ($find) {				
			foreach ($find as $row) {				
				$returnDisplay[] = "<tr>";
				extract($row);
				if ($firstname == "") {
					$findLast = db("SELECT a.firstname FROM comments AS c
									LEFT JOIN accounts AS a
									ON a.accountId=c.userId 
									WHERE c.commentTypeId='$issueId' AND c.commentType='issue' ORDER BY c.commentId DESC LIMIT 0,1",$debug);
					if ($findLast) {
						$firstname = $findLast[0];
						/*$findName = db("SELECT firstname FROM accounts WHERE accountId='$userId'",$debug);
						if ($findName) {
							$firstname = $findName[0];
						}*/
					}
				}
				$returnDisplay[] = '<td>'.$issueId.'</td>';
				$returnDisplay[] = '<td>'.str_replace("_"," ",$status).'</td>';
				$returnDisplay[] = '<td><a href="respond.php?id='.$issueId.'">'.stripslashes($ititle).'</a></td>';
				if ($priority == "Important") {
					$returnDisplay[] = '<td><i class="icon icon-flag"></i> '.$priority.'</td>';
				} elseif ($priority == "Urgent") {
					$returnDisplay[] = '<td><i class="icon icon-star"></i> '.$priority.'</td>';
				} else {
					$returnDisplay[] = '<td>'.$priority.'</td>';
				}
				$returnDisplay[] = '<td>'.$firstname.'</td>';
				$returnDisplay[] = '<td>'.$lastUpdate.'</td>';
				$returnDisplay[] = "</tr>";
			}			
			return join("\n",$returnDisplay);
		}
	}
	function addIssue($postedValues,$debug=0) {
		extract($postedValues); //already sanitized
		if ($ititle == '') {
			$_SESSION['errors'] = "Please enter a title for the issue.";
			return FALSE;
		}
		$issueId = db("INSERT INTO issues 
					(projectId,orderId,openUserId,ititle,idescription,iurl,isection,openDate,type,priority,ranking,lastUser,lastAccessed)
					VALUES
					('$projectId','$orderId','$userId','$ititle','$idescription','$iurl','$isection',NOW(),
					'$type','$priority','$ranking','$userId',NOW())",$debug);
		if ($issueId > 0) {
			return $issueId;
		} else {
			saveAction("ERROR did not add issue",getRealIp(),"Add Issue");//use on demo site to ensure save	
		}
	}
	function findIssueOpener($issueId,$debug=NULL) {
		$findOpener = db("SELECT a.firstname AS from_name,a.email AS from_email
						FROM accounts AS a
						LEFT JOIN issues AS i ON a.accountId=i.openUserId
						WHERE i.issueId='$issueId'",$debug);
		if ($findOpener) {
			foreach ($findOpener as $row) {
				extract($row);
				return $from_name.' <'.$from_email.'>';	
			}
		} else {
			return FALSE;
		}
		
	}
	function findLastUser($lastUserId,$debug=NULL) {
		$findLastUser = db("SELECT firstname AS from_name,email AS from_email
							FROM accounts 
							WHERE accountId='$lastUserId'");
		if ($findLastUser) {
			foreach ($findLastUser as $row) {
				extract($row);
				return $from_name.' <'.$from_email.'>';	
			}
		} else {
			return FALSE;
		}
	}
	function newIssueEmail($issueId,$lastUserId=1,$debug=NULL) {
		$findInfo = db("SELECT ititle,status,priority FROM issues WHERE issueId='$issueId'",$debug);
		if (!$findInfo) {
			return FALSE;
		}
		foreach ($findInfo as $row) { 
			extract($row);	
		}	
		$Message = "<p>".$firstname." reported ".stripslashes($ititle)." (priority <strong>$priority</strong>).";
		$Message .= '<p><a href="'.SECUREURL.'/epcAdmin/tracking/respond.php?id='.$issueId.'">View Issue</a>.';
		$subject = "New Issue on the e-patches and crests website";
		error_log("epatches new issue send to $sendTo with subject $subject");
		$success = $this->emailNotification($issueId,$Message,$subject,$this->$adminPerson);
		if ($success == TRUE) {
			$_SESSION['success'] = 'Thank you, the issue was reported.<br><a href="respond.php?id='.$issueId.'">
								Continue to update this issue</a> or <a href="report.php">Add Another Issue</a>';
			return TRUE;	
		} else {
			error_log("failed to send email when new issue addded");
			$_SESSION['errors'] = 'I am sorry, am error occurred sending the email notification.';
			 return FALSE;
		}
  
	}
	function newCommentEmail($issueId,$lastUserId=1,$debug=NULL) {
		$findInfo = db("SELECT ititle,status,priority FROM issues WHERE issueId='$issueId'",$debug);
		if (!$findInfo) {
			return FALSE;
		}
		foreach ($findInfo as $row) { 
			extract($row);	
		}	
		$from = "e-patches & crests Web site <webmaster@e-patchesandcrests.com>";
		$lastUserId = '';
		$firstPerson = $this->findIssueOpener($issueId);
		$lastPerson = $this->findLastUser($lastUserId);
		
		$sentTo = 'Julaine Scott <work@julaine.ca>'; //default is Julaine	
		if ($lastUserId == 1) { //ie not Julaine
			if ($firstPerson !== $lastPerson) { $sendTo = $firstPerson; }
		} else {
			//someone at epc makes a comment					
			if ($status == "Fixed") {						
				if ($firstPerson !== $lastPerson) {
					//if someone other than the opener closes the issue,
					$sendTo = $firstPerson;
				} //else do nothing
			} else {			
				if ($firstPerson !== $lastPerson) {
					$sendTo = $firstPerson;
					//someone other than the opener is adding a comment
				}
			}
		}
		
		if ($status == "Fixed") {						
			$saveStatus = db("UPDATE issues SET closeUserId='$lastUserId',closeDate=NOW(),
							status='$status',lastUser='$lastUserId',lastAccessed=NOW() 
							WHERE issueId='$issueId'");
			$Message = "<p>Issue ".stripslashes($ititle)." was closed.";
			//override existing settings for this instance
		} else {
			$saveStatus = db("UPDATE issues SET lastUser='$lastUserId',status='$status',lastAccessed=NOW() 
							WHERE issueId='$issueId'");
			$Message = "<p>Updated Issue ".stripslashes($ititle).".";
		}
		$Message .= '<p><a href="'.SECUREURL.'/epcAdmin/tracking/respond.php?id='.$issueId.'">View Issue</a>.';
		$subject = "Updated issue on the e-patches and crests website.";		 
		error_log("epatches new comment send to $sendTo with subject $subject");
		return $this->emailNotification($issueId,$Message,$subject,$sendTo);
	}
	function emailNotification($issueId,$Message,$subject,$sendTo) {
		include('Mail.php');
		include('Mail/mime.php');
		$headers["From"] = $this->from;
		$headers["To"]    = $sendTo; 
		$headers["Bcc"]    = "Julaine Scott <work@julaine.ca>"; 
		$headers["Subject"] = $subject; 			
		$crlf = "\n"; 
		$mime = new Mail_mime($crlf); 
		$mime->setHTMLBody($Message);
		$body = $mime->get();
		$hdrs = $mime->headers($headers);
		$mime =& Mail::factory('mail');			
		if($mime->send($sendTo, $hdrs, $body)) {
			return TRUE;	
		} else {
			error_log("failed to send email when new issue addded");
			$_SESSION['errors'] = 'I am sorry, am error occurred sending the email notification.';
			 return FALSE;
		}	
	}	
	function changeIssue($id,$postedValues,$debug) {
		$userId = getUserId();
		if ($status == "Fixed") {		
			$saveStatus = go("UPDATE issues SET closeUserId='$userId',closeDate=NOW(),status='$status',lastUser='$userId',lastAccessed=NOW() 
							WHERE issueId='$id'");
			$Message = "<p>Issue ".stripslashes($ititle)." was closed by $firstname.";
		} else {
			$saveStatus = go("UPDATE issues SET lastUser='$userId',status='$status',lastAccessed=NOW() WHERE issueId='$id'");
			$Message = "<p>Issue ".stripslashes($ititle)." was marked $status by $firstname.";
		}
		if ($find) {
			return TRUE;	
		} else {
			saveAction("ERROR changing $what to $toWhat failed","changeIssue");	
		}
	}	
}
?>
