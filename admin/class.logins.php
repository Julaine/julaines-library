<?php
/**
 * Update the htaccess files on the server
 * @package julaines-library
 */
/**
 * Class Logins
 * @version 1.0.1
 * @package julaines-library
 *
 */
class Logins  {
	/**
	 * htaccess handler
	 *
	 * @author    Sven Wagener <sven.wagener@intertribe.de>
	 * @copyright Intertribe - Internetservices Germany
	 * @notes Use crypt for passwords (not sha1 or md5 - see php.net)
	 * @notes Always set the salt so you know what it is for later.
	 * @param  $logins = new Logins($group);
	 */

	private static $location 	= 'empty';
	private static $authType 	= 'Basic';
	private static $authName 	= 'Basic'; //lowest security as default
	private static $fHtaccess 	= '';
	private static $fPasswd 	= '';
	private static $group 		= 'clients';

	public function __construct($group) {
		$this->location = PATH_ROOT.".htmaster";
		$this->group = $group;
		if ($group == "admin") {
			$this->authType="Basic"; // Default authentification type
			$this->authName="Admin"; // Default authentification name			
			$this->fHtaccess = PATH_ROOT."login/.htaccess";
			$this->fPasswd= $this->$location.'/.admin';
		} elseif ($group == "members") {
			$this->authType="Basic"; // Default authentification type
			$this->authName="Members"; // Default authentification name
			$this->fHtaccess = PATH_ROOT."members/.htaccess";
			$this->fPasswd= $this->$location."/.members";
		}
	}

	/**
	 * Add a user to the file
	 *
	 * @param $username
	 * @param $password
	 * @return bool|string
	 */
	function addUser($username,$password){
		$file=@fopen($this->fPasswd,"r");
		$isAlready=false;
		while($line=@fgets($file,200)){
			$lineArr=explode(":",$line);
			if($username == $lineArr[0]){
				$isAlready=TRUE;
			}
		}
		if($isAlready==false){
			$salt=substr($password,0,2);
			if (is_writable($this->fPasswd)) {
				// In our example we're opening $filename in append mode.
				// The file pointer is at the bottom of the file hence
				// that's where $somecontent will go when we fwrite() it.
				if (!$handle = fopen($this->fPasswd, 'a')) {
					return "Cannot open file ($this->fPasswd)";
				}
				$salt = substr($password,0,3);
				$newPassword=crypt($password,$salt);
				//$contents = $username.":".'{SHA}' . base64_encode($password)."\n";
				// Write $somecontent to our opened file.
				if (fputs($handle, utf8_encode($contents)) === FALSE) {
					return "Cannot write to file (".$this->fHtaccess.")";
				} else {
					fclose($handle);
					return TRUE;

				}
			} else {
				return "The file ".$this->fPasswd." is not writable";
			}
		} elseif ($isAlready == TRUE) {
			return TRUE;
		}
	}

	/**
	 * Remove a uesr from the file
	 * @param $username
	 * @return bool
	 */
	function delUser($username){
		$deleted = FALSE;
		$file=fopen($this->fPasswd,"r");
		$i=0;
		while($line=fgets($file,200)){
			$lineArr=explode(":",$line);
			if($username!=$lineArr[0]){
				$newUserlist[$i][0]=$lineArr[0];
				$newUserlist[$i][1]=$lineArr[1];
				$i++;
			}else{
				$deleted=TRUE;
			}
		}
		fclose($file);
		// Writing names back to file (without the user to delete)
		$file=fopen($this->fPasswd,"w+");
		for($i=0; $i<count($newUserlist); $i++){
			fputs($file,$newUserlist[$i][0].":".$newUserlist[$i][1]."\n");
		}
		fclose($file);
		return($deleted);
	}

	/**
	 * Set the password for the user
	 * @param $username
	 * @param $new_password
	 * @return bool|string
	 */
	function setPasswd($username,$new_password){
		// Reading names from file
		$isSet = FALSE;
		$newUserlist="";
		$file=fopen($this->fPasswd,"r");
		if ($file) {
			$x=0;
			for($i=0;$line=fgets($file,200);$i++){
				$lineArr=explode(":",$line);
				if($username!=$lineArr[0] && $lineArr[0]!="" && $lineArr[1]!=""){
					$newUserlist[$i][0]=$lineArr[0];
					$newUserlist[$i][1]=$lineArr[1];
					$x++;
				}else if($lineArr[0]!="" && $lineArr[1]!=""){
					$newUserlist[$i][0]=$lineArr[0];
					$newUserlist[$i][1]=$new_password."\n";
					$isSet=TRUE;
					$x++;
				}
			}
			fclose($file);
			unlink($this->fPasswd);
			/// Writing names back to file (with new password)
			$file=fopen($this->fPasswd,"w");
			for($i=0;$i<count($newUserlist);$i++){
				$content=$newUserlist[$i][0].":".$newUserlist[$i][1];
				fputs($file,$content);
			}
			fclose($file);
			return $isSet;
		} else {
			return "Password file not opened";
		}
	}
	/**
	 * Writes the htaccess file to the given Directory and protects it
	 * @see setFhtaccess()
	 */
	function addLogin(){
		if (is_writable($this->fHtaccess)) {
			// In our example we're opening $filename in append mode.
			// The file pointer is at the bottom of the file hence
			// that's where $somecontent will go when we fwrite() it.
			//if (!$handle = fopen($fHtaccess, 'a')) {
			$emptyFile = fputs($handle, "");
			if (!$emptyFile) {
				return "Cannot open file ($this->fHtaccess)";
			} else {
				//empty the file
				$contents = "AuthType        ".$this->authType."\n";
				$contents .= "AuthUserFile    ".$this->fPasswd."\n\n";
				$contents .= "AuthName        \"".$this->authName."\"\n";
				$contents .= "require valid-user\n\n";
				// Write $somecontent to our opened file.
				if (fputs($handle, utf8_encode($contents)) === FALSE) {
					return "Cannot write to file ($this->fHtaccess)";
				} else {
					echo "Success, wrote ($contents) to file ($this->fHtaccess)";
					fclose($handle);
				}
			}
		} else {
			return "The file $this->fHtaccess is not writable";
		}
	}

	/**
	 * Deletes the protection of the given directory
	 * @see setFhtaccess()
	 */
	function delLogin(){
		unlink($this->fHtaccess);
	}
	function updatePasswordFile($group) {
		$success = FALSE;
		$file=fopen($this->fPasswd,"r");
		if (!$file= fopen($this->fPasswd, 'a')) {
			return FALSE;
		}
		switch($this->$group) {
			case "$group":
				$findAll = Database::db("SELECT account_email,account_password FROM accounts
							WHERE account_type='$group' ORDER BY account_email",2);

		}
		if ($findAll) {
			unlink($this->fPasswd);
			$file=fopen($this->fPasswd,"w");
			foreach ($findAll as $row) {
				extract($row);
				$contents = $account_email.":".$account_password."\n";
				if (fputs($file, utf8_encode($contents)) === FALSE) {
					$success = FALSE;
				} else {
					$success = TRUE;
				}
			}
		}
		unset($contents);
		fclose($file);
		return $success;
	}
}
?>