<?php
/**
 * Account Admin
 * @package julaines-library
 */
/**
 * Class Admin
 * Use bcrypt for security
 *
 * @version 2.0.7
 * @package julaines-library
 *
 * @see http://sunnyis.me/blog/secure-passwords/
 * @see http://www.openwall.com/phpass/
 * @uses spoon
 *
 * @todo  Include a description of or MySQL code for the db tables
 * @todo Separate out the cookie stuff...
 */
class Admin {

    protected $db;

    function __construct() {
        // create database object
        $this->db = new SpoonDatabase(DB_TYPE, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        if (!$this->db) {
            $_SESSION['errors'] = 'Due to unforeseen circumstances, the site is unavaiable.
				Please try again later.';
        }

    }

    /**
     * Save a new account to the database
     * @param $postedValues
     * @return bool
     */
	function addAccount($postedValues) {
		extract($postedValues);
		if ($confirm_word == $new_word) {
            $newUword = crypt($new_word);

            //hey, you need to encrypt the password
            $account_param['firstname'] = $firstname;
            $account_param['lastname'] = $lastname;
            $account_param['accountLogin'] = $accountLogin;
            $account_param['accountPassword'] = $newUword;
            $account_param['email'] = $email;
            $account_param['accessLevel'] = $accessLevel;
            $accountId = $this->db->insert("accounts",$account_param);


            if ($accountId > 0) {
                if (Logins::updatePasswordFile("Admin") == TRUE) {
                    $_SESSION['success'] = 'New login account created.';
                    Bugs::setError("save",'New login account created.');
                    return TRUE;
                } else {
                    $_SESSION['errors'] = "The account saved to the db but the access was not updated.";
                    return FALSE;
                }
            } else {
                $_SESSION['errors'] = "The information was not saved to the database.";
                Bugs::setError("save","The information was not saved to the database.");
                return FALSE;
            }
        } else {
            $_SESSION['errors'] = "The passwords do not match.";
            return FALSE;
        }
    }
    /**
     * Find the comment group, the account holder belongs to
     *
     * @return string commentGroup value
     */
    function findCommentGroup() {
        $query = 'SELECT accessLevel FROM accounts
				WHERE accountId=?';
        $account_group = $this->db->getVar($query, $_SESSION['id']);
        if (!$account_group) return FALSE;
        return $account_group;
    }
    function findUserName() {
        $loginName = $_SESSION['ADMIN'];
        $firstname = NULL;
        $findUserId = Database::db("SELECT firstname FROM accounts WHERE accountId='$loginName'");
        if ($findUserId) {
            $firstname = $findUserId[0];
        }
        return $firstname;

    }

    /**
     * Generate a Random Secure Password
     *
     * @param int $length
     * @param int $strength
     * @return string password
     * @return string
     */
    public static function generatePassword($length=9, $strength=8) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        $length = (int)$length;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }
    function getAccess() {
        $id = (int)$_SESSION['id'];
        $query = 'SELECT accessLevel FROM accounts
				WHERE accountId=?';
		$accessLevel = $this->db->getVar($query,$id);
        if (!$accessLevel) return FALSE;
        return $accessLevel;
    }
    
    /*
     * Get Account holder's name
     *
     * Relies on the recognition of a server
     * authentication
     */
    
    function getName() {
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $loginName = $_SERVER['PHP_AUTH_USER'];
            $findUserId = Database::db("SELECT firstname FROM accounts WHERE accountLogin='$loginName'");
            if ($findUserId) {
                $firstname = $findUserId[0];
                return $firstname;
            }
        } else {
            return FALSE;
        }

    }

    /**
     * Get the name and email for the account holder
     * using the accountI
     *
     * @param $accountId
     * @internal param int accountId
     * @return firstname and lastname
     */
    function getNameEmail($accountId) {
        $query = 'SELECT account_firstname,account_lastname,account_email FROM accounts
					WHERE accountId=?';
        return $this->db->getRecord($query, (int)$accountId);
    }

    /**
	* Borrowed from Julaine
	* with changes
	*
	* @return mixed
	*/
	public static function getUserId() {
        //$loginName = $_SERVER['PHP_AUTH_USER'];
        if (isset($_SERVER['REDIRECT_REMOTE_USER'])) {
            $loginName = $_SERVER['REDIRECT_REMOTE_USER'];
        } elseif (isset($_SERVER['PHP_AUTH_USER'])) {
            $loginName = $_SERVER['PHP_AUTH_USER'];
        } elseif (isset($_SESSION['id'])) {
            $loginName = (int)$_SESSION['id'];
        } else {
            $loginName = 1;
        }



        $findUserId = Database::db("SELECT accountId FROM accounts WHERE accountId='$loginName'");
        if ($findUserId) {
            return  $findUserId[0];
        } else {
            return 0;
        }
	}
    /**
     * Save the new password
     * @return  Boolean success or fail
     * @param  string 	$pword 		New password
     * @param  int 		$account_id Realtor's account Id
     */
    function saveNewPassword($pword,$account_id) {
        //these should not work
        //$this->portable_hashes = FALSE;
        //$this->iteration_count_log2 = 9;

        $hasher = new PasswordHash(8,FALSE);
        $hash = $hasher->HashPassword($pword);
        $save = $this->db->update("accounts",array("accountPassword"=>$hash),'accountId=?',$account_id);
        if ($save > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    /**
     * Create sessions for protected areas
     * @note     decided NOT to use session for name
     *
     * @param int $accountId
     * @internal param string $name
     * @return int
     */
    function setAccess($accountId) {
		$_SESSION['account'] = $accountId;
    }
    /**
     * Create a hidden form field
     * @param $id
     * @param $fieldname
     * @param $fieldvalue
     * @return string
     *
     */
    public static function createHiddenForm($id,$fieldname,$fieldvalue) {

        $display = <<<DISPLAY
<a href="#$fieldname$id" data-target="#$fieldname$id" data-toggle="collapse">$fieldvalue</a>
<div class="collapse" id="fieldname$id">
<form name="fieldname" action="logins.php" method="post">
<div class="form-group">
<input type="text" name="fieldname" class="form-control" placeholder="$fieldvalue">
</div>
<input type="submit" value="Save" class="btn btn-primary btn-sm">
<input type="hidden" name="id" value="$id">
</form>
</div>
DISPLAY;
        return $display;
    }

    /**
     * Not working; so I switched and put the code in the file directly
     *
     * @param $accessLevel
     * @throws SpoonDatabaseException
     * @throws SpoonFormException
     * @throws SpoonTemplateException
     */
    function showAddAccountForm($accessLevel) {
        $frm = new SpoonForm('account_add');
        $frm->setAction('login_add.php');
        $frm->setParameter('class','form-horizontal');
        $frm->addText('account_firstname', NULL, 40, 'form-control')
            ->setAttribute('required')
            ->setAttribute('placeholder', 'First Name');
        $frm->addText('account_lastname', NULL, 40, 'form-control')
            ->setAttribute('required')
            ->setAttribute('placeholder', 'Last Name');

        $frm->addText('account_email', NULL, 200, 'form-control')
            ->setAttribute('type', 'email')
            ->setAttribute('placeholder', 'Email address')
            ->setAttribute('class', 'form-control');
        /*$frm->addText('account_phone', NULL, 20, 'form-control')
            ->setAttribute('type', 'tel')
            ->setAttribute('placeholder', 'Phone Number');*/
        // add hidden value(s)
        $frm->addHidden('accessLevel', $accessLevel);
        // add submit button
        $frm->addButton('submit', 'Save & Notify')
            ->setAttribute('class', 'btn btn-primary');

        if($frm->isSubmitted()) {
            $frm->getField('account_firstname')->isFilled('Please provide a first name.');
            // email is required
            $frm->getField('account_lastname')->isFilled('Please provide a last name.');
            // email is required
            $frm->getField('account_email')->isEmail('Please provide a valid email address.');

            if($frm->isCorrect()){
                // all the information that was submitted
                $postedValues = $frm->getValues();
                extract($postedValues);

                //must generate a password for this new realtor
                $new_password = $this->generatePassword();

                $hasher = new PasswordHash(8,false); //unique to ASDS
                $hash = $hasher->HashPassword($new_password);

                $params = array('account_firstname' => $account_firstname,
                                'account_lastname'=> $account_lastname,
                                'accountLogin'=> $account_email,
                                'accountPassword'=> $hash,
                                'account_email'=> $account_email,
                                'accessLevel'=> $accessLevel,
                                'account_status'=> 1,
                                'accountUpdate' => date("Y-m-d h:i:a",time())
                );
                $accountId = $this->db->insert('accounts', $params);

                if ($accountId > 0) {
                    /**
                     * @todo
                     */

                    $emails = new Emails;
                    $parameters = array();
                    $parameters['account_firstname'] = $account_firstname;
                    $parameters['password'] = $new_password;

                    $send = $emails->sendMessageByEmail("$account_firstname $account_lastname",
                        $account_email,1, $parameters);
                    if ($send == TRUE) {
						$_SESSION['success'] = 'Thank you, an email was sent to the account
						holder with their account login information.';
                    } else {
                        //REFACTOR
                        //should I also log the times this fails
						$_SESSION['errors'] = 'Failed to send email';
                    }
                } else {
					Bugs::setError("save",'Did not create an account for '.$account_firstname." ".$account_lastname);

                }
                foreach($_POST as $key => $value) {
                    unset($_POST[$key]);
                }
                header('Location: logins.php');
                die;
            } else {
				$_SESSION['errors'] = 'Did not create an account';
            }
        }
        $tpl = new SpoonTemplate();
        $tpl->setForceCompile(true);
        $tpl->setCompileDirectory(PATH_ROOT.'cache');
        $frm->parse($tpl);
        $tpl->display(PATH_TEMPLATES.'admin/form_account_add.tpl');
    }

    /**
     * Show form to reset password based on email
     * Updated to only check valid accounts.
     *
     * @return Boolean
     *
     */
    function showForgotPasswordForm() {
        // create form
        $frm = new SpoonForm('forgot');
        $frm->setAction("index.php");
        $frm->setParameter('class','form-horizontal');

        // add email (input field)
        $frm->addText('email_address')
            ->setAttribute('type', 'email') // html5 'email' type attribute
            ->setAttribute('required')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Email address'); // html5 'email' type attribute

        // add hidden value(s)
        $frm->addHidden('process2', 2);
        // add submit button
        $frm->addButton('send', 'Send Password')
            ->setAttribute('class', 'btn btn-success');

        if($frm->isSubmitted()) {
            if ((int)$frm->getField('process2')->getValue() !== 1) {
				$_SESSION['errors'] = 'Please try again.';
            }
            $frm->getField('email_address')->isEmail('Please provide a valid email address.');
            if($frm->isCorrect()){
                // all the information that was submitted
                $postedValues = $frm->getValues();
                $query = 'SELECT accountId
						FROM accounts
						WHERE account_email=? AND account_status=?';
                $accountId = $this->db->getVar($query,array($postedValues['email_address'],1));

                if ($accountId > 0){
                    $admin = new Admin;
                    $pword = $admin->generatePassword();
                    if ($this->saveNewPassword($pword,$accountId) == TRUE) {
                        $notices = new Notices;
                        //if ($this->sendRealtorNewPassword($postedValues['email_address'],$pword) == TRUE) {
                        if ($notices->notifyNewPassword($pword,$accountId) == TRUE) {
                            $_SESSION['success'] = 'Please check your email for your new password.';
                        } else {
							$_SESSION['errors'] = 'Eek, something went wrong sending the email.';
                        }
                    } else {
						$_SESSION['errors'] = 'Failed to save new password.';
                    }
                } else {
					$_SESSION['errors'] = 'Please try again.';
                }
            }
        }
        $tpl = new SpoonTemplate();
        $tpl->setForceCompile(true);
        $tpl->setCompileDirectory(PATH_ROOT.'cache');
        $frm->parse($tpl);
        $tpl->display(PATH_TEMPLATES.'admin/form_forgot.tpl');
    }

    /**
     * Sign in form
     *
     * @uses Spoon
     *
     */
    function showSignInForm() {
        // create form
        $frm = new SpoonForm('signin');
        $frm->setAction("index.php");
        $frm->setParameter('class','form-horizontal');

        // add email (input field)
        $frm->addText('email')
            ->setAttribute('type', 'email') // html5 'email' type attribute
            ->setAttribute('required')
            ->setAttribute('class','form-control')
            ->setAttribute('maxlength','150')
            ->setAttribute('placeholder', 'Email address'); // html5 'email' type attribute

        // add preferred password (password)
        $frm->addPassword('password')
            ->setAttribute('class','form-control')
            ->setAttribute('maxlength','40');
        // add hidden value(s)
        $frm->addHidden('process', 1);
        // add submit button
        $frm->addButton('submit', 'Enter')
            ->setAttribute('class', 'btn btn-primary');

        if($frm->isSubmitted()) {
            sleep(1); // 1 second delay

            $frm->getField('email')->isEmail('Please provide a valid email address.');
            $frm->getField('password')->isFilled('Please choose a password');
            if($frm->isCorrect()){
                // all the information that was submitted
                $postedValues = $frm->getValues();
                $email = $postedValues['email'];


                $hasher = new PasswordHash(9,false);
                $new_hash = $hasher->HashPassword($postedValues['password']);

                $find_account = 'SELECT accountId,accessLevel,accountPassword,account_firstname
								FROM accounts
								WHERE account_email=? AND account_status=?';
                $account_info = $this->db->getRecord($find_account, array($email,1));
                if ($account_info) {
                    extract($account_info);

                    //echo $accountPassword;
                    $debugger = new Debugger;
                    $parameters = array("accountId"=>$accountId,
                                        "password"=>$new_hash,
                                        "ip"=>$debugger->getRealIp(),
										"dateLogin"=>date("Y-m-d h:i:a",time()));
                    $this->db->insert("accountsLogins",$parameters);

                    if ($hasher->CheckPassword($postedValues['password'], $accountPassword) == TRUE) {
                        // set session variables
                       $_SESSION['id'] = $accountId;
						$_SESSION['name'] = $account_firstname;


                        header('Location: '.SITE_URL.'login/pages/');
                        die;
                    } else {
						$_SESSION['errors'] = 'Please try again, the login does not  match.';
                    }
                } else {
                    $debugger = new Debugger;
                    $parameters = array("accountId"=>0,
                                        "password"=>$new_hash,
                                        "ip"=>$debugger->getRealIp(),
										"dateLogin"=>time());
                    $this->db->insert("accountsLogins",$parameters);

					$_SESSION['errors'] = 'Please try again, did not find your account.';
                }
            } else {
				$_SESSION['errors'] = "Please try again, did not find your account.";

            }
            header('Location: index.php');
            die;
        }
        $tpl = new SpoonTemplate();
        $tpl->setForceCompile(true);
        $tpl->setCompileDirectory(PATH_ROOT.'cache');
        $frm->parse($tpl);
        $tpl->display(PATH_TEMPLATES.'admin/form_signin.tpl');
    }
    /**
     * Show form for updating personal account
     * @param $id
     * @return string
     */
    function showUpdateAccountForm($id) {

        $query = 'SELECT account_firstname,account_lastname,account_email,
				account_phone
				FROM accounts
				WHERE accountId=?';
        $find = $this->db->getRecord($query,$id);
        if ($find) extract($find);
        // create form
        $frm = new SpoonForm('update');
        $frm->setAction("myaccount.php");
        $frm->setParameter('class','form-horizontal');
        $frm->addText('account_firstname', $account_firstname, 40, 'form-control')
            ->setAttribute('required')
            ->setAttribute('placeholder', 'First Name');
        $frm->addText('account_lastname', $account_lastname, 40, 'form-control')
            ->setAttribute('required')
            ->setAttribute('placeholder', 'Last Name');
        // add email (input field)
        $frm->addText('account_email', $account_email, 150, 'form-control')
            ->setAttribute('type', 'email') // html5 'email' type attribute
            ->setAttribute('required')
            ->setAttribute('placeholder', 'Email address');
        $frm->addText('account_phone', $account_phone, 20, 'form-control')
            ->setAttribute('type', 'tel')
            ->setAttribute('required')
            ->setAttribute('placeholder', 'Phone Number');
        // add preferred password (password)
        $frm->addText('new_password', NULL, 40, 'form-control');
        $frm->addText('new_password2', NULL, 40, 'form-control');
        // add hidden value(s)
        $frm->addHidden('process', 1);
        $frm->addHidden('id', $id);
        // add submit button
        $frm->addButton('submit', 'Save')
            ->setAttribute('class', 'btn btn-primary btn-medium');

        if($frm->isSubmitted()) {
            sleep(1); // 1 second delay
            if ((int)$frm->getField('process')->getValue() !== 1) {
				$_SESSION['success'] = 'Please try again.';
            }
            $frm->getField('account_email')->isEmail('Please provide a valid email address.');

            if($frm->isCorrect()){
                // all the information that was submitted
                $postedValues = $frm->getValues();
                extract($postedValues);
                $updateAcc = array();
                $updateAcc['account_email'] = $account_email;
                $updateAcc['account_phone'] = $account_phone;
                $updateAcc['account_firstname'] = $account_firstname;
                $updateAcc['account_lastname'] = $account_lastname;
                if (strlen($new_password) > 1) {
                    if ($new_password !== $new_password2) {
						$_SESSION['errors'] = 'Please enter matching passwords.';
                    }

                    $hasher = new PasswordHash(8,false);
                    $updateAcc['accountPassword'] = $hasher->HashPassword($new_password);

                }
                $updateAcc['account_update'] = date("Y-m-d h:i:a",time());

                $save = $this->db->update("accounts",$updateAcc,'accountId=?',$id);

                if ($save > 0) {
					$_SESSION['name'] =  $account_firstname;

					$_SESSION['success'] = 'Thank you, the information is now updated.';
                } else {
					$_SESSION['errors'] = 'Please try again';
                }
                //header("Location: myaccount.php");
                //die;
            }
        }
        $tpl = new SpoonTemplate();
        $tpl->setForceCompile(true);
        $tpl->setCompileDirectory(PATH_ROOT.'cache');
        $frm->parse($tpl);
        $tpl->display(PATH_TEMPLATES.'admin/form_myaccount.tpl');
    }
    /**
     * Update the account details in the database
     * I choose to keep usage of database class for now
     *
     * @todo change the updatePasswordFile call - not in this file
     */
    function updateAccount($postedValues) {
        $database = new Database;
        extract($postedValues);
        if ($confirm_word !== $new_word) {
            return FALSE;
        } else {
            if ($confirm_word == $new_word && $confirm_word !== "") {
                //$this->portable_hashes = FALSE;
                //$this->iteration_count_log2 = 9;
                $hasher = new PasswordHash(8,false);

                $newUword = $hasher->HashPassword($new_word);

                $saveLogin = $database->db("UPDATE accounts SET
							accountLogin='$accountLogin',accountPassword='$newUword',email='$email',
							firstname='$firstname',lastname='$lastname',accessLevel='$accessLevel' 
							WHERE accountId='$id'",2);
            } else {
                $saveLogin = $database->db("UPDATE accounts SET
							accountLogin='$accountLogin',email='$email',firstname='$firstname',
							lastname='$lastname',accessLevel='$accessLevel' 
							WHERE accountId='$id'",2);
            }
            if ($saveLogin) {
				$_SESSION['success'] = "The login account was updated";
                return TRUE;
            } else {
				$_SESSION['errors'] = "The information was not saved to the database.";
                return FALSE;
            }
        }
    }
}
?>