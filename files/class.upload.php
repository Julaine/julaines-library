<?php
/**
 * Upload files for the catalogue
 * @package julaines-library
 */
/**
 * Class Uploaded Files
 * @package julaines-library
 * @author		Julaine Scott
 * @version  		1.0.2
 * @uses 		Spoon
 */
class UploadedFiles {
	protected $db_connect;
	/**
	 * Open a db connection
	 *
	 * @return setup db connection
	 */
	function __construct() {
		// create database object
		$this->db_connect = new SpoonDatabase(DB_TYPE, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

	}
	/**
	 * Clean the name for the file
	 * @usage called in showAttachForm
	 */

	function createNewName($form_id,$file) {
		$find_array = array(',','"'," ");
		$replace_array = array('','','');
		//$new_name = str_pad($form_id,5,0,STR_PAD_LEFT).'_'.str_replace($find_array,$replace_array,$file);
        $new_name = str_replace($find_array,$replace_array,$file);
		return $new_name;
	}
	/**
	 * Use this function to update deal_app and automate saving files
	 * when uploaded as multiples

	 * @param $name
	 * @param $filesArray
	 * @param $resourceId
	 * @param $account_id
	 * @param $form_desc
	 * @param $page
	 */
	function saveUploadedFiles($name,$filesArray,$resourceId,$account_id,$form_desc,$page) {
		$num = count($filesArray['tmp_name']);

		Bugs::setError('save','Hey, there are '.$num.' '.$name.' files');
		for($i=0; $i<$num; $i++) {
			$j = $i+1;
			//Get the temp file path
			$tmpFilePath = $filesArray['tmp_name'][$i];
			$form_filename = $filesArray['name'][$i];
			Bugs::setError("save","uploading file $form_filename ($tmpFilePath)",$page);
			//Make sure we have a filepath
			if ($tmpFilePath != ""){
				//Upload the file into the temp dir
				$form_param = array('account_id'=>$account_id,'resourceId'=>$resourceId, 'form_filename'=>$form_filename,
									'form_desc' => $form_desc.' '.$j,
									'form_added'=> date("Y-m-d h:i:a",time()));
				$form_id = $this->db_connect->insert('forms', $form_param);

				if ($form_id > 0) {
					$new_name = $this->createNewName($form_id,$form_filename);
					$success = SpoonFile::move($tmpFilePath, PATH_DOCUMENTS.$new_name, FALSE, 0777);

					if ($success) {
						//$this->renameUploads($form_filename,$form_id);
					} else {
						Bugs::setError('notify','Hey the form '.$form_filename.' did not upload.');
					}
				} else {
					Bugs::setError('notify','Hey the form '.$form_filename.' did not upload.');
				}
			}
		}
	}

	/**
	 * This is a new function and could be useful on the listing edit page.
	 * @todo Not used anywhere
	 *
	 * @param  [type] $memberId [description]
	 * @return form
	 */

	function showForms4Member($memberId) {
		$find_forms = 'SELECT form_id,form_filename,form_desc FROM forms
						WHERE resourceId=?
						ORDER BY form_desc,form_filename';
		$rows = $this->db_connect->getRecords($find_forms,$memberId);
		if ($rows) {
			$string = array();
			$string[] ='<div class="panel panel-primary"><div class="panel-heading"><i class="icon-file"></i> Files</div>
 			<ul class="list-group">';
			foreach ($rows as $values) {
				extract($values);
				if (strlen($form_filename) > 2) {
					//no I have to do a redirect as they are outside the server??
					$string[] = '<li class="list-group-item"><a href="download.php?id='.$form_id.'" target="_blank"><strong>'.
						$form_desc.'</strong>: '.$form_filename.'</a> </li>';
				}
			}
			$string[] ='</ul></div>';
			return join("\n",$string);
		}
		return FALSE;
	}

}

?>