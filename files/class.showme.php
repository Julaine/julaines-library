<?php
/**
 * Show me a list of directories
 * and filenames
 *
 * @package julaines-library
 */
/**
 * Class Show Me
 * @version 1.0.1
 * @author  Julaine Scott <work@julaine.ca>
 * @package julaines-library
 * 
 */
class ShowMe {
	public function dir_contents_recursive($dir) {
	    // open handler for the directory
	    $iter = new DirectoryIterator($dir);
	    $content = array();
	    foreach( $iter as $item ) {
	    	// make sure you don't try to access the current dir or the parent
	    	if ($item != '.' && $item != '..') {
	    		if( $item->isDir() ) {
	    			// call the function on the folder
	    			dir_contents_recursive("$dir/$item");
	    		} else {
	    			// print files
	    			$dir_fix = str_replace(PATH_SITE,'',$dir);
	    			
	    			$content[] = '<div class="col-xs-6 col-md-3"><div class="thumbnail">
	    						<img src="/'.$dir_fix . "/" .$item->getFilename() . '" alt=" ">
	    						</div></div>';
	    		}
	    	}
	    }
	  return join("\n",$content);
	}
	
}
?>