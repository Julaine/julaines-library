<?php
/**
 * manage files on the server
 * @package julaines-library
 */
/**
 * This class controls files
 * Setup when cleaning up functions
 * @package julaines-library
 * @version 0.0.2
 *
 */
class Files {

	/**
	 * Used in CatalogueFiles and the CMS
	 * pulled out of functionsRoot
	 *
	 * @param $destinationFilename
	 * @param $destinationPath
	 * @param $destination
	 * @return bool
	 */
	public static function doesDestinationExist($destinationFilename,$destinationPath,$destination) {
		if (!file_exists($destinationFilename)) {
			Bugs::setError("debug","The file $destinationFilename does not exist in $destination, attempt to create.");
			if (!is_dir($destination)) {
				Bugs::setError("save","The path $destinationPath does not exist in $destination, attempt to create.");
				chmod($destinationPath, intval(0777)); //change the permission on the parent
				mkdir($destination, 0755);
				if (is_dir($destination)) {
					Bugs::setError("save","new directory created $destinationPath");
				} else {
					Bugs::setError("debug", "$destination not created in $destinationPath");
					return FALSE;
				}
				chmod($destinationPath, intval(0755)); //change the permission on the parent
				if (!is_dir($destination)) {
					$_SESSION['errors'] = "new directory ($destination) not created ";
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	/**
	 * Determine the date the file was last touched
	 * @param $filename
	 * @return bool|string
	 */
    public static function showLastUpdated($filename) {
        if (file_exists($filename)) {
            $last_date = filemtime($filename);
            if (isset($last_date)) {
                return 'Last updated: ' . date("F d Y H:i:s.", filemtime($filename));
            } else {
				echo 'no date';
			}
        } else {
			return 'No file '.$filename;
		}
        return false;
    }
	/**
	 * Create a new version of a file on the server
	 *
	 * @param      $destinationFilename
	 * @param      $newFileContents
	 * @return bool
	 * @throws SpoonFileException
	 */
	public static function updateFile($destinationFilename,$newFileContents) {
		Bugs::setError("debug","$destinationFilename is the filename");
		SpoonFile::delete($destinationFilename);
		return SpoonFile::setContent($destinationFilename, $newFileContents, $createFile = true, $append = false,
									 $chmod = 0715);
	}
}
?>