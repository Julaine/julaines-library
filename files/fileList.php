<?php
/**
 * What is this file? Am I using it?
 * @version  1.0.1
 * from PHP Solutions: Dynamic Web Design Made Easy, Second Edition 
 * @param  [type] $dir
 * @param  [type] $extensions
 * @return string
 */
function buildFileList($dir, $extensions) {
  if (!is_dir($dir) || !is_readable($dir)) {
     return false;
  } else {
     if (is_array($extensions)) {
       $extensions = implode('|', $extensions);
     }
     $pattern = "/\.(?:{$extensions})$/i";
     $folder = new DirectoryIterator($dir);
     $files = new RegexIterator($folder, $pattern);
     $filenames = array();
     foreach ($files as $file) {
       $filenames[] = $file->getFilename();
     }
     natcasesort($filenames);
     return $filenames;
  }
}
?>
------------------

<?php
$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator('../images'));
$images = new RegexIterator($files, '/\.(?:jpg|png|gif)$/i');
foreach ($images as $file) {
  echo $file->getRealPath() . '<br>';
}
?>
------ 
 <form id="form1" name="form1" method="post" action="">
  <select name="pix" id="pix">
    <option value="">Select an image</option>
    <?php
     $files = new DirectoryIterator('../images');
     $images = new RegexIterator($files, '/\.(?:jpg|png|gif)$/i');
     foreach ($images as $image) {
     ?>
    <option value="<?php echo $image; ?>"><?php echo $image; ?></option>
    <?php } ?>
  </select>
  </form>
