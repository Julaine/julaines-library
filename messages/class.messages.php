<?php
/**
 * Message handler
 * @package julaines-library
 */
/**
 * Class Messages
 * @package julaines-library
 * @version 1.0.4
 * @uses  Spoon
 */
class Messages {
	/**
	 * Are there any other sessions to destroy?
	 */
	public static function destroy() {
		//destroy all sessions
		if (isset($_SESSION['success'])) {
			unset($_SESSION['success']);
		}
		if (isset($_SESSION['errors'])) {
			unset($_SESSION['errors']);
		}
		if (isset($_SESSION['lastFormVars'])) {
			unset($_SESSION['lastFormVars']);
		}
	}
	/**
	 * Retrieve a message from the db
	 *
	 * @return array of message and message_file
	 * @param  int $message_id optional
	 * @param  string $message optional
	 */
	function getMessage($message_id=NULL,$message=NULL) {
		if (!is_null($message_id)) {
			$find = 'SELECT message
					FROM messages
					WHERE message_id=?';
			$message = $this->db->getVar($find, $message_id);
		}
		if (!is_null($message)) {
			$find = 'SELECT message
					FROM messages
					WHERE message=?';
			$message = $this->db->getVar($find, $message);
		}
		if (!$message) return FALSE;
		return $message;
	}

	/**
	 * show error messages stored in a session
	 * 
	 * @return string html code
	 */
	public static function messages_error() {
	   $message = array();
		if (isset($_SESSION['errors'])) {
		//@todo this front end code changes per site
		//move the div tag into the header.php
			$message[] = '<div class="alert alert-danger">';
			if (is_array($_SESSION['errors'])) {
				foreach ($_SESSION['errors'] as $k=>$value) {
					$message[] = "<p>$value";
				}
			} else {
				$message[] = $_SESSION['errors'];	
			}
			$message[] = '</div>';
			return join("\n",$message);
		}
		return FALSE;   

	}

	/**
	 * Remove Spoon error messages
	 * @return bool|string
	 */
	public static function messages_error_spoon() {
	   $message = array();
		if (SpoonSession::exists('errors')) {
		//@todo this front end code changes per site
		//move the div tag into the header.php
			$message[] = '<div class="alert alert-danger">';
			if (is_array(SpoonSession::get('errors'))) {
				foreach (SpoonSession::get('errors') as $k=>$value) {
					$message[] = "<p>$value";
				}
			} else {
				$message[] = SpoonSession::get('errors');	
			}
			$message[] = '</div>';
			return join("\n",$message);
		}
		return FALSE;   

	}
	/**
	 * show success messages stored in a session
	 * 
	 * @return string html code
	 */
	public static function messages_success() {
	   $message = array();
	   if (isset($_SESSION['success'])) {
			$message[] = '<div class="alert alert-success">';
			if (is_array($_SESSION['success'])) {
				foreach ($_SESSION['success'] as $k=>$value) {
					$message[] = "<p>$value";
				}
			} else {
				$message[] = $_SESSION['success'];
			}
			$message[] = '</div>';
			return join("\n",$message);
		} 
		return FALSE;
	   	
	}

	/**
	 * show success messages stored in spoon session
	 * @return bool|string
	 */
	public static function messages_success_spoon() {
	   $message = array();
	   if (SpoonSession::exists('success')) {
			$message[] = '<div class="alert alert-success">';
			if (is_array(SpoonSession::get('success'))) {
				foreach (SpoonSession::get('success') as $k=>$value) {
					$message[] = "<p>$value";
				}
			} else {
				$message[] = SpoonSession::get('success');
			}
			$message[] = '</div>';
			return join("\n",$message);
		} 
		return FALSE;
	   	
	}
	/**
	 * Message handler
	 * 
	 * @return string HTML code
	 */
	public static function msgHandler() {
		if (self::messages_error() !== FALSE || self::messages_success() !== FALSE) {
			echo '<div class="row">';
			echo self::messages_error();
			//echo self::messages_error_spoon();
			echo self::messages_success();
			//echo $this->messages_success_spoon();
			echo '</div>';
		// destroy session - logs out the user by destroying his/her session
		//SpoonSession::delete('success');
		//SpoonSession::delete('errors');
		}
	}	
}
?>