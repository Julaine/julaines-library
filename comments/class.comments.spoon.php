<?php
/**
 * Add comments to things
 * Uses Spoon db handler
 *
 * @package julaines-library
 */
/**
 * Class Comments
 * Better showComments Display
 * @uses Spoon
 * @version 0.0.1
 * @uses	Bootstrap3
 * @package julaines-library
 */
class Comments {
	protected $db;
	function __construct() {
		// create database object
		$this->db = new SpoonDatabase(DB_TYPE, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		

	}
	/**
	 * Add a new comment
	 * @todo  check for usage and remove second parameter
	 * 
	 * @param int  $comment
	 * @param [type]  $group         [description]
	 * @param [type]  $type          [description]
	 * @param [type]  $commentTypeId [description]
	 * @return bool
	 */
	function addComment($comment,$group2remove,$type,$commentTypeId) {
		if (strlen($comment) <= 1) {
			return FALSE;
		}
		$admin = new Admin;
		$group = $admin->findCommentGroup();
		$param = array();
		$param['commentTypeId'] = $commentTypeId;
		$param['commentGroup'] = $group;
		$param['commentType'] = $type;
		$param['comment'] = $comment;

		$comment_id = $this->db->insert("comments",$param);

		if($comment_id > 0) {
			return TRUE;
		} else {
			$logs->write("WARNING did not save comment",'custom');
			return FALSE;
		}
	}

	/**
	 * Fetch the comments for a particular type
	 *
	 * @param        $id
	 * @param string $type
	 * @return null
	 */
	function getComment($id,$type="Lead") {
		$find = 'SELECT accountId,comment,commentUpdate FROM comments 
				WHERE commentType=? AND commentTypeId=?';
		$rows = $this->db->getRecord($find,array($type,$id));
		if (!$rows) return NULL;
		extract($rows);
		return $comment;
	}
	/**
	 * Save a comment
	 *
	 * @param  str $comment 
	 * @param  str $type    
	 * @param  int $id      
	 * @return boolean      
	 */
	function saveComment($comment,$type,$id) {
		if (strlen($comment) > 1) {
			$admin = new Admin;
			$group = $admin->findCommentGroup();
			//does the comment already exist?
			$query = 'SELECT commentId FROM comments
					WHERE commentTypeId=? AND commentType=?';
			$commentAlready = $this->db->getVar($query,array($id,$type)); 
			if ((int)$commentAlready > 0) {
				$save = $this->editComment($commentAlready,array('accountId'=>$_SESSION['id'],'comment'=>$comment));
			} else {
				$save = $this->addComment($comment,$group,$type,$id);
			}
			if ($save == FALSE) {

				Bugs::setError('save','WARNING Comment did not save');
			}
			return $save;
		} else {
			return FALSE;
		}
	}
	/**
	 * Show All Comments
	 *
	 * With optional type and secondary type exception
	 * or by group or exclude
	 *
	 * @param  [type]  $id            [description]
	 * @param  string  $type          [description]
	 * @param  [type]  $secondaryType [description]
	 * @param  [type]  $group         [description]
	 * @param  [type]  $exclude       [description]
	 * @return string
	 */
	function showComments($id,$type="order",$secondaryType=NULL,$group=NULL,$exclude=NULL) {
		$parameters = array();
		$findLine = 'SELECT c.commentId,c.accountId,c.comment,c.commentGroup,c.commentUpdate,a.account_firstname AS commentName 
						FROM comments AS c LEFT JOIN accounts AS a ON a.accountId=c.accountId WHERE ';
		if (is_null($secondaryType)) {
			$findLine .= 'c.commentType=? ';			
			$parameters[] = $type;
		} else {
			$findLine .= '(c.commentType=? OR c.commentType=? ';	
			$parameters[] = $type;	
			$parameters[] = $secondaryType;	
		}
		if (is_null($group)) {			
			$findLine .= 'AND c.commentTypeId=? ';
			$parameters[] = $id;		
		} else {
			$findLine .= 'AND c.commentTypeId=? AND c.commentGroup=? ';
			$parameters[] = $id;
			$parameters[] = $group;		
		}
		if (!is_null($exclude)) {
			$findLine .= 'AND c.commentGroup !=? ';	
			$parameters[] = $exclude;
		}
		$findLine .= ' ORDER BY c.commentId,c.commentUpdate ASC';
		$find = $this->db->getRecords($findLine,$parameters);
		if ($find) {
			$showComments = array();
			foreach ($find as $v) {
				extract($v);
				$commentUpdated = date("M d, y",strtotime($commentUpdate));
				if ((int)$accountId > 0)  {
					$commentName = " by $commentName"; 
				} else {
					$commentName = " by server ";	
				}
				$showCommentForm = '';
				if ($type !== "order") {
					$showCommentForm = $this->wrapCommentsWithForm($commentId,$comment,$accountId,$type,$id);
				}
				$showComments[] = <<<COMMENTS
<div class="panel panel-info">
<div class="panel-heading">
<div class="caption"><h3 class="panel-title">[$commentId] $commentUpdated comment by $commentName</h3></div>
<div class="btn-group">
	<button class="btn btn-default btn-xs" data-toggle="collapse" data-target="#comment$commentId">
	<i class="fa fa-edit"></i></button>
	<a href="delete_comment.php?id=$commentId&amp;return=$id" class="btn btn-default btn-xs"><i class="fa
	fa-trash"></i></a>
</div></div>
<div class="panel-body">

<p>$comment
$showCommentForm								

</div><!--end of panel-body-->
</div>
COMMENTS;
			}

			return implode("\n",$showComments);
		} else {
			return "No comments";	
		}
	}


	/**
	 * Delete a comment from database
	 * @param $commentId
	 * @return bool
	 */
	function deleteComment($commentId) {
		return $this->db->delete("comments","commentId=?",$commentId);
		return TRUE;
	}

	/**
	 * Update the comments with changes
	 * @param $commentId
	 * @param $postedValues
	 * @return bool
	 */
	function editComment($commentId,$postedValues) {
		$commentArray = array();
		foreach ($postedValues as $k=>$v) {
			$commentArray[$k] = $v; 
		}
		$commentArray['commentUpdate'] = date("Y-m-d h:i:a",time());
		$updateSave = $this->db->update("comments",$commentArray,'commentId=?',$commentId);

		if ($updateSave > 0) {
			return TRUE;
		} else {
			return FALSE;	
		}
	}
	/**
	 * Wrap Comments in a Form so they are editable
	 * Do I need this function?
	 * 
	 * @param  int $commentId [description]
	 * @param  string $comment   [description]
	 * @param  int $commentBy [description]
	 * @param  string $type      [description]
	 * @return string 			HTML code
	 */
	function wrapCommentsWithForm($commentId,$comment,$commentBy,$type="order",$issueId) {
		//type is the first part of the table name
		//need a string of values for editing the form
		//use 999 for process to prevent duplication
		//2013-02-15
		//I added id as a generic way of supplying return id number for issues, and such
		if (!(int)$commentBy > 0) {
			$commentBy = "";
		}
		$commentEdit = <<<HEREDOC
<div class="collapse" id="comment$commentId">
<form name="editComment" method="post" action="save_comment.php">
<p><textarea name="internal_comment" cols="80" rows="4" class="textarea">$comment</textarea></p>
<input name="Submit" type="submit" class="btn btn-primary" value="Save">
<input type="hidden" name="process" value="999">
<input type="hidden" name="commentId" value="$commentId">
<input type="hidden" name="id" value="$issueId">
</form>
</div>
HEREDOC;
		return $commentEdit;
	}
}?>
