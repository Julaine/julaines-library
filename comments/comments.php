<?php
class commentsTools {
	function addComment($comment,$group,$type,$commentTypeId,$accountId=NULL,$orderId=NULL,$debug=2) {
		//NOTE save all comments for now
		if (is_null($accountId) || $accountId == "") { $accountId = 1; } //getUserId(); }
		$comment = addslashes($comment);
		$saveComment = db("INSERT INTO comments 
				(commentTypeId,orderId,accountId,commentGroup,commentType,comment)
				VALUES ('$commentTypeId','$orderId','$accountId','$group','$type','$comment')",$debug);
		if($saveComment > 0) {
			return TRUE;
		} else {
			error_log("WARNING did not save comment");
			return FALSE;
		}
	}
	function showNames($selected=NULL) {
		if (strlen($selected) < 1) { $selected = 1; }
		$findNames = db("SELECT accountId,firstname FROM accounts 
						WHERE accessLevel='Admin' 
						ORDER BY firstname");
		if ($findNames) {
			$showNames = '<select name="accountId"><option value="" ';
			//if (is_null($default)) { $showNames .= 'selected="selected"'; }
			$showNames .= '></option>';
			foreach ($findNames as $values) {
				extract($values);
				$showNames .= '<option value="'.$accountId.'"';
				if ($selected == $accountId) { $showNames .= ' selected="selected"'; }
				//if (!is_null($default) && ($default == $values['accountId'])) { $showNames .=  'selected="selected"'; } 
				$showNames .= '>'.$firstname.'</option>';
				
			}
			$showNames .= '</select>';
		}
		return $showNames;
	}
	function showComments($id,$type="order",$secondaryType=NULL,$debug=0) {
		/*
		2013-03-20
		If you edit a note, the order of the notes changes (and so does the last updated name) 
		so you must sort first by commentId, then by date
		*/
		$findLine = "SELECT c.commentId,c.accountId,c.comment,c.commentGroup,c.commentUpdate,a.firstname AS commentName 
						FROM comments AS c LEFT JOIN accounts AS a USING (accountId) WHERE ";
		if (is_null($secondaryType)) {
			$findLine .= "c.commentType='$type' ";			
		} else {
			$findLine .= "(c.commentType='$type' OR c.commentType='$secondaryType') ";			
		}
		$findLine .= "AND c.commentTypeId='$id'";	
		$findLine .= " ORDER BY c.commentId,c.commentUpdate ASC";
		$find = db($findLine,$debug);
		if ($find) {
			$showComments = array();
			$showComments[] = <<<COMMENTS
<div class="panel panel-info">
<div class="panel-heading"><h3 class="panel-title">Comments</h3></div>
<ul class="list-group">
COMMENTS;
			foreach ($find as $v) {
				extract($v);
				$commentUpdated = date("M d, y",strtotime($commentUpdate));
				$showCommentForm = '';
				if ($type !== "order") {
					$showCommentForm = $this->wrapCommentsWithForm($commentId,$comment,'Julaine',$type,$id);
				}
				$comment = html_entity_decode ($comment,ENT_NOQUOTES,"UTF-8");//ENT_NOQUOTES
				$showComments[] = <<<COMMENTS
<li class="list-group-item"><strong>[$commentId] updated   $commentUpdated</strong> 
<span class="pull-right">
<button class="btn btn-xs btn-primary" data-toggle="collapse" data-target="#comment$commentId">
<i class="fa fa-unlock-alt"></i> </button>&nbsp;
<a class="btn btn-xs btn-warning" href="delete_comment.php?id=$commentId&amp;return=$id">
<i class="fa fa-trash-o"></i> delete</a>
</span><br>
<p>{$comment}
$showCommentForm
<p>							
</li>
							
COMMENTS;
			}
			$showComments[] = '</ul>
			
</div><!--end of panel-->';
			return implode("\n",$showComments);
		} else {
			return FALSE;	
		}
	}
	
	/* untested below here */
	function wrapCommentsWithForm($commentId,$comment,$commentBy='Julaine',$type="order",$id=NULL) {
		//type is the first part of the table name
		//need a string of values for editing the form
		//use 999 for process to prevent duplication
		//2013-02-15
		//I added id as a generic way of supplying return id number for issues, and such
		if (!(int)$commentBy > 0) {
			$commentBy = "";
		}
		$commentEdit = <<<HEREDOC
<div class="collapse" id="comment$commentId">
<form name="editComment" method="post" action="save_comment.php">
<p><textarea name="internal_comment" cols="80" rows="4" class="textarea">$comment</textarea></p>
<P><label>by</label>{$this->showNames($commentBy)}</P>
<input name="Submit" type="submit" class="btn btn-primary" value="Save">
<input type="hidden" name="process" value="999">
<input type="hidden" name="id" value="$id">
<input type="hidden" name="commentId" value="$commentId">
</form>
</div>
HEREDOC;
		return $commentEdit;
	}

	function deleteComment($commentId) {
		$del = Database::db("DELETE FROM comments WHERE commentId='$commentId'");
		//check to see if it worked?
		//TODO -
		return TRUE;
	}
	function editComment($commentId,$postedValues,$debug=0) {
		$updateSql = "UPDATE comments SET";
		foreach ($postedValues as $k=>$v) {
			$updateSqlMiddle .= "$k='".mysql_real_escape_string($v)."',";	
		}
		$updateSql .= substr(0,strlen($updateSqlMiddle)-1,$updateSqlMiddle);	
		$updateSql .= "WHERE commentId='$commentId'";
		
		$saveChanges = db($updateSql,$debug);
		if (!$saveChanges) {
			return FALSE;
		} else {
			return TRUE;	
		}
	}
}?>
