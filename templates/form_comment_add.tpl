{form:comment_add}
<div class="form-group">
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><label for="commentNew" class="control-label">Add a Comment</label> 
		{$txtCommentNew}<br>{$txtCommentNewError}</div>
</div>
<div class="form-group">
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><p><br>Priority<br>
	        {iteration:priority}
	            <label for="{$priority.id}">{$priority.rbtPriority} {$priority.label}</label>
	        {/iteration:priority}
	        {$rbtPriorityError}
	<br>Status<br>
	        {iteration:status}
	            <label for="{$priority.id}">{$status.rbtStatus} {$status.label}</label>
	        {/iteration:status}
	        {$rbtStatusError}
	</div>
</div>
<div class="form-group">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">{$btnSubmit} {$hidProcess}</div>
</div>
{/form:comment_add}		

