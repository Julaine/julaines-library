<?php
/**
 * These were parts of functions file
 * Now they have their own home
 * @package julaines-library
 */
/**
 * Class DateTime
 *
 * Converted from a list of functions that I used elsewhere
 * @todo What about the other classes in this folder?
 *
 * New method generate_dateDropDown
 * needs to be used and tested
 * @version 0.0.1
 * @package julaines-library
 */
class DateTimeArchive {
    /**
     * http://stackoverflow.com/questions/12938895/php-show-todays-date-time-in-form-drop-down-lists
     * Consider the alternative
     * where $maxDate is 31 for days, 12 for months, etc.
     *
     * @param $name
     * @param $maxDate
     * @return string
     */
    function generate_dateDropDown($name,$maxDate) {
        $display[] = '<select name="$name">';
         for ($i = 0; $i <= $maxDate; $i++) {
             $sel = ($i == $sec) ? ' selected="selected"' : '';
             $display[] = '<option value="$i" '.$sel.'>'.$i.'</option>';
        }
        return join("/n",$display);
    }

    /**
     * make a list box of options
     * that shows all the days possible in a month
     * @param     $name
     * @param int $default
     * @return string
     */

    public static function listbox_date($name,$default = 0){
        $result = '<select name="'.$name.'" class="form-control">'."\n";
        for ($d = 1; $d <= 31; $d++){
            if ($default == $d) {
                $selected = "selected";
            } else {
                $selected = "";
            }
            $result .= "<option value=\"$d\" $selected>$d</option>\n";
        }
        $result .= "</select>\n";

        return $result;
    }

    /**
     * makes a list box of options
     * that shows all the months possible
     * @param     $name
     * @param int $default
     * @return string
     */
    public static function listbox_month($name,$default = 0){
        $result = '<select name="'.$name.'" class="form-control">'."\n";
        for ($m = 1; $m <= 12; $m++){
            if ($default == $m) {
                $selected = "selected";
            } else {
                $selected = "";
            }
            $result .= "<option value=\"$m\" $selected>".date("M",mktime(0,0,0,$m,1,2000))."</option>\n";
        }
        $result .= "</select>\n";

        return $result;
    }

    /**
     * Makes a list box of options
     * for all years from start to end
     * @param     $name
     * @param     $start
     * @param     $end
     * @param int $default
     * @return string
     */
    public static function listbox_year($name,$start,$end,$default = 0){
        $result = '<select name="'.$name.'" class="form-control">'."\n";
        for ($y = $end; $y >= $start; $y--){
            if ($default == $y) {
                $selected = "selected";
            } else {
                $selected = "";
            }
            $result .= '<option value="'.$y.'" '.$selected.'>'.$y.'</option>';
        }
        $result .= "</select>\n";

        return $result;
    }

    /**
     * fix the date and return in a specific format
     *
     * @todo could be more useful
     * @param $val
     * @return bool|string
     */
    public static function fixDate($val){
        // split it up into components
        $datearr = explode("-",$val);

        // create a timestamp with time(), format it with date()
        return date("l, d M Y",mktime(0,0,0,$datearr[1],$datearr[2],$datearr[0]));
    }

    /**
     * Generate a series of dropdowns
     * tos select month,day,year
     *
     * @todo could be made from three separate methods
     * @param string $prefix
     * @return string
     */
    public static function generateDateSelector($prefix = ""){
        // month array
        $monthArray = array("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $arr = getdate(mktime());
        $currYear = $arr["year"];
        $currMonth = $arr["mon"];
        $currDay = $arr["mday"];

        // generate month drop-down
        $string[] = '<div class="col-md-4"><select name="'.$prefix.'m" class="form-control">';
        for ($x=1; $x<=12; $x++) 	{
            $str = "<option value=" . sprintf("%02d", $x) . "";
            if ($x == $currMonth)		{
                $str .= " selected";
            }
            $str .= ">" . $monthArray[$x] . "</option>";
            $string[] = $str;
        }
        $string[] = "</select></div>";

        // generate date drop-down
        $string[] = '<div class="col-md-4"><select name="'.$prefix.'d" class="form-control">';
        for ($x=1; $x<=31; $x++) 	{
            $str = "<option value=" . sprintf("%02d", $x) . "";
            if ($x == $currDay)		{
                $str .= " selected";
            }
            $str .= ">" . sprintf("%02d", $x) . "</option>";
            $string[] = $str;
        }
        $string[] = "</select></div>";

        // generate year drop-down
        $string[] = '<div class="col-md-4"><select name="'.$prefix.'y" class="form-control">';
        for ($x=(2000); $x<($currYear+5); $x++)	{
            $str = "<option value=$x";
            if ($x == $currYear)		{
                $str .= " selected";
            }
            $str .= ">" . sprintf("%04d", $x) . "</option>";
            $string[] = $str;
        }
        $string[] = "</select></div>";
        return join("\n",$string);
    }

    /**
     * Create a dropdown of hours
     * @param string $id
     * @param null   $selected
     * @return string
     */
    public static function createHours($id = 'hours_select',$selected = NULL){
        $r = range(1,12);

        $selected = is_null($selected) ? date('h') : $selected;

        $select = "<select name=\"$id\" id=\"$id\">\n";
        foreach ($r as $hour){
            $select .= "<option value=\"$hour\"";
            $select .= ($hour == $selected) ? ' selected="selected"' : '';
            $select .= ">$hour</option>\n";
        }
        $select .= '</select>';

        return $select;
    }

    /**
     * Create a dropdown with all minutes
     * @param string $id
     * @param null   $selected
     * @return string
     */
    public static function createMinutes($id = 'minute_select',$selected = NULL){
        $minutes = array(0,15,30,45);

        $selected = in_array($selected,$minutes) ? $selected : 0;

        $select = "<select name=\"$id\" id=\"$id\">\n";
        foreach ($minutes as $min){
            $select .= "<option value=\"$min\"";
            $select .= ($min == $selected) ? ' selected="selected"' : '';
            $select .= ">".str_pad($min,2,'0')."</option>\n";
        }
        $select .= '</select>';

        return $select;
    }

    /**
     * create a select box with AM/PM
     * @param string $id
     * @param null   $selected
     * @return string
     */
    public static function createAmPm($id = 'select_ampm',$selected = NULL){
        $r = array('AM','PM');

        $selected = is_null($selected) ? date('A') : strtoupper($selected);

        $select = "<select name=\"$id\" id=\"$id\">\n";
        foreach ($r as $ampm){
            $select .= "<option value=\"$t\"";
            $select .= ($ampm == $selected) ? ' selected="selected"' : '';
            $select .= ">$ampm</option>\n";
        }
        $select .= '</select>';

        return $select;
    }

}
?>