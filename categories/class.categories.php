<?php
/**
 * Manage Categories on the site
 *
 * @package julaines-library
 */

/**
 * Class Categories
 *
 * Manage the categories table in the database
 *
 * @todo     FIND showSubCategory() method and add it here
 * @todo     clean code and alphabetize
 *
 * @package  julaines-library
 * @version  1.0.6 findParentCategoryFolder
 */
class Categories {
    /*
     * Add a new category to the database
     *
     * It is finding the parent categoryFolder
     * and using it as part of the categoryFolder
     * when adding
     *
    * @todo import function showCategories($p=0,$productType=0=0) from functionsRoot
    */
    function addCategory($postedValues) {
        if (is_array($postedValues)) {
            extract($postedValues);
        }
        $userId = Admin::getUserId();
        //need support for the subfolder name.
        if ($parentCategoryId > 0) {
            $findParentFolder     = Database::db("SELECT categoryFolder FROM categories WHERE categoryId='$parentCategoryId'",
                2);
            $parentCategoryFolder = $findParentFolder[0];
            $categoryFolder       = $parentCategoryFolder . '/' . $categoryFolder;
        } else {
            //need function for this...
            $categoryFolder = strtolower(trim($category));
        }

        $categoryId = Database::db("INSERT INTO categories
						VALUES ('','$userId','$parentCategoryId',
						'$category','$categoryDescription','Yes',
						'$categoryType','','','','$categoryFolder',NOW())",
            2);

        return $categoryId;
    }

    /**
     * Clean out unwanted characters from the categoryName
     *
     * Could this be better using a filter_var?
     *
     * @param $name
     * @return mixed
     */
    public static function cleanCategoryName($name) {
        $findArray    = array(" ", "&", "__", ",", ".", "&#39;", "'",);
        $replaceArray = array("_", "", "_", "", "", "", "");
        $newName      = str_replace($findArray, $replaceArray, strtolower($name));

        return $newName;
    }

    /**
     * Generic function brought in from  catalogueFiles
     * different version in epatches fork
     * @todo Needs to be moved
     *
     * @param int $suppliedCategoryId
     * @param int $debug
     * @return string
     */
    function createCategoryPages($suppliedCategoryId = 0, $debug = 0) {
        if ((int)$suppliedCategoryId > 0) {
            $findAll = Database::db("SELECT categoryId,parentCategoryId,category,
                        categoryDescription,categoryType,categoryFolder
						FROM categories
						WHERE categoryId='$suppliedCategoryId'",
                $debug);
        } else {
            $findAll = Database::db("SELECT categoryId,parentCategoryId,category,
                        categoryDescription,categoryType,categoryFolder
						FROM categories
						WHERE display='Yes' ORDER BY category", $debug);
        }
        if (!$findAll) {
            Bugs::setError("save", "failed to find any category info in createCategoryPages");

            return FALSE;
        }
        $resultArray = array();
        foreach ($findAll as $row) {
            extract($row);
            /*STEP 1 - is it a sub-category?
            if so, find the parent information
            */
            if ((int)$parentCategoryId > 0) {
                $parentArray = $this->findParentCategoryInfo($parentCategoryId);
                Bugs::setError("save", "parent path is " . $parentArray['parentPath']);
                $subCategoriesHere = $this->findSubCategories($parentCategoryId,
                    $parentArray['parentName'],
                    $categoryType);
                $destinationPath   = PATH_SITE;

                $destinationFilename  = $destinationPath . $categoryFolder . ".php";
                $destination          = $parentArray['parentPath'] . $categoryFolder . "/";
                $destinationPath      = $parentArray['parentPath'];
                $parentCategoryFolder = $parentArray['parentFolder'];
                $parentCategory       = $parentArray['parentName'];
            } else {
                Bugs::setError('save', "no parent");
                //is this category itself a parent? If so, find the other sub-categories.
                $findAllSubs = Database::db("SELECT category,categoryFolder FROM categories
								WHERE parentCategoryId='$categoryId' ORDER BY category",$debug);
                if ($findAllSubs) {
                    $subCategoriesHere = $this->findSubCategories($categoryId, $category, $categoryType);
                } else {
                    $subCategoriesHere = '';
                }
                $destinationPath      = PATH_SITE;
                $destinationFilename  = $destinationPath . $categoryFolder . ".php";
                $destination          = $destinationPath . $categoryFolder . "/";
                $parentCategoryFolder = '';
                $parentCategory       = '';
            }
            //doesDestinationExist($destinationFilename,$destinationPath,$destination);
            $source = PATH_TEMPLATES . "category_new.php";

            $findArray    = array("titleHere", "descriptionHere", "idHere", "categoryHere", "categoryFolderHere", "subCategoriesHere", "categoryTypeHere", "parentCategoryHere", "parentCategoryFolderHere");
            $replaceArray = array($category, addslashes($categoryDescription), $categoryId, $category, $categoryFolder, addslashes($subCategoriesHere), $categoryType, $parentCategory, $parentCategoryFolder);
            Bugs::setError('save',
                "destination is $destination and destinationPath is $destinationPath and destinationFilename
			 is $destinationFilename");
            if (!file_exists($destinationFilename)) {
                Bugs::setError('save', "The file $destinationFilename does not exist, attempt to create.");
                // Must create folder first.
                Files::doesDestinationExist($destinationFilename, $destinationPath, $destination);
            }

            $data = file_get_contents($source);
            //$originalFileContents = file_get_contents($source);
            $newFileContents = str_replace($findArray, $replaceArray, $data);
            if ($makeFile = Files::updateFile($destinationFilename, $newFileContents) == TRUE) {
                $resultArray[] = '<p><i class="icon icon-plus"></i> ' . $category . ' <a href="/catalogue/' . $categoryFolder . '.php">
								/catalogue/' . $categoryFolder . '.php</a>';
            } else {
                $resultArray[] = "No update for " . $category;
            }

            unset($subCategoriesHere);
            unset($parentCategory);
            unset($parentCategoryFolder);
            unset($subCategory);
            unset($subCategoryFolder);
            unset($categoryType);
            unset($categoryFolder);
        }
        if ($suppliedCategoryId == 0) {
            return join("\n", $resultArray);
        } else {
            return "Failed";
        }
    }

    /**
     * Generic function brought in from catalogueFiles
     *
     * @param $parentId
     * @param $parentName
     * @return string
     */
    function findSubCategories($parentId, $parentName) {
        //does the parent category have other sub-categories.
        $findAllSubs = Database::db("SELECT category,categoryFolder FROM categories
						WHERE parentCategoryId='$parentId' AND display='Yes' ORDER BY category");
        if ($findAllSubs) {
            $subCategoriesHere = "<p>Find more " . $parentName . " below:<br>";
            foreach ($findAllSubs as $row) {
                extract($row);
                $subCategoriesHere .= $parentName . ' > <a href="/' . $categoryFolder . '.php">' . $category . '</a><br>';
            }
        }

        return $subCategoriesHere;
    }

    /**
     * Generic function brought in from catalogueFiles
     *
     * @param $parentCategoryId
     * @return array
     */
    function findParentCategoryInfo($parentCategoryId) {
        $findParent = Database::db("SELECT category,categoryType,categoryFolder
						FROM categories
						WHERE categoryId='$parentCategoryId'");
        if (!$findParent) {
            Bugs::setError("debug", "no parent information found");

            return FALSE;
        }
        $parentArray = array();
        foreach ($findParent as $row) {
            extract($row);
            $parentArray['parentPath']   = PATH_SITE . "catalogue/" . $categoryFolder . "/";
            $parentArray['parentName']   = $category;
            $parentArray['parentFolder'] = $categoryFolder;
        }

        return $parentArray;
    }

    /**
     * Find the full path to the category
     *
     * @param $parentCategoryId
     * @return array|bool
     */
    function findParentCategoryFolder($parentCategoryId) {

        $findParent = Database::db("SELECT category,categoryType,categoryFolder
						FROM categories
						WHERE categoryId='$parentCategoryId'",$debug);
        if (!$findParent) {
            Bugs::setError("debug","no parent information found");
            return FALSE;
        }
        $parentArray = array();
        foreach ($findParent as $row) {
            extract($row);
            if ($categoryType == "Idea") {
                $parentArray['parentPath'] = PATH_SITE.$categoryFolder."/";
            } else {
                $parentArray['parentPath'] = PATH_SITE.$categoryFolder."/";
            }
            $parentArray['parentName'] = $category;
            $parentArray['parentFolder'] = $categoryFolder;
        }
        return $parentArray;
    }
    /**
     * Show all the categories
     *
     * By type (if applicable)
     * in a list of checkboxes
     *
     * @todo    Same as epc>Categories
     * @param      $p
     * @param null $categoryType
     * @return bool|string
     */
    function showCategories($p, $categoryType = NULL) {
        $find = "SELECT c.categoryId,c.category
				FROM categories AS c
				LEFT JOIN pagesCategories AS pc USING (categoryId)
				WHERE pc.pageId='$p'";
        if (!is_null($categoryType)) {
            $find .= " AND c.categoryType='$categoryType'";
        }
        $findAll = Database::db($find, 2);
        if (!$findAll) {
            return FALSE;
        }
        $showCategories = array();
        foreach ($findAll as $row) {
            extract($row);
            $showCategories[] = '<label class="radio-inline">
			<input type="checkbox" name="categoryId[]" checked value="Yes"> ' . $category . '</label><br>';
        }

        return join("\n", $showCategories);
    }

    /**
     * Show the categories in a select box
     *
     * @param null $categoryType
     * @param null $c
     * @return bool|string
     */
    function showCategories2select($categoryType = NULL, $c = NULL) {
        $find = "SELECT categoryId,category
				FROM categories";
        if (!is_null($categoryType)) {
            $find .= " WHERE categoryType='$categoryType'";
        }
        $findAll = Database::db($find);
        if (!$findAll) {
            return FALSE;
        }
        $showCategories2select = array();
        foreach ($findAll as $row) {
            extract($row);
            $showCategories2select[] = '<option value="' . $categoryId . '"';
            if ($c == $categoryId) {
                $showCategories2select[] .= 'selected';
            }
            $showCategories2select[] = '>' . $category . '</option>';
        }

        return join("\n", $showCategories2select);
    }

    /**
     * Generate a list of options
     * for a select box
     *
     * @todo Should include a check for status active
     * @return bool|string
     */
    function listCategories() {
        $findAll = Database::db("SELECT categoryId,category FROM categories
						WHERE parentCategoryId='0' ORDER BY categoryFolder,category");
        if (!$findAll) {
            return FALSE;
        }
        foreach ($findAll as $row) {
            extract($row);
            //while (list($parentCategoryId,$parentCategory,$categoryType2) = mysql_fetch_array($findAll)) {
            $show[] = '<option value="' . $categoryId . '">' . $category . '</option>' . "\n";

            $findChildren = Database::db("SELECT category AS childName FROM categories WHERE parentCategoryId='$categoryId'");
            if ($findChildren) {
                foreach ($findChildren as $row2) {
                    extract($row2);
                    //while (list($childName) = mysql_fetch_array($findChildren)) {
                    $show[] = '<option value="' . $categoryId . '">' . $category . ' -' . $childName . '</option>' . "\n";
                }
            }
        }

        return join("\n", $show);
    }

    /**
     * Find the categoryFolder for the category
     *
     * @param null $categoryId
     * @return bool|int
     */
    function getFolder($categoryId = NULL) {
        if ((int)$categoryId > 0) {
            $findCategory = Database::db("SELECT category,categoryFolder FROM categories WHERE categoryId='$categoryId'");
            foreach ($findCategory as $row) {
                return extract($row);
            }
        } else {
            return FALSE;
        }
    }

    /**
     * Hey, this does not work for non-ecommerce sites
     * Show a list of categories in a table
     * based on categoryType
     *
     * with sub-categories
     */
    function showCategoryTypeInTable($type) {
        $types = array('All', 'Admin', 'Members'); //2013-03-12
        if (!in_array($type, $types)) {
            return FALSE;
        }
        if ($type == 'Inactive') {
            $findAll = Database::db("SELECT categoryId,category,discount
							FROM categories 
							WHERE parentCategoryId='0' AND display='No' 
							ORDER BY categoryFolder,category");
        } else {
            $findAll = Database::db("SELECT categoryId,category,discount
						FROM categories 
						WHERE parentCategoryId='0' AND display='Yes' 
						AND categoryType='$type' 
						ORDER BY categoryFolder,category");
        }
        if ($findAll) {
            $tableArray = <<<TABLE
<h3>$type</h3>
<table class="table table-striped table-bordered">
 <thead>
  <tr><th>Num</th><th>Parent</th><th>Category</th><th>Discount</th></tr>
  </thead>
TABLE;
            foreach ($findAll as $row) {
                extract($row);
                $tableArray .= <<<TABLE
<tr><td>$categoryId</td>
<td>&nbsp;</td>
<td><a href="category.php?id=$categoryId">$category</a></td>
<td>$discount%</td>
</tr>
TABLE;
                $findAllSubs = Database::db("SELECT categoryId AS subCategoryId,category AS subCategory,discount AS subDiscount
								FROM categories WHERE parentCategoryId='$categoryId' 
								ORDER BY category");
                if ($findAllSubs) {
                    foreach ($findAllSubs as $srow) {
                        extract($srow);
                        $tableArray .= <<<SUBTABLE
<tr><td>$subCategoryId</td>
<td>$category</td>
<td><a href="category.php?id=$subCategoryId">$subCategory</a></td>
<td>$subDiscount%</td>
</tr>
SUBTABLE;
                    }
                }
            }
            $tableArray .= '</table>';

            return $tableArray;
        } else {
            return FALSE;
        }

    }

    /**
     * Show all the categories (by type if applicable)
     * in a table
     *
     * @param $type
     * @return bool|string
     */
    function showCategoryInTable($type) {
        $types = array('All', 'Admin', 'Members'); //2013-03-12
        if (!in_array($type, $types)) {
            return FALSE;
        }
        if ($type == 'Inactive') {
            $findAll = Database::db("SELECT categoryId,category
							FROM categories 
							WHERE parentCategoryId='0' AND display='No' 
							ORDER BY categoryFolder,category");
        } else {
            $findAll = Database::db("SELECT categoryId,category
						FROM categories 
						WHERE parentCategoryId='0' AND display='Yes' 
						AND categoryType='$type' 
						ORDER BY categoryFolder,category");
        }
        if ($findAll) {
            $tableArray = <<<HEREDOC
<h3>$type</h3>
<table class="table table-striped table-bordered">
 <thead>
  <tr><th>Num</th><th>Parent</th><th>Category</th></tr>
  </thead>
HEREDOC;
            foreach ($findAll as $row) {
                extract($row);
                $tableArray .= <<<HEREDOC
<tr><td>$categoryId</td>
<td>&nbsp;</td>
<td><a href="category.php?id=$categoryId">$category</a></td>
</tr>
HEREDOC;
                $findAllSubs = Database::db("SELECT categoryId AS subCategoryId,category AS subCategory AS subDiscount
								FROM categories WHERE parentCategoryId='$categoryId' 
								ORDER BY category");
                if ($findAllSubs) {
                    foreach ($findAllSubs as $srow) {
                        extract($srow);
                        $tableArray .= <<<HEREDOC
<tr><td>$subCategoryId</td>
<td>$category</td>
<td><a href="category.php?id=$subCategoryId">$subCategory</a></td>
</tr>
HEREDOC;
                    }
                }
            }
            $tableArray .= '</table>';

            return $tableArray;
        } else {
            return FALSE;
        }

    }

    /**
     * Show the parent categories in a list of options
     * for a select box
     *
     * This is different from epatches fork
     *
     * @param null $type
     * @param null $id
     * @return string   options for a select field in a form
     */
    function showParentCategory($type = NULL, $id = NULL,$parent = NULL) {
        /*use in place of crestLocation() */
        if (!is_null($type)) {
            $findAll = Database::db(
                "SELECT parentCategoryId,categoryId,category,categoryType FROM categories
                        WHERE parentCategoryId='0' AND categoryType='$type'
                        ORDER BY categoryFolder,category"
            );

        } else {
            $findAll = Database::db(
                "SELECT parentCategoryId,categoryId,category,categoryType FROM categories
                    WHERE parentCategoryId='0' ORDER BY categoryFolder,category"
            );


        }
        if (!$findAll) {
            return FALSE;
        }
        foreach ($findAll as $rows) {
            extract($rows);
            $selected = NULL;
            if ($id == $categoryId) {
                $selected = ' selected';
            }
            if ($parent == $categoryId) {
                $selected = ' selected';
            }
            $show[] = '<option value="' . $categoryId . '" class="' . $categoryType . '"'.$selected.'>' . $category .
                '</option>';

        }
        return join("", $show);
    }
    /**
     * Show all sub-categories in database
     *
     * @todo add active search
     * @param null $id
     * @return bool|string
     */
    function showSubCategory($id = NULL) {
        $findChildren = Database::db(
            "SELECT categoryId,parentCategoryId,category,categoryType FROM categories WHERE parentCategoryId !='0' ORDER BY category"
        );
        if ($findChildren) {
            $show = array();
            foreach ($findChildren as $row) {
                extract($row);
                $findAll = Database::db(
                    "SELECT category AS parentCategory FROM categories
								WHERE parentCategoryId='$parentCategoryId' ORDER BY categoryFolder,category"
                );
                if ($findAll) {
                    $parentCategory = $findAll[0];
                } else {
                    $parentCategory = '';
                }
                    $show[]         = '<option value="' . $categoryId . '" class="' . $categoryType . '"';
                    if ($id == $categoryId) {
                        $show[] = ' selected';
                    }
                    $show[] = '>' . $category . '</option>';

            }

            return join("\n", $show);
        }

        return FALSE;
    }
}

?>