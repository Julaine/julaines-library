<?php
/**
 * Setup to clean out functions file
 *
 * @package julaines-library
 */

/**
 * Class Downloads
 *
 * @version  1.0.1
 * @package  julaines-library
 */
class Downloads {
    /**
     * Force a file to download
     *
     * @param $fileName
     * @param $filePath
     */
    public function forceDownload($fileName, $filePath) {
        $fileSize = filesize($filePath);
        $fileExt  = trim(str_replace('.', '', strrchr($fileName, '.')));
        Bugs::setError('debug', 'Downloading file ' . $fileName . ' with ext ' . $fileExt);
        switch ($fileExt) {
            case "pdf":
                $ctype = "application/pdf";
                break;
            case "exe":
                $ctype = "application/octet-stream";
                break;
            case "zip":
                $ctype = "application/zip";
                break;
            case "doc":
                $ctype = "application/msword";
                break;
            case "xls":
                $ctype = "application/vnd.ms-excel";
                break;
            case "ppt":
                $ctype = "application/vnd.ms-powerpoint";
                break;
            case "gif":
                $ctype = "image/gif";
                break;
            case "png":
                $ctype = "image/png";
                break;
            case "jpeg":
            case "jpeg":
            case "jpg":
                $ctype = "image/jpg";
                break;
            case "txt":
                $ctype = "text/plain";
                break;
            case "mp3":
                $ctype = "audio/mpeg";
                break;
            case "wav":
                $ctype = "audio/x-wav";
                break;
            case "mpg":
            case "mpeg":
            case "mpe":
                $ctype = "video/mpeg";
                break;
            case "mov":
                $ctype = "video/quicktime";
                break;
            case "avi":
                $ctype = "video/x-msvideo";
                break;
            case "txt":
                $ctype = "text/plain";
                break;
            default:
                $ctype = "application/force-download";
        }
        if ($fileExt == "pdf") {
            $size = filesize($filePath);
            Bugs::setError('debug',
                'Downloading PDF ' . $fileName . ' of size ' . $size . ' with ext ' . $fileExt . '--end');
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            //hid these or the pdf is a word doc
            //header('Content-Type: application/octet-stream');
            //header("Content-Type: application/force-download");
            header("Content-disposition: inline; filename=" . $fileName);
            header("Content-Transfer-Encoding: binary");
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . $size);
            readfile($filePath);
        } else {
            Bugs::setError('debug',
                'Downloading ' . $ctype . ' (filename: ' . $fileName . ') with ext ' . $fileExt . '--other');
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", FALSE);
            header("Content-Type: $ctype");
            header("Content-Disposition: attachment; filename=" . $fileName . ";");
            header("Content-Transfer-Encoding: binary");
            //header("Content-Length: ".$fileSize);
            header("Content-Length: " . filesize($filePath));
            header('Content-Description: File Transfer');
            readfile("$filePath");
        }

        exit();
    }
}

?>