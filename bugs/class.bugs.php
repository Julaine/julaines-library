<?php
/**
 * What do I do with errors and what-nots
 * @package julaines-library
 */
/**
 * Added ability to select from actions db
 *
 * @version 2.0.1
 * @package julaines-library
 * @uses        Spoon
 * @requires    Debugger
 */
class Bugs
{
    /**
     * Brought in from logged actions
     * @todo expand and add table row syntax
     *       with row color highlighting for warnings and such
     * @param $s
     * @return array
     */
    public static function getActions($s)
    {
        $currMonth = date("m", time());
//$currYear = date("Y", time());
        unset($range1);
        unset($range2);
        $findAllSql = "SELECT actionDate,action,actionPage,ip FROM actions";
        if ($s == "cm") {
            $findAllSql .= " WHERE (MONTH(CURDATE())=MONTH(actionDate)) AND (YEAR(CURDATE())=YEAR(actionDate)) ORDER
            BY actionDate DESC";
        } elseif ($s == "lm") {
            if ($currMonth == "01") {
                $lastMonth = "12";
                $lastYear  = currYear - 1;
            } else {
                $lastMonth = $currMonth - 1;
                $lastYear  = currYear;
            }
            //$lastMonth = $currMonth-1;
            $findAllSql .= " WHERE (MONTH(actionDate)='$lastMonth') AND (YEAR(actionDate)='$lastYear') ORDER BY actionDate DESC";
        } elseif ($s == "y") {
            $findAllSql .= " WHERE (YEAR(actionDate)='".currYear."') ORDER BY actionDate DESC";
        } elseif ($s == "ly") {
            $lastYear = currYear - 1;
            $findAllSql .= " WHERE (YEAR(actionDate)='$lastYear') ORDER BY actionDate DESC";
        } elseif (isset($_POST['Submit']) && ($_POST['dateRange'] !== "")) {

            $dateRange  = $_POST['dateRange'];
            $dateRange1 = substr($dateRange, 0, 10).' 00:00:00';
            $dateRange2 = substr($dateRange, 13, 10).' 23:59:59';

            if ($dateRange1 !== "") {
                if ($dateRange2 !== "") {
                    $findAllSql .= " WHERE actionDate BETWEEN '$dateRange1' AND '$dateRange2' ORDER BY actionDate DESC";
                } else {
                    $findAllSql .= " WHERE actionDate>='$dateRange1' ORDER BY actionDate DESC";
                }
            } else {
                if ($dateRange2 !== "") {
                    $findAllSql .= " WHERE actionDate <= '$dateRange2' ORDER BY actionDate DESC";
                }
            }

        } elseif ($s == "t") {
            $today = date("Y-m-d",time());
            $findAllSql .= " WHERE actionDate LIKE ('$today%') ORDER BY actionDate DESC";
        } elseif ($s == "a") {
            $findAllSql .= " ORDER BY actionDate DESC";
        } else {
            $findAllSql .= " WHERE (MONTH(actionDate)='$currMonth') AND (YEAR(actionDate)='".currYear."') ORDER BY actionDate
	DESC";
        }
        return Database::db($findAllSql);
    }
    /**
     * Save the information to the actions table
     *
     * @todo Exists elsewhere and needs to be sought out and destroyed
     * @param     $action
     * @param int $page
     * @return mixed
     */
    public static function saveAction($action, $page = null)
    {
        //*static* means that I must call debugger this way
        $debugger = new Debugger;
        /*TODO needs to be more robust - can it just replace error_log?
        save error_log for mysql traffic.*/
        if (is_null($page)) {
            $self = filter_var($_SERVER['PHP_SELF'], FILTER_SANITIZE_MAGIC_QUOTES);
            $action = filter_var($action, FILTER_SANITIZE_MAGIC_QUOTES);
            $page = basename($self);
        }
        $actionId = Database::db("INSERT INTO actions VALUES ('',NOW(),'$action','$page','".$debugger->getRealIp()."',NOW())");
        return $actionId;
    }

    /**
     * Handle message and sort how to record/show them
     *
     * @param  string $type how to handle the message
     * @param         $text
     * @param  string $page Page wherein message occurred
     * @internal param string $message Message Text
     * @todo     Setup Code to handle Exceptions
     *                      there is an exception file started in other projects...
     * @return  error message output
     */
    public static function setError($type = null, $text, $page = null)
    {
        if ($type == "save") {
            self::saveAction($text, $page);
        } elseif ($type == "debug") {
            error_log($text.' - '.$page);
        } elseif ($type == "debug+notify") {
            error_log("$text on $page");
            $_SESSION['errors'] = $text;
        } elseif ($type == "notify") {
            $_SESSION['errors'] = $text;

            /**
             * @todo do I use "show" ?
             * No, do I want to?
             * how is it different than Debugger::Debug?
             */
        } elseif ($type == "show") {
            if (in_array($_SERVER['REMOTE_ADDR'], $allowed)) {
                return $text;
            }
            /**
             * Will I want to use track? Could it replace logged messages?
             */
        } elseif ($type == "track") {

        } else {
            $_SESSION['errors'] = $text;
        }

    }

}

?>
