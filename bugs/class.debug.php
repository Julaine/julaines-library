<?php
/**
 * Can you and should you see the debugging elements
 * @package julaines-library
 */
/**
 * Class Debugger
 * @package julaines-library
 * @version  1.0.2
 */
class Debugger {
    /**
     * Is your IP allowed
     *
     * @return bool
     */
    public static function canDebug() {
        $allowed = array ('127.0.0.1', '');
        if (in_array (self::getRealIP(), $allowed)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Show the debugging message
     * In a Bootstrap alert box
     *
     * @param $debug_value
     * @param $message
     */
    public static function Debug ($debug_value,$message) {
        if (!self::CanDebug()) return;
        if ((int)$debug_value == 1) {
            echo '<div class="alert-danger">';
            if (is_string ($message)) {
                echo $message;
            } else {
                var_dump ($message);

            }
            echo '</div>';
        } elseif ((int)$debug_value == 2) {
            Bugs::setError("debug",$message);
        }
    }

    /**
     * New handler that has some interesting ideas
     * Not sure what to use it for yet
     *
     * @param $errno
     * @param $errstr
     * @param $errline
     * @return string
     */
    function errorHandler($errno, $errstr, $errline) {
        $errortype = array (
            1   =>  "Error",
            2   =>  "Warning",
            4   =>  "Parsing Error",
            8   =>  "Notice",
            16  =>  "Core Error",
            32  =>  "Core Warning",
            64  =>  "Compile Error",
            128 =>  "Compile Warning",
            256 =>  "User Error",
            512 =>  "User Warning",
            1024=>  "User Notice",
            2048=>  "E_ALL",
            2049=>  "PHP5 E_STRICT"

        );
        $str = sprintf("[%s]\n%s:\t%s\nFile:\t\t'%s'\nLine:\t\t%s\n\n", date('d-m-Y H:i:s'),(isset($errortype[@$errno])?$errortype[@$errno]:('Unknown '.$errno)),@$errstr,@$errfile,@$errline);
        @fwrite($f, $str);
        if (@$errno == 2 && isset($already_sent) && !$already_sent==true){
            $error = '<ERRORS>'."\n";
            $error .= '<ERROR><DESCRIPTION>An Warning Type error appeared. The error is logged into the log file.</DESCRIPTION></ERROR>'."\n";
            $error .= '</ERRORS>'."\n";
            $already_sent = true;
            return $error;
        }
    }
	/**
	 * My version of how to get IP 
	 * Differs to spoon
	 * 
	 *
	 * @return string ip address 
	 */
	public static function getRealIp() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  //to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif (!empty($_SERVER['REMOTE_ADDR'])) {  //to check ip is pass from proxy
			$ip = $_SERVER['REMOTE_ADDR'];
		} else {
			$ip = "unknown";
		}
		return $ip;
	}
}
?>