<?php

/**
 * Julaine Library
 *
 * @package        julaines-library
 * @version 1.0.2
 *
 * @author         Julaine Scott <work@julaine.ca>
 */

/*
 * This is the version number for the current version of the
 * Spoon Library.
 */
define('JULAINE_VERSION', '1.0.3'); //added components

/*
 * This setting will intervene when an exception occurs. If enabled the exception will be
 * shown in all its glory. If disabled 'Julaine_DEBUG_MESSAGE' will be displayed instead.
 */
if (!defined('JULAINE_DEBUG')) {
	define('JULAINE_DEBUG', TRUE);
}

/*
 * If 'JULAINE_DEBUG' is enabled and an exception occures, this message will be
 * displayed.
 */
if (!defined('JULAINE_DEBUG_MESSAGE')) {
	define('JULAINE_DEBUG_MESSAGE', 'There seems to be an issue with this page.');
}

/*
 * If 'JULAINE_DEBUG' is enabled and an exception occures, an email with the contents of this
 * exception will be emailed to 'JULAINE_DEBUG_EMAIL' if it contains a valid email address.
 */
if (!defined('JULAINE_DEBUG_EMAIL')) {
	define('JULAINE_DEBUG_EMAIL', 'work@julaine.ca');
}

/*
 * If an exception occures, you can hook into the process that handles this exception
 * and add your own logic. The callback may be a function or static method. If you wish
 * to use a static method define this constant in this way: 'MyClass::myMethod'
 */
if (!defined('JULAINE_EXCEPTION_CALLBACK')) {
	define('JULAINE_EXCEPTION_CALLBACK', '');
}

/*
 * Default charset that will be used when a charset needs to be provided to use for
 * certain functions/methods.
 */
if (!defined('JULAINE_CHARSET')) {
	define('JULAINE_CHARSET', 'iso-8859-1');
}

/*
 * Should we use the JULAINE autoloader to ensure the dependancies are automatically
 * loaded?
 */
if (!defined('JULAINE_AUTOLOADER')) {
	define('JULAINE_AUTOLOADER', TRUE);
}

/* JULAINEException class */
//require_once 'exception/exception.php';

// check mbstring extension
if (!extension_loaded('mbstring')) {
	throw new JULAINEException('You need to make sure the mbstring extension is loaded.');
}

// attach autoloader
if (JULAINE_AUTOLOADER) {
	spl_autoload_register(array('JULAINE', 'autoLoader'));
}

/**
 * This class holds objects/data in a name based registry to make them easily
 * available throughout your application.
 *
 * @package        julaines-library
 *
 * @author         Julaine Scott <work@julaine.ca>
 */
class Julaine {
	/**
	 * Registry of variables
	 *
	 * @var    array
	 */
	private static $registry = array();


	/**
	 * JULAINE autoloader
	 *
	 * @param    string $class The class that should be loaded.
	 */
	public static function autoLoader($class) {
		// redefine class
		$class = strtolower($class);

		// list of classes and their location
		$classes                 = array();
		$classes['admin']        = 'admin/class.admin.php';
		$classes['bcrypt']       = 'admin/class.bcrypt.php';
		$classes['login']        = 'admin/class.logins.php';
		$classes['bugs']         = 'bugs/class.bugs.php';
		$classes['debugger']     = 'bugs/class.debug.php';
		$classes['categories']   = "categories/class.categories.php";
		$classes['comments']     = 'comments/class.comments.php';
		$classes['comments']     = 'comments/class.comments.spoon.php';
		//$classes['components']     = 'ui/class.components.php';
		$classes['database']     = 'database/class.database.php';
        $classes['datetimearchive'] = 'DateTime/functions_dateTime.php';
		$classes['downloads']    = 'downloads/class.download.php';
		$classes['emails']       = 'emails/class.emails.php';
		$classes['files']        = 'files/class.files.php';
		$classes['uploadedfiles'] = 'files/class.upload.php';
		$classes['forms']        = 'forms/class.forms.php';
		$classes['messages']     = 'messages/class.messages.php';
		$classes['notices']      = 'notices/class.notices.php';
		$classes['snippets']      = 'snippets/class.snippets.php';
		$classes['testimonials'] = 'testimonials/testimonials.php';
		$classes['timesheet'] = "timesheet/class.timesheet.php";
		$classes['passwordhash']    = 'phpass-0.3/PasswordHash.php';

		$classes['tracking'] = 'tracking/class.tracking.php';
		$classes['edittables'] = 'ui/class.tables.php';
		$classes['ui']     = 'ui/class.ui.php';
		$classes['linkchecker']     = 'ui/class.links.php';

		// path
		$path = dirname(realpath(__FILE__));

		// does this file exist?
		if (isset($classes[$class]) && file_exists($path . '/' . $classes[$class])) {
			require_once $path . '/' . $classes[$class];
		}
	}


	/**
	 * Dumps the output of a variable in a more readable manner.
	 *
	 * @param    mixed $var The variable to dump.
	 * @param          bool [optional] $exit    Should the code stop here?
	 */
	public static function dump($var, $exit = TRUE) {
		ob_start();
		var_dump($var);
		$output = ob_get_clean();

		// Make sure var_dump is not overridden by Xdebug before tweaking its output.
		// Note that all truthy INI values ("On", "true", 1) are returned as "1" by ini_get().
		$hasXdebugVarDump = extension_loaded('xdebug') && ini_get('xdebug.overload_var_dump') === '1';

		if (!$hasXdebugVarDump) {
			$output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
			$output = '<pre>' . htmlspecialchars($output, ENT_QUOTES, JULAINE_CHARSET) . '</pre>';
		}

		echo $output;
		if ($exit) {
			exit;
		}
	}


	/**
	 * Checks if an object with this name is in the registry.
	 *
	 * @return    bool
	 * @param    string $name The name of the registry item to check for existence.
	 */
	public static function exists($name) {
		return isset(self::$registry[(string)$name]);
	}

	/**
	 * Retrieve the list of available charsets.
	 *
	 * @return    array
	 */
	public static function getCharsets() {
		return array('utf-8', 'iso-8859-1', 'iso-8859-15');
	}

	/**
	 * Are we running in the command line?
	 *
	 * @return    bool
	 */
	public static function inCli() {
		return (PHP_SAPI == 'cli');
	}

	/**
	 * Registers a given value under a given name.
	 *
	 * @param    string $name The name of the value to store.
	 * @param           mixed [optional] $value    The value that needs to be stored.
	 * @return str
	 */
	public static function set($name, $value = NULL) {
		// redefine name
		$name = (string)$name;

		// delete item
		if ($value === NULL) {
			unset(self::$registry[$name]);
		} // add & return its value
		else {
			self::$registry[$name] = $value;

			return self::get($name);
		}
	}

	/**
	 * Fetch an item from the registry.
	 *
	 * @param    string $name The name of the item to fetch.
	 * @throws JULAINEException
	 * @return    mixed
	 */
	public static function get($name) {
		$name = (string)$name;

		if (!isset(self::$registry[$name])) {
			throw new JULAINEException('No item "' . $name . '" exists in the registry.');
		}

		return self::$registry[$name];
	}
}
